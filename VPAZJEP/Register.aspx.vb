﻿Imports System
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Owin

Partial Public Class Register
    Inherits Page
    Protected Sub CreateUser_Click(sender As Object, e As EventArgs) Handles Registrarse.Click


        If Password.Text = ConfirmPassword.Text And Len(Password.Text) >= 8 Then
            Label1.Text = ""
            '   System.Threading.Thread.Sleep(2000)
            Dim userName As String = UserNames.Text

            Dim manager = Context.GetOwinContext().GetUserManager(Of ApplicationUserManager)()
            Dim signInManager = Context.GetOwinContext().Get(Of ApplicationSignInManager)()
            Dim user = New ApplicationUser() With {.UserName = userName, .Email = Email.Text, .PhoneNumber = "", .EmailConfirmed = True}


            Dim result = manager.Create(user, Password.Text)

            If result.Succeeded Then
                'Membership.UpdateUser(user)
                Dim Contenidomail As String
                Contenidomail = ""
                Contenidomail = contenidomail & Chr(13) + Chr(10) + Chr(13) + Chr(10)
                contenidomail = contenidomail & Now().ToLongDateString + Chr(13) + Chr(10) + Chr(13) + Chr(10)
                contenidomail = contenidomail & "Señor(a):" + Chr(13) + Chr(10)
                Contenidomail = Contenidomail & lblnombreusuario.Text + Chr(13) + Chr(10)
                Contenidomail = Contenidomail & Chr(13) + Chr(10) & "Asunto: Usuario de acceso a YACHAY " + Chr(13) + Chr(10) + Chr(13) + Chr(10)
                Contenidomail = Contenidomail & "Usted se ha registrado en el sistema YACHAY." & Chr(13) & Chr(10) & Chr(13) & Chr(10) & "Su usuario para acceder al sistema es: " & UserNames.Text & " y la contraseña que usted asignó." & Chr(13) & Chr(10)
                Contenidomail = Contenidomail & Chr(13) + Chr(10) + Chr(13) + Chr(10) + Chr(13) + Chr(10) & "Cordialmente " & Chr(13) + Chr(10)
                Contenidomail = Contenidomail & Chr(13) + Chr(10) & Chr(13) & Chr(10) & "Secretaria Ejecutiva Jurisdicción Especial para la Paz" & Chr(13) & Chr(10)

                Dim Asunto As String = "Usuario para acceder a YACHAY "
                'enviar mail a despacho a fin de que atienda peticion, con copia al correo del usuario

                Dim idpersona As String
                idpersona = ""
                idpersona = SRegistraPersona()
                Registrarse.Enabled = False
                Enviarmail(Contenidomail, Asunto, user.Email)
                Label1.Text = "Revise su mail, tiene los datos de accceso al sistema"
                signInManager.SignIn(user, isPersistent:=False, rememberBrowser:=False)
                IdentityHelper.RedirectToReturnUrl(Request.QueryString("ReturnUrl"), Response)
            Else
                ErrorMessage.Text = result.Errors.FirstOrDefault()
                CmdEnviarClave.Visible = True
                'txtsegundaclave.Visible = false
                'cmdSegundaclave.Visible = false
                'LblIngreseCodigos.Visible = false
                LblMensajeIngreso.Visible = True

                'CmdEnviarClave.Visible = False
                'Registrarse.Visible = False
                'Password.Visible = False
                'ConfirmPassword.Visible = False
                'LblConfirmaPassword.Visible = False
                'LblAsigneContraseña.Visible = False

            End If
        Else
            Label1.Text = "Password no cumple con los requisitos mínimos"
        End If


    End Sub







    Private Function SConsultarUsuario() As String
        Dim sql2 As String
        Dim conexiontmp As Object
        Dim rs3 As Object
        Dim usuarios As String

        Dim esta As Integer
        usuarios = ""

        esta = 2
        '2 esta no esta en el listado


        'primero se buscan los Códigos que requiero eliminar
        sql2 = "SELECT        NombreUsuarioWeb,iduser, estadouser  FROM            UsuariosSistema  WHERE   "
        sql2 = sql2 + "  (emailautorizado = '" & Email.Text & "')   "

        conexiontmp = Server.CreateObject("ADODB.Connection")
        conexiontmp.Open(Application("CadVJep"))

        rs3 = conexiontmp.Execute(sql2)

        Do Until rs3.eof
            If Not rs3("estadouser").value Is DBNull.Value Then
                esta = rs3("estadouser").value
            End If
            If Not rs3("iduser").value Is DBNull.Value Then
                usuarios = rs3("iduser").value
            End If

            If Not rs3("nombreusuarioweb").value Is DBNull.Value Then
                usuarios = usuarios & ", persona:" & rs3("nombreusuarioweb").value
            End If

            rs3.movenext()
        Loop


        rs3.close
        rs3 = Nothing

        conexiontmp.close()
        conexiontmp = Nothing
        If esta = 0 Then
            usuarios = "0"
        Else
            If usuarios = "" Then
                usuarios = Str(esta)

            End If
        End If

        Return usuarios



    End Function


    Private Function SConsultarExistePersona(documento As String) As String
        Dim sql2 As String
        Dim conexiontmp As Object
        Dim rs As Object
        Dim idpersona As String
        conexiontmp = Server.CreateObject("ADODB.Connection")
        conexiontmp.Open(Application("CadVJep"))
        sql2 = "Select idpersona from mpersonas where documentoidentificacion='" & documento & "'"
        rs = conexiontmp.Execute(sql2)
        idpersona = ""
        Do Until rs.eof
            If Not rs("idpersona").value Is DBNull.Value Then
                idpersona = rs("idpersona").value
            End If
            rs.movenext()
        Loop
        rs.close()
        rs = Nothing
        conexiontmp.close()
        conexiontmp = Nothing
        Return idpersona
    End Function


    Private Function SRegistraPersona() As String
        'solo se actualiza si el correo es confirmado con la segunda clave
        Dim sql2 As String
        Dim conexiontmp As Object
        Dim exito As Integer
        exito = 0
        Dim Idpersona As String
        Idpersona = SConsultarExistePersona(UserNames.Text)
        If Idpersona = "" Then
            sql2 = " insert into mpersonas (IDPersona, PrimerNombre, SegundoNombre, PrimerApellido, SegundoApellido, IdTipoDocumento, DocumentoIdentificacion, idgenero, Aspectosdiferenciales, Idetnia) "
            sql2 = sql2 & " values(newid(),'" & HttpUtility.HtmlDecode(txtPrimerNombre.Text) & "','" & HttpUtility.HtmlDecode(txtSegundoNombre.Text) & "','" & HttpUtility.HtmlDecode(txtPrimerApellido.Text) & "','" & HttpUtility.HtmlDecode(txtSegundoApellido.Text) & "','" & LsttipoDocu.SelectedValue & "','" & UserNames.Text & "','NA','NA','NA')"
        Else
            sql2 = "update mpersonas set PrimerNombre='" & HttpUtility.HtmlDecode(txtPrimerNombre.Text) & "',SegundoNombre='" & HttpUtility.HtmlDecode(txtSegundoNombre.Text) & "', PrimerApellido='" & HttpUtility.HtmlDecode(txtPrimerApellido.Text) & "', SegundoApellido='" & HttpUtility.HtmlDecode(txtSegundoApellido.Text) & "', idgenero='NA', Aspectosdiferenciales='NA', Idetnia='NA' where idpersona='" & Idpersona & "' "
        End If

        conexiontmp = Server.CreateObject("ADODB.Connection")
        conexiontmp.Open(Application("CadVJep"))
        conexiontmp.Execute(sql2)
        Dim auditoria As String
        auditoria = "Se registró el usuario:" & UserNames.Text & " Con el correo:" & Email.Text
        sql2 = " insert into rauditoria (Evento, IdUser) values ('" & auditoria & "','" & UserNames.Text & "')"
        conexiontmp.Execute(sql2)
        conexiontmp.close()
        conexiontmp = Nothing

        If Idpersona = "" Then
            Idpersona = SConsultarExistePersona(UserNames.Text)
        End If
        Return Idpersona
    End Function


    Private Function CompletarNumero(dato As String, largo As String) As String
        Dim dator As String
        Dim falta As String
        Dim i As Integer
        dator = ""
        falta = largo - Len(dato)
        For i = 1 To falta
            dator = dator + "0"
        Next
        dator = RTrim(dator & dato)
        Return dator
    End Function


    Function Enviarmail(ByVal contenido As String, ByVal asunto As String, ByVal mail As String) As String
        'esta funcion envia correo con la cuenta del despacho si esta esta configurada de forma correcta o con la cuenta del portal en caso que el despacho aun no la haya configurado
        'en este caso siempre remite copia al correo del portal

        Dim swcorreo As Integer
        swcorreo = 0
        Dim Servidor As New System.Net.Mail.SmtpClient
        Dim SMTP As String = Application("smtp")
        Dim Usuariomail As String
        Dim Contraseñamail As String

        Usuariomail = Application("mailportal")
        Contraseñamail = Application("kmail")

        Dim puertomail As String = Application("PortalPuerto")
        Servidor.Port = puertomail
        Servidor.EnableSsl = Application("PortalSSLmail")

        Dim mensaje As Integer
        Dim Dnoptions As New System.Net.Mail.DeliveryNotificationOptions
        Dim correo As New System.Net.Mail.MailMessage()
        correo.From = New System.Net.Mail.MailAddress(Usuariomail)
        '(file, MediaTypeNames.Application.Octet)
        correo.To.Add(mail)
        correo.Subject = asunto



        correo.Body = contenido

        Dim correos As String
        Dim posi As String
        Dim i As Integer
        Dim coma As Integer
        correos = mail
        posi = 1

        Do While posi > 0
            coma = 0
            For i = posi To Len(mail)
                If Mid(mail, i, 1) = ";" Then
                    coma = i
                    i = Len(mail)
                End If
            Next
            If coma = 0 Then
                Dim mailp As String
                mailp = Mid(mail, posi, Len(mail))
                If Len(mailp) > 5 Then
                    Try
                        correo.To.Add(mailp)
                    Catch
                        ErrorMessage.Text = "El correo: " & mailp & " no tiene la estructura correcta"
                    End Try

                End If
                posi = 0
            Else
                Dim agregado As String
                agregado = Mid(mail, posi, coma - posi)
                'MsgBox(agregado)
                Try
                    correo.To.Add(agregado)
                Catch
                    ErrorMessage.Text = "El correo: " & agregado & " no tiene la estructura correcta"
                End Try

                posi = coma + 1
            End If
        Loop


        'correo.CC.Add(a)

        correo.DeliveryNotificationOptions = Net.Mail.DeliveryNotificationOptions.OnSuccess Or Net.Mail.DeliveryNotificationOptions.OnFailure
        Servidor.Host = SMTP
        Servidor.Port = puertomail
        Servidor.EnableSsl = Application("PortalSSLmail")
        Servidor.Credentials = New System.Net.NetworkCredential(Usuariomail, Contraseñamail)
        '    Try
        Servidor.Send(correo)
        'mensaje = "Correo enviado con éxito"
        mensaje = 1
        correo.Attachments.Dispose()
        correo.Dispose()
        '   Catch
        'mensaje = 0 no enviado
        '  mensaje = 0
        ' End Try


        Return mensaje
    End Function


    Private Function ConsultarSegundaClave(mail As String) As Integer
        Dim sql2 As String
        Dim conexiontmp As Object
        Dim exito As Integer
        Dim rs3 As Object

        exito = 0
        LblExito.Text = "0"


        sql2 = "select idrol from musuariossistema where emailautorizado='" & Email.Text & "' and iduser='" & Trim(UserNames.Text) & "' and EstadoUser=1  "
        conexiontmp = Server.CreateObject("ADODB.Connection")
        conexiontmp.Open(Application("CadVJep"))
        'Try

        rs3 = conexiontmp.Execute(sql2)

        Do Until rs3.eof
            LblExito.Text = rs3("idrol").value
            rs3.movenext()
        Loop


        conexiontmp.Execute(sql2)
        conexiontmp.close()
        conexiontmp = Nothing
        Return LblExito.Text
    End Function

    Protected Sub CmdEnviarClave_Click(sender As Object, e As EventArgs) Handles CmdEnviarClave.Click
        If Trim(UserNames.Text) <> "" And Trim(txtPrimerNombre.Text) <> "" And Trim(txtPrimerApellido.Text) <> "" Then
            CmdEnviarClave.Enabled = False

            Label1.Text = SValidarEmail(Email.Text)
            If Label1.Text = "" Then
                Dim nombreusuario As String
                nombreusuario = Trim(txtPrimerNombre.Text)
                If Trim(txtSegundoNombre.Text) <> "" Then
                    nombreusuario = nombreusuario & " " & Trim(txtSegundoNombre.Text)
                End If
                nombreusuario = nombreusuario & " " & Trim(txtPrimerApellido.Text)
                If Trim(txtSegundoApellido.Text) <> "" Then
                    nombreusuario = nombreusuario & " " & Trim(txtSegundoApellido.Text)
                End If
                lblnombreusuario.Text = nombreusuario

                ConsultarSegundaClave(Email.Text)
                If LblExito.Text = 0 Then
                    Label1.Text = "Usuario no autorizado, comuniquese con Técnologia"
                Else
                    LblMensajeIngreso.Text = "Asigne su contraseña: La clave debe tener mínimo una longitud de 8 caracteres, con al menos: 1 minúscula, 1 mayúscula, 1 número y 1 caracter no alfanumérico tales como (,.$*#).  No emplee los caracteres: &amp;, comillas y paréntesis"
                    LblMensajeIngreso.Visible = True
                    CmdEnviarClave.Visible = False
                    Registrarse.Visible = True
                    Password.Visible = True
                    ConfirmPassword.Visible = True
                    LblConfirmaPassword.Visible = True
                    LblAsigneContraseña.Visible = True
                    LblMensajeSalida.Visible = True
                End If
            End If


        Else
                Label1.Text = "Debe diligenciar primero los campos para el registro"
        End If
    End Sub



    Protected Sub LsttipoDocu_SelectedIndexChanged(sender As Object, e As EventArgs) Handles LsttipoDocu.SelectedIndexChanged

    End Sub

    Private Function SValidarEmail(mail As String) As String
        Dim sw As Integer
        Dim sql2 As String
        Dim conexion As Object
        Dim rs2 As Object
        Dim sw2 As Integer
        Dim mensaje As String
        mensaje = ""

        conexion = Server.CreateObject("ADODB.Connection")
        conexion.Open(Application("CadUsr"))
        sw = 0
        sw2 = 0

        sql2 = "Select email from aspnetusers WHERE email='" & mail & "'"
        rs2 = conexion.Execute(sql2)
        Do Until rs2.eof
            If Not rs2("email").value Is DBNull.Value Then
                sw = 1
            End If

            rs2.movenext()
        Loop

        sql2 = "Select username from aspnetusers WHERE username='" & UserNames.Text & "'"
        rs2 = conexion.Execute(sql2)
        Do Until rs2.eof
            If Not rs2("username").value Is DBNull.Value Then
                sw2 = 1
            End If

            rs2.movenext()
        Loop

        rs2.close()
        conexion.CLOSE()
        conexion = Nothing
        rs2 = Nothing

        If sw = 1 Then
            mensaje = "Este correo ya aparece registrado, favor verifique que su usuario ya esta registrado con la opción de soporte"
        End If
        If sw2 = 1 Then
            If mensaje = "" Then
                mensaje = "Este usuario ya aparece registrado, favor verifique que si su usuario ya esta registrado con la opción de soporte"

            Else
                mensaje = "Este correo y el usuario ya aparecen registrados, favor verifique que su usuario ya esta registrado con la opción de soporte"

            End If
        End If
        Return mensaje


    End Function
End Class

