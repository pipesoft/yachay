﻿<%@ Page Title="Register" Language="vb"  aspcompat="true" AutoEventWireup="false" enableEventValidation="false"  MasterPageFile="~/Site.Master" CodeBehind="Register.aspx.vb" Inherits="SICA.Register" %>



<%@ Import Namespace="SAAD" %>
<%@ Import Namespace="Microsoft.AspNet.Identity" %>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
     <h2>Registre sus datos para inscribirse</h2>
    
    <table>
      
        <tr>
            <td colspan="4" style="height: 30px">
                <p class="text-danger">  <asp:Literal runat="server" ID="ErrorMessage" />
                </p>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <p class="text-danger">
                <asp:Label ID="Label1" runat="server" ></asp:Label>
                <asp:Label ID="lblnombreusuario" runat="server" visible="false">  </asp:Label>
                </p>
            </td>
        </tr>
     <asp:ValidationSummary runat="server" CssClass="text-danger" />
        
     <tr>
        <td>
            <asp:Label runat="server" CssClass="col-md-10 control-label"  Font-Bold="false" > Tipo de Documento </asp:Label>
        </td>
        <td>
           <asp:ListBox ID="LsttipoDocu"  runat="server" Rows="1" 
                                  AutoPostBack="False"   CssClass="form-control" Width="200px" >
                                  <asp:ListItem Value="CC">Cédula de ciudadanía</asp:ListItem>
                                  <%--<asp:ListItem Value="TI">Tarjeta de identidad</asp:ListItem>--%>
                                  <asp:ListItem Value="CE">Cédula de extranjería</asp:ListItem>
           </asp:ListBox>
        
        </td>
        <td>
            <asp:Label runat="server" AssociatedControlID="UserNames" CssClass="col-md-10 control-label">Número de Documento  </asp:Label>
        </td>
         <td>
               <asp:TextBox runat="server" ID="UserNames" CssClass="form-control" TextMode="Number" MaxLength="15" Width="288px"/>
               <asp:RequiredFieldValidator runat="server" ControlToValidate="UserNames"
                    CssClass="text-danger" ErrorMessage="El documento de identidad es requerido." />
         </td>
      </tr>

    <tr>
        <td>
            <asp:Label runat="server" AssociatedControlID="txtPrimerNombre" CssClass="col-md-10 control-label">Primer Nombre</asp:Label>
            
        </td>
        <td>
                <asp:TextBox runat="server" ID="txtPrimerNombre" CssClass="form-control"  TextMode="SingleLine" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+"  title="entre 2 a 50 caracteres, pueden haber tildes, no emplear números y signos" MaxLength="50"  />

                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtPrimerNombre"
                    CssClass="text-danger" ErrorMessage="El Primer nombre es requerido." />
            
        </td>
        <td>
            <asp:Label runat="server" AssociatedControlID="txtSegundoNombre" CssClass="col-md-10 control-label">Segundo Nombre</asp:Label>
            
        </td>
        <td>
            <asp:TextBox runat="server" ID="txtSegundoNombre" CssClass="form-control"  TextMode="SingleLine" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+"  title="entre 2 a 50 caracteres, pueden haber tildes,  no emplear números y signos" MaxLength="50" />
            
        </td>
    </tr>
    <tr>
        <td>
                  <asp:Label runat="server" AssociatedControlID="txtPrimerApellido" CssClass="col-md-10 control-label">Primer Apellido</asp:Label>
        </td>
        <td>
                <asp:TextBox runat="server" ID="txtPrimerApellido" CssClass="form-control"  TextMode="SingleLine" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+"  title="entre 2 a 50 caracteres, pueden haber tildes,  no emplear números y signos" MaxLength="50" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtPrimerApellido"
                    CssClass="text-danger" ErrorMessage="El Primer apellido es requerido." />
            
        </td>
        <td>
              <asp:Label runat="server" AssociatedControlID="txtSegundoApellido" CssClass="col-md-10 control-label">Segundo Apellido</asp:Label>
            
        </td>
        <td>
                <asp:TextBox runat="server" ID="txtSegundoApellido" CssClass="form-control"  TextMode="SingleLine" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+"  title="entre 2 a 50 caracteres, pueden haber tildes,  no emplear números y signos" MaxLength="50" />
        </td>
    </tr>
<%--    <tr>
        <td>
              <asp:Label runat="server" CssClass="col-md-10 control-list"  Font-Bold="false" > Identidad de género </asp:Label>
      
        </td>
        <td>
             <asp:ListBox ID="lstSexo"  runat="server" Rows="1" 
                                  AutoPostBack="False"   CssClass="form-control" Width="200px">
                                  <asp:ListItem Value="MU">Mujer</asp:ListItem>
                                  <asp:ListItem Value="HO">Hombre</asp:ListItem>
                                  <asp:ListItem Value="LG">LGTBI</asp:ListItem>
                                  </asp:ListBox>
                
        </td>
        <td>
            <asp:Label runat="server" AssociatedControlID="lstDiferenciales" CssClass="col-md-10 control-label">Aspectos diferenciales</asp:Label>
           
        </td>
        <td>
             <asp:ListBox ID="lstDiferenciales"  runat="server" Rows="1" 
                                  AutoPostBack="False"   CssClass="form-control" Width="296px">
                            <asp:ListItem Value="NA">Ninguno</asp:ListItem>
                                  <asp:ListItem Value="MujerCabezaHogar">Mujer cabeza de hogar</asp:ListItem>
                                  <asp:ListItem Value="DiscapacidadFisica">Discapacidad física</asp:ListItem>
                                  <asp:ListItem Value="DiscapacidadCognitiva">Discapacidad cognitiva</asp:ListItem>
                                  <asp:ListItem Value="DiscapacidadPsicosocial">Discapacidad psicosocial</asp:ListItem>
                                  <asp:ListItem Value="DiscapacidadSensorial">Discapacidad sensorial auditiva</asp:ListItem>
                                  <asp:ListItem Value="DiscapacidadVisual">Discapacidad sensorial visual</asp:ListItem>
                                  </asp:ListBox>
                
        </td>
    </tr>

    <tr>
        <td>
            <asp:Label runat="server"  CssClass="col-md-10 control-label">Pertenencia étnica </asp:Label>
           
        </td>
        <td colspan="3">
                <asp:ListBox ID="LstEtnia"  runat="server" Rows="1" 
                                AutoPostBack="False"   CssClass="form-control" Width="296px">
                                <asp:ListItem Value="NA">Sin autoidentificación étnica</asp:ListItem>
                                <asp:ListItem Value="Rom">ROM o gitano</asp:ListItem>
                                <asp:ListItem Value="Afro">Afrodescendiente</asp:ListItem>     
                                <asp:ListItem Value="Raizal">Raizal</asp:ListItem>
                                <asp:ListItem Value="Palenquero">Palenquero</asp:ListItem>
                                <asp:ListItem Value="Indígena">Indígena</asp:ListItem>                                  
                                </asp:ListBox>
        </td>
    </tr>--%>

    <tr>
        <td>
                <asp:Label runat="server" AssociatedControlID="Email" CssClass="col-md-10 control-label">Email </asp:Label>

        </td>
    
        <td colspan="3">
                <asp:TextBox runat="server" ID="Email" CssClass="form-control" TextMode="Email" MaxLength="200" Width="420px" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                    CssClass="text-danger" ErrorMessage="El email es requerido." />
        </td>
    </tr>
    <tr>
        <td colspan="4">
            <asp:Button ID="CmdEnviarClave" onclick="CmdEnviarClave_Click" runat="server"  CssClass="btn btn-default"  Height="47px" Text="Confirmar email" Width="226px"  BackColor="#133e65" Font-Bold="false" ForeColor="White" />
        </td>
    </tr>
    <tr>
        <td >  
            &nbsp;</td>
        <td colspan="2">
            &nbsp;</td>
        <td>
            &nbsp;</td>
    </tr>
         <tr>
        <td>
              <asp:Label id="LblAsigneContraseña" runat="server" AssociatedControlID="Password" CssClass="col-md-2 control-label" Visible="False">Asigne una contraseña</asp:Label>
               
        </td>
        <td>
                  <asp:TextBox runat="server"  MaxLength="20" ID="Password" TextMode="Password" CssClass="form-control" Visible="false" />
            
        </td>
        <td>
              <asp:Label id="LblConfirmaPassword" runat="server" AssociatedControlID="ConfirmPassword" CssClass="col-md-10 control-label" Visible="False" style="left: 0px; top: 25px" >Confirme su contraseña</asp:Label>
          
        </td>
        <td>
                  <asp:TextBox runat="server" ID="ConfirmPassword" TextMode="Password" CssClass="form-control" MaxLength="20" Visible="false" />
          
        </td>
    </tr>
        <tr>
            <td colspan="4">
                <asp:Label id="lblMensajeContraseña" runat="server" CssClass="col-md-2 control-label" Visible="False" ForeColor="#990000" Font-Italic="True">La Contraseña debe tener al menos 1 minúscula, 1 mayúscula, 1 número y 1 caracter no alfanumérico como .,* y una longitud minima de 8 caracteres. No emplee los caracteres: &amp;, comillas y paréntesis</asp:Label>
          
            </td>
        </tr>
   <tr>

       <td colspan="4" style="height: 60px">
                <br />
                <asp:Label id="LblMensajeIngreso" runat="server" CssClass="col-md-10 control-label" Visible="False" style="left: 0px; top: 25px" ForeColor="#990000" Font-Bold="false" ></asp:Label>
        
                <br />
                <asp:Label id="LblMensajeSalida" runat="server" CssClass="col-md-10 control-label" Visible="False" style="left: 0px; top: 25px" ForeColor="#990000" Font-Bold="false" >Una vez presione el botón Registrarse podrá inscribirse a las invitaciones, solo debe iniciar sesión</asp:Label>
        
       </td>
   </tr>
<tr>
    <td colspan="4">
                <asp:Label ID="LblExito" runat="server" Visible="False"></asp:Label>
                <br />
                <br />
                <asp:Button runat="server" OnClick="CreateUser_Click" Text="Registrarse" class="btn btn-primary btn-lg" Height="56px" ID="Registrarse"  Width="153px" Visible="false" BackColor="#133e65" Font-Bold="false" />
    </td>
     </tr>
    </table>

</asp:Content>
