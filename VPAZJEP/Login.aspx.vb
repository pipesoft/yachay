﻿Imports System.Web
Imports System.Web.UI
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Microsoft.Owin.Security
Imports Owin

Partial Public Class Login
    Inherits Page
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        RegisterHyperLink.NavigateUrl = "Register"
        ' Enable this once you have account confirmation enabled for password reset functionality
        ' ForgotPasswordHyperLink.NavigateUrl = "Forgot"
        ' OpenAuthLogin.ReturnUrl = Request.QueryString("ReturnUrl")
        Dim returnUrl = HttpUtility.UrlEncode(Request.QueryString("ReturnUrl"))
        If Not [String].IsNullOrEmpty(returnUrl) Then
            RegisterHyperLink.NavigateUrl += "?ReturnUrl=" & returnUrl
        End If
    End Sub

    Protected Sub LogIn(sender As Object, e As EventArgs)
        If IsValid Then



            ' Validate the user password
            Dim manager = Context.GetOwinContext().GetUserManager(Of ApplicationUserManager)()
            Dim signinManager = Context.GetOwinContext().GetUserManager(Of ApplicationSignInManager)()

            ' This doen't count login failures towards account lockout
            ' To enable password failures to trigger lockout, change to shouldLockout := True
            Dim result = signinManager.PasswordSignIn(UserNames.Text, Password.Text, RememberMe.Checked, shouldLockout:=True)

            Select Case result
                Case SignInStatus.Success
                    IdentityHelper.RedirectToReturnUrl(Request.QueryString("ReturnUrl"), Response)
                    Exit Select
                Case SignInStatus.LockedOut
                    Response.Redirect("/Account/Lockout")
                    Exit Select
                Case SignInStatus.RequiresVerification
                    Response.Redirect(String.Format("/Account/TwoFactorAuthenticationSignIn?ReturnUrl={0}&RememberMe={1}",
                                                    Request.QueryString("ReturnUrl"),
                                                    RememberMe.Checked),
                                      True)
                    Exit Select
                Case Else
                    FailureText.Text = "Intento incorrecto, verifique clave o usuario"
                    ErrorMessage.Visible = True
                    Exit Select
            End Select
        End If
    End Sub

    ''Private Function ConsultarDatosUsuario(usuario As String) As String
    'Dim sql2 As String
    '    Dim conexiontmp As Object
    '    Dim rs3 As Object

    '    Dim esta As Integer
    '    esta = 0
    '    Dim datos As String
    '    datos = ""

    '    'primero se buscan los codigos que requiero eliminar
    '    sql2 = "SELECT       emailautorizado, NombreUsuarioWeb   FROM     UsuariosSistema  WHERE        (iduser = '" & usuario & "')  "
    '    conexiontmp = Server.CreateObject("ADODB.Connection")
    '    conexiontmp.Open(Application("CadVJep"))



    '    rs3 = conexiontmp.Execute(sql2)

    '    Do Until rs3.eof
    '        If Not rs3("emailautorizado").value Is DBNull.Value Then
    '            datos = rs3("emailautorizado").value

    '        End If
    '        If Not rs3("NombreUsuarioWeb").value Is DBNull.Value Then
    '            datos = datos & ";" & rs3("NombreUsuarioWeb").value

    '        End If

    '        rs3.movenext()
    '    Loop


    '    rs3.close
    '    rs3 = Nothing

    '    conexiontmp.close()
    '    conexiontmp = Nothing
    '    Return datos



    'End Function


    ''Private Function SegundaClave(usuario As String, mail As String, nombresuje As String) As Integer
    ''    Dim sql2 As String
    ''    Dim conexiontmp As Object
    ''    Dim contenidomail As String


    ''    'DATOS PARA CONSTRUIR LAS NOTIFICACIONES
    ''    contenidomail = "SECRETARIA EJECUTIVA " + Chr(13) + Chr(10) & "JURISDICCIÓN ESPECIAL PARA LA PAZ " + Chr(13) + Chr(10)
    ''    contenidomail = contenidomail & Chr(13) + Chr(10) + Chr(13) + Chr(10)
    ''    contenidomail = contenidomail & Now().ToLongDateString + Chr(13) + Chr(10) + Chr(13) + Chr(10)
    ''    contenidomail = contenidomail & "Señor(a):" + Chr(13) + Chr(10)
    ''    contenidomail = contenidomail & nombresuje + Chr(13) + Chr(10)


    ''    contenidomail = contenidomail & Chr(13) + Chr(10) & "ASUNTO:SEGUNDA CLAVE ACCESO - " + Application("NombreSistema") + Chr(13) + Chr(10) + Chr(13) + Chr(10)




    ''    Dim esta As Integer
    ''    esta = 0
    ''    Dim clave As String
    ''    Dim auditoria As String

    ''    Randomize()
    ''    ' Generate random value between 1 and 6.
    ''    Dim value As Integer = CInt(Int((999 * Rnd()) + 1))


    ''    clave = completarNumero(value, 3) & Mid(Now.Second, 1, 1)



    ''    'primero se buscan los codigos que requiero eliminar
    ''    sql2 = "INSERT INTO ControlAcceso  (emailautorizado,FechaClave ,clave, ingresos, fallas ) VALUES  "
    ''    sql2 = sql2 + " ('" & mail & "',getdate(),'" & clave & "',0,0)"


    ''    conexiontmp = Server.CreateObject("ADODB.Connection")
    ''    conexiontmp.Open(Application("CadVJep"))


    ''    conexiontmp.Execute(sql2)

    ''    auditoria = "La contraseña asignada para acceso al sistema : " & Application("NombreSistema") & ", es:" & clave
    ''    sql2 = " insert into auditoria (Evento, IdUser) values ('" & auditoria & "','" & usuario & "')"





    ''    conexiontmp.Execute(sql2)


    ''    contenidomail = contenidomail & auditoria

    ''    contenidomail = contenidomail & Chr(13) + Chr(10) + Chr(13) + Chr(10) + Chr(13) + Chr(10) & "Cordialmente, " & Chr(13) + Chr(10)
    ''    contenidomail = contenidomail & Chr(13) + Chr(10) & Chr(13) & Chr(10) & "Secretaria Ejecutiva Jurisdicción Especial para la Paz" & Chr(13) & Chr(10)

    ''    enviarmail(contenidomail, "Autorización de acceso " + Application("NombreSistema"), mail)







    ''    conexiontmp.close()
    ''    conexiontmp = Nothing
    ''    Return esta



    ''End Function
    ''Function enviarmail(ByVal contenido As String, ByVal asunto As String, ByVal mail As String) As String
    ''    'esta funcion envia correo con la cuenta del despacho si esta esta configurada de forma correcta o con la cuenta del portal en caso que el despacho aun no la haya configurado
    ''    'en este caso siempre remite copia al correo del portal

    ''    Dim swcorreo As Integer
    ''    swcorreo = 0
    ''    Dim Servidor As New System.Net.Mail.SmtpClient
    ''    Dim SMTP As String = Application("smtp")
    ''    Dim Usuariomail As String
    ''    Dim Contraseñamail As String

    ''    Usuariomail = Application("mailportal")
    ''    Contraseñamail = Application("kmail")

    ''    Dim puertomail As String = Application("PortalPuerto")
    ''    Servidor.Port = puertomail
    ''    Servidor.EnableSsl = Application("PortalSSLmail")

    ''    Dim mensaje As Integer
    ''    Dim Dnoptions As New System.Net.Mail.DeliveryNotificationOptions
    ''    Dim correo As New System.Net.Mail.MailMessage()
    ''    correo.From = New System.Net.Mail.MailAddress(Usuariomail)
    ''    '(file, MediaTypeNames.Application.Octet)
    ''    correo.To.Add(mail)
    ''    correo.CC.Add(Usuariomail)
    ''    correo.Subject = asunto



    ''    correo.Body = contenido

    ''    Dim correos As String
    ''    Dim posi As String
    ''    Dim i As Integer
    ''    Dim coma As Integer
    ''    correos = mail
    ''    posi = 1

    ''    Do While posi > 0
    ''        coma = 0
    ''        For i = posi To Len(mail)
    ''            If Mid(mail, i, 1) = ";" Then
    ''                coma = i
    ''                i = Len(mail)
    ''            End If
    ''        Next
    ''        If coma = 0 Then
    ''            Dim mailp As String
    ''            mailp = Mid(mail, posi, Len(mail))
    ''            If Len(mailp) > 5 Then
    ''                Try
    ''                    correo.To.Add(mailp)
    ''                Catch
    ''                    LblErrores.Text = "El correo: " & mailp & " no tiene la estructura correcta"
    ''                End Try

    ''            End If
    ''            posi = 0
    ''        Else
    ''            Dim agregado As String
    ''            agregado = Mid(mail, posi, coma - posi)
    ''            'MsgBox(agregado)
    ''            Try
    ''                correo.To.Add(agregado)
    ''            Catch
    ''                LblErrores.Text = "El correo: " & agregado & " no tiene la estructura correcta"
    ''            End Try

    ''            posi = coma + 1
    ''        End If
    ''    Loop


    ''    'correo.CC.Add(a)

    ''    correo.DeliveryNotificationOptions = Net.Mail.DeliveryNotificationOptions.OnSuccess Or Net.Mail.DeliveryNotificationOptions.OnFailure
    ''    Servidor.Host = SMTP
    ''    Servidor.Port = puertomail
    ''    Servidor.EnableSsl = Application("PortalSSLmail")
    ''    Servidor.Credentials = New System.Net.NetworkCredential(Usuariomail, Contraseñamail)
    ''    '    Try
    ''    Servidor.Send(correo)
    ''    'mensaje = "Correo enviado con éxito"
    ''    mensaje = 1
    ''    correo.Attachments.Dispose()
    ''    correo.Dispose()
    ''    '   Catch
    ''    'mensaje = 0 no enviado
    ''    '  mensaje = 0
    ''    ' End Try


    ''    Return mensaje
    ''End Function

    ''Private Function completarNumero(dato As String, largo As String) As String
    'Dim dator As String
    '    Dim falta As String
    '    Dim i As Integer
    '    dator = ""
    '    falta = largo - Len(dato)
    '    For i = 1 To falta
    '        dator = dator + "0"
    '    Next
    '    dator = RTrim(dator & dato)
    '    Return dator
    'End Function



End Class
