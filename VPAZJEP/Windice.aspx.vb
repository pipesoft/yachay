﻿Imports Microsoft.AspNet.Identity
Imports System.IO
Imports System.Drawing
Imports itextsharp
Imports itextsharp.text
Imports itextsharp.text.pdf
Imports System.Data.SqlClient
Imports System.Data


Public Class Windice
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        MaintainScrollPositionOnPostBack = True
        If Not Page.IsPostBack Then
            PopulateRootLevel()

            Dim acceso As Integer
            acceso = 0
            LblRol.Text = ConsultarRol()
            If LblRol.Text <> "" Then
                acceso = 1
                Lblaviso.Visible = True

                PInicio.Visible = True
                POpciones.Visible = True
                LbtAgregar.Visible = False

                GVDatos2.DataSource = TConsultarNovedades()
                GVDatos2.DataBind()
                'pintar las novedades
                ConsultarNovedades()


                If LblRol.Text = Application("FuentesYachay") Or LblRol.Text = Application("EditorBasico") Or LblRol.Text = Application("EditorJuridico") Then
                    LbtAgregar.Visible = True
                End If
            Else
                RegisterHyperLink.Visible = True
                IngreseHyperLink.Visible = True
                PInicio.Visible = False
                If Context.User.Identity.GetUserName() = "" Then
                    LblMensajes.Text = "Debe iniciar sesión para acceder a la utilidad"
                Else
                    LblMensajes.Text = "No tiene acceso al sistema, favor solicitar activación de su usuario"
                End If

            End If

        End If

    End Sub

    Private Function ConsultarRol() As String
        Dim conexion As Object
        Dim rs2 As Object
        Dim sql2 As String
        Dim usuario As String
        Dim rol As String
        rol = ""
        conexion = Server.CreateObject("ADODB.Connection")
        conexion.Open(Application("CadVJep"))
        usuario = Context.User.Identity.GetUserName()

        If usuario IsNot Nothing Then

            sql2 = "Select NOMBREROL FROM mUsuariosSistema U, mRolSistema R WHERE R.IDROL=U.IDROL And U.EstadoUser=1 And U.IDUSER='" & usuario & "'"
            rs2 = conexion.Execute(sql2)

            Do Until rs2.eof
                If Not rs2(0).value Is DBNull.Value Then
                    rol = rs2("nombrerol").value

                End If

                rs2.movenext()
            Loop


        End If

        conexion.close()
        conexion = Nothing
        rs2 = Nothing
        Return rol
    End Function

    Function CreateDataSource(ByVal sql As String) As ICollection
        Dim conexiontmp As Object
        Dim rs1 As Object
        conexiontmp = Server.CreateObject("ADODB.Connection")
        conexiontmp.Open(Application("CadVJep"))
        rs1 = conexiontmp.Execute(sql)
        Dim dt As DataTable = New DataTable()
        ' Define the columns of the table.
        dt.Columns.Add(New DataColumn("A", GetType(String)))
        dt.Columns.Add(New DataColumn("B", GetType(String)))
        Do While Not rs1.EOF()
            If rs1.fields(1).value() Is DBNull.Value Then
                dt.Rows.Add(CreateRow(rs1.fields(0).value(), "", dt))
            Else
                dt.Rows.Add(CreateRow(rs1.fields(0).value(), rs1.fields(1).value(), dt))
            End If

            rs1.movenext()
        Loop
        rs1.close()
        conexiontmp.close()
        conexiontmp = Nothing
        rs1 = Nothing

        Dim dv As DataView = New DataView(dt)
        Return dv

    End Function

    Function CreateRow(ByVal Text As String, ByVal Value As String, ByVal dt As DataTable) As DataRow
        Dim dr As DataRow = dt.NewRow()
        dr(0) = Text
        dr(1) = Value

        Return dr

    End Function

    Protected Sub ImgBuscar_Click(sender As Object, e As ImageClickEventArgs) Handles ImgBuscar.Click
        BuscarFuentes("G", "", "", 0)
    End Sub

    Private Sub BuscarFuentes(tipo As String, clasificador As String, tipoDocumentoFuente As String, esEntidad As Boolean)
        MultiView1.SetActiveView(VResultados)
        ' "G" consulta general
        ' "E" consulta una sola fuente
        TxtBuscar.Text = Trim(TxtBuscar.Text)

        GVResultados.DataSource = TConsultarFuentes(tipo, clasificador, tipoDocumentoFuente, esEntidad)
        GVResultados.DataBind()

        If GVResultados.Rows.Count > 0 Then
            Dim y As Integer
            Dim kb As Integer
            Dim kg As Integer

            y = 1
            For i As Integer = 0 To GVResultados.Rows.Count - 1
                GVResultados.Rows(i).Font.Size = 11
                ' GVResultados.Rows(i).Cells(1).Visible = False
                If y = 1 Then
                    GVResultados.Rows(i).ForeColor = Color.FromName("Blue")
                    GVResultados.Rows(i).Font.Underline = True
                    GVResultados.Rows(i).Font.Size = 12
                    GVResultados.Rows(i).Cells(1).Visible = True
                    GVResultados.Rows(i).Cells(1).HorizontalAlign = HorizontalAlign.Left
                    kb = y + 4
                End If
                If y = 2 Then
                    GVResultados.Rows(i).ForeColor = Color.FromName("Green")
                    GVResultados.Rows(i).Font.Size = 10
                    GVResultados.Rows(i).Cells(1).HorizontalAlign = HorizontalAlign.Left
                    kg = y + 4
                End If
                If y = kb Then
                    GVResultados.Rows(i).ForeColor = Color.FromName("Blue")
                    GVResultados.Rows(i).Font.Underline = True
                    GVResultados.Rows(i).Font.Size = 12
                    GVResultados.Rows(i).Cells(1).HorizontalAlign = HorizontalAlign.Left
                    '    GVResultados.Rows(i).Cells(1).Visible = True
                    kb = kb + 4
                End If
                If y = kg Then
                    GVResultados.Rows(i).ForeColor = Color.FromName("Green")
                    GVResultados.Rows(i).Cells(1).HorizontalAlign = HorizontalAlign.Left
                    kg = kg + 4
                End If
                y = y + 1
            Next
        End If



    End Sub

    Protected Sub TxtBuscar_TextChanged(sender As Object, e As EventArgs) Handles TxtBuscar.TextChanged
        'BuscarFuentes()
    End Sub

    Function TConsultarFuentesReporte() As DataTable

        Dim conexiontmp As Object
        Dim conexiontmp2 As Object
        Dim rs1 As Object
        Dim rs2 As Object
        Dim sql As String
        Dim sql1 As String
        Dim iReg As Integer
        Dim Sentidad As String
        Dim SnombreFuente As String
        Dim SnombreArchivo As String        'NUEVO
        Dim SautorFuente As String          'NUEVO        
        Dim StipoFuente As String           'NUEVO    
        Dim Sdesagreagacion As String       'NUEVO    
        Dim StieneSoporte As String         'NUEVO    
        Dim SresponsableGestion As String   'NUEVO    
        Dim SidiomaFuente As String         'NUEVO    
        Dim SradicadoOrfeo As String        'NUEVO    
        Dim Sconsecutivo As String          'NUEVO  
        Dim SIdFuente As String             'NUEVO  
        Dim SfechaCorte As String
        Dim SfechaRecepcion As String
        Dim SformasAcceso As String
        Dim SresponsableFuente As String
        Dim ScantidadRegistrosAutor As String
        Dim SnumeroVariables As String
        Dim Scontenido As String
        Dim SdescripcionFuente As String
        Dim StemasRelacionados As String
        Dim Stemporalidad As String
        Dim SvariablesRelevantes As String
        Dim SestaRevisada As String
        Dim SesPrimaria As String
        Dim SesEstructurada As String
        Dim SUniverso As String
        Dim SLimitaciones As String
        Dim SPorcentajeAplicabilidad As String
        Dim SaccesiblePara As String
        Dim sensibles As String
        Dim StipoInformacion As String
        Dim SfundamentoJuridico As String
        Dim SobjetivoJuridico As String
        Dim SfundamentoConstLegal As String
        Dim IdClasificadorFuente As Integer
        Dim SclasificadoresFuenteAcumulador As String
        Dim SclasificadorCrimen As String
        Dim SclasificadorActorArmado As String
        Dim SclasificadorCoberturaDepartamental As String
        Dim SclasificadorContieneEnfoqueDiferencial As String
        Dim SclasificadorContieneEnfoqueTerritorial As String
        Dim IdClasificadorCrimen As Integer
        Dim IdClasificadorActorArmado As Integer
        Dim IdClasificadorCoberturaDepartamental As Integer
        Dim IdClasificadorContieneEnfoqueDiferencial As Integer
        Dim IdClasificadorContieneEnfoqueTerritorial As Integer

        Dim SnombreUsuario As String

        iReg = 0
        Dim dtReporte As DataTable = New DataTable()

        dtReporte.Columns.Add(New DataColumn("Consecutivo", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("IdFuente", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("NombreFuente", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("EntidadRemite", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("DescripcionFuente", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("EntidadGeneradora", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("TipoFormatoFuente", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("NivelDesagregacion", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("ResponsableGestion", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("IdiomaFuente", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("RadicadoOrfeo", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("FormaAcceso", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("ResponsableCustodio", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("UniversoEstudio", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("EsEstructurada", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("Formato", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("Temporalidad", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("EstaRevisada", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("FechaActualizacion", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("FechaRecepcionJEP", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("TieneDatosSensibles", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("TipoInformacion", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("FundamentoJuridico", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("ObjetivoJuridico", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("FundamentoLegal", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("Crimen", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("ActorArmado", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("CoberturaDepartamental", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("EnfoqueDiferencial", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("EnfoqueTerritorial", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("NombreUsuario", GetType(String)))

        sql = " SELECT fne.consecutivo, id_fuente_estructurada as id_fuentes, nombre_fuente, nombre_subfuente as nombre_subfuente_tipo_f, nombre_archivo, autor_fuente, vtf.Descripcion as tipo_fuente, vdt.Descripcion as desagreagacion, vds.Descripcion as tieneSoporte, vrg.Descripcion as responsableGestion, vi.descripcion_idioma as idiomaFuente,  fe.sobre_forma_ingreso as radicadoOrfeo, formato, numero_de_variables as numero_variables, cantidad_registros, fne.descripcion_fuente as descripcion_registros_autor, tamaño_bytes as tamaño_bytes_ubicacion, universo_estudio, limitaciones, fecha_recepcion, fecha_corte, fecha_carga, anexos_documentales, sobre_estructura, temporalidad_ano_inicia, temporalidad_ano_termina, esta_revisada, porcentaje_aplicabilidad, ec.entidad_nombre as entidad_nombre, fne.descripcion_fuente as descripcion_fuente, vc.nombre_custodio as responsable_custodio, accesible_para, es_estructurada, fi.descripcion as forma_ingreso, fa.descripcion as forma_acceso "
        sql = sql & " , ft.descripcion as forma_tradiccion, fs.descripcion as forma_soporte, ti.Descripcion as tipo_informacion, es_fuente_primaria, tiene_datos_sensibles, fundamento_juridico,objetivo_juridico , fundamento_const_legal, ampliacion_justificacion_ds, tiene_victimas, tiene_responsables,fne.id_fuente as id_fuente, usuario.NombreUsuario "
        sql = sql & " FROM mFuentes fne left join mentidades_convenios ec on ec.id_entidad = fne.id_entidad  "
        sql = sql & " left join vFormas_ingreso fi on fi.id_forma_ingreso=fne.id_forma_ingreso "
        sql = sql & " left join vFormas_acceso fa on fa.id_forma_acceso=fne.id_forma_acceso "
        sql = sql & " left join vFormas_tradiccion ft on ft.id_tradiccion=fne.id_tradiccion "
        sql = sql & " left join vTipos_informacion ti on ti.Id_tipo_informacion=fne.id_tipo_informacion "
        sql = sql & " left join mFuentes_estructuradas fe on fe.id_fuente=fne.Id_fuente "
        sql = sql & " left join vformas_soporte fs on fs.id_tipo_soporte=fe.id_tipo_soporte "
        sql = sql & " Left join vfundamentos_juridicos_ti vfj on vfj.id_fundamento_juridico=fne.id_fundamento_juridico_ti "
        sql = sql & " left join vObjetivos_legitimos_ti volt on volt.id_objetivo_juridico=fne.id_objetivo_legitimo_ti "
        sql = sql & " left join vFundamentos_const_legal_ti vfc on vfc.id_fundamento_const_legal=fne.id_fundamento_constitucional_ti "
        sql = sql & " left join vcustodios vc on vc.id_custodio=fe.id_custodio "
        sql = sql & " left Join vFuentes_tipoFormato vtf on vtf.Id_tipo_formato = fe.id_tipo_fuente "
        sql = sql & " left Join vFuentes_desagregacionTerritorial vdt on vdt.Id_desagregacion_territorial = fe.id_nivel_desagrega_territorial "
        sql = sql & " left Join vFuentes_opcionDocumentosSoporte vds on vds.Id_opcion_documento = fe.id_documento_soporte "
        sql = sql & " left Join vFuentes_responsableGestion vrg on vrg.Id_reponsable_gestion = fe.id_responsable_gestion "
        sql = sql & " left Join vIdiomas vi on vi.id_idioma = fne.id_idioma "
        sql = sql & " Left Join mUsuariosSistema usuario on usuario.Iduser = fe.id_usuario "
        sql = sql & " where  fe.esta_activa=1 "
        sql = sql & " union "
        sql = sql & " SELECT fne.consecutivo, id_fuente_noestructurada, nombre_fuente, vft.descripcion as tipo_fuente, nombre_archivo, autor_fuente, vtf.Descripcion as tipo_fuente, vdt.Descripcion as desagreagacion, vds.Descripcion as tieneSoporte, vrg.Descripcion as responsableGestion, vi.descripcion_idioma as idiomaFuente,  fe.sobre_forma_ingreso as radicadoOrfeo, formato, '', '', autor_fuente,vu.ubicacion, '','', fecha_recepcion, '', fecha_carga, '', '',temporalidad_ano_inicia,temporalidad_ano_termina, esta_revisada, porcentaje_aplicabilidad,  ec.entidad_nombre as entidad_nombre, fne.descripcion_fuente as descripcion_fuente, vc.nombre_custodio as responsable_custodio, accesible_para, es_estructurada, fi.descripcion as forma_ingreso, fa.descripcion as forma_acceso "
        sql = sql & " , ft.descripcion as forma_tradiccion, fs.descripcion as forma_soporte, ti.Descripcion as tipo_informacion, es_fuente_primaria, tiene_datos_sensibles, fundamento_juridico,objetivo_juridico , fundamento_const_legal,ampliacion_justificacion_ds,tiene_victimas, tiene_responsables, fne.id_fuente as id_fuente, usuario.NombreUsuario "
        sql = sql & " FROM mFuentes fne left join mentidades_convenios ec on ec.id_entidad = fne.id_entidad  "
        sql = sql & " left join vFormas_ingreso fi on fi.id_forma_ingreso=fne.id_forma_ingreso  "
        sql = sql & " left join vFormas_acceso fa on fa.id_forma_acceso=fne.id_forma_acceso  "
        sql = sql & " left join vFormas_tradiccion ft on ft.id_tradiccion=fne.id_tradiccion  "
        sql = sql & " left join vTipos_informacion ti on ti.Id_tipo_informacion=fne.id_tipo_informacion "
        sql = sql & " left join mFuentes_no_estructuradas fe on fe.id_fuente=fne.Id_fuente "
        sql = sql & " left join vUbicaciones vu on vu.id_ubicacion=fe.id_ubicacion "
        sql = sql & " left join vformas_soporte fs on fs.id_tipo_soporte=fe.id_tipo_soporte "
        sql = sql & " left join vfuentes_tipo vft on vft.id_tipo_fuente =fe.id_tipo_fuente "
        sql = sql & " left join vfundamentos_juridicos_ti vfj on vfj.id_fundamento_juridico=fne.id_fundamento_juridico_ti "
        sql = sql & " left join vObjetivos_legitimos_ti volt on volt.id_objetivo_juridico=fne.id_objetivo_legitimo_ti "
        sql = sql & " left join vFundamentos_const_legal_ti vfc on vfc.id_fundamento_const_legal=fne.id_fundamento_constitucional_ti "
        sql = sql & " left join vcustodios vc on vc.id_custodio=fe.id_custodio "
        sql = sql & " left Join vFuentes_tipoFormato vtf on vtf.Id_tipo_formato = fe.id_tipo_fuente "
        sql = sql & " left Join vFuentes_desagregacionTerritorial vdt on vdt.Id_desagregacion_territorial = fe.id_nivel_desagrega_territorial "
        sql = sql & " left Join vFuentes_opcionDocumentosSoporte vds on vds.Id_opcion_documento = fe.id_documento_soporte "
        sql = sql & " left Join vFuentes_responsableGestion vrg on vrg.Id_reponsable_gestion = fe.id_responsable_gestion "
        sql = sql & " left Join vIdiomas vi on vi.id_idioma = fne.id_idioma "
        sql = sql & " Left Join mUsuariosSistema usuario on usuario.Iduser = fe.id_usuario"
        sql = sql & " where  fe.esta_activa=1 "
        sql = sql & " ORDER BY fne.consecutivo DESC "

        conexiontmp = Server.CreateObject("ADODB.Connection")
        conexiontmp.Open(Application("CadVJep"))

        rs1 = conexiontmp.Execute(sql)

        Do While Not rs1.EOF()
            iReg = iReg + 1
            Sentidad = ""
            SnombreFuente = ""
            SfechaCorte = ""
            SfechaRecepcion = ""
            SformasAcceso = ""
            SresponsableFuente = ""
            ScantidadRegistrosAutor = ""
            SnumeroVariables = ""
            Scontenido = ""
            SdescripcionFuente = ""
            StemasRelacionados = ""
            Stemporalidad = ""
            SvariablesRelevantes = ""
            SestaRevisada = ""
            SesPrimaria = ""
            SesEstructurada = ""
            SUniverso = ""
            SLimitaciones = ""
            SPorcentajeAplicabilidad = ""
            SaccesiblePara = ""
            SautorFuente = String.Empty
            SnombreArchivo = String.Empty
            StipoFuente = String.Empty
            Sdesagreagacion = String.Empty
            StieneSoporte = String.Empty
            SresponsableGestion = String.Empty
            SidiomaFuente = String.Empty
            SradicadoOrfeo = String.Empty
            sensibles = String.Empty
            StipoInformacion = String.Empty
            SfundamentoJuridico = String.Empty
            SobjetivoJuridico = String.Empty
            SfundamentoConstLegal = String.Empty
            SIdFuente = String.Empty
            Sconsecutivo = String.Empty
            SclasificadoresFuenteAcumulador = String.Empty
            SclasificadorCrimen = String.Empty
            SclasificadorActorArmado = String.Empty
            SclasificadorCoberturaDepartamental = String.Empty
            SclasificadorContieneEnfoqueDiferencial = String.Empty
            SclasificadorContieneEnfoqueTerritorial = String.Empty
            IdClasificadorCrimen = 33
            IdClasificadorActorArmado = 34
            IdClasificadorCoberturaDepartamental = 120
            IdClasificadorContieneEnfoqueDiferencial = 121
            IdClasificadorContieneEnfoqueTerritorial = 145
            SnombreUsuario = String.Empty

            If Not rs1.fields("id_fuentes").value() Is DBNull.Value Then
                SIdFuente = rs1.fields("id_fuentes").value()
            End If

            If Not rs1.fields("consecutivo").value() Is DBNull.Value Then
                Sconsecutivo = rs1.fields("consecutivo").value()
            End If


            If Not rs1.fields("accesible_para").value() Is DBNull.Value Then
                SaccesiblePara = rs1.fields("accesible_para").value()
            End If

            If Not rs1.fields("porcentaje_aplicabilidad").value() Is DBNull.Value Then
                SPorcentajeAplicabilidad = rs1.fields("porcentaje_aplicabilidad").value()
            End If

            If Not rs1.fields("limitaciones").value() Is DBNull.Value Then
                SLimitaciones = rs1.fields("limitaciones").value()
            End If

            If Not rs1.fields("universo_estudio").value() Is DBNull.Value Then
                SUniverso = rs1.fields("universo_estudio").value()
            End If

            If Not rs1.fields("es_estructurada").value() Is DBNull.Value Then
                If rs1.fields("es_estructurada").value() = True Then
                    SesEstructurada = "Si"
                End If
            End If

            If Not rs1.fields("es_fuente_primaria").value() Is DBNull.Value Then
                If rs1.fields("es_fuente_primaria").value() = True Then
                    SesPrimaria = "Si"
                End If
            End If


            If Not rs1.fields("esta_revisada").value() Is DBNull.Value Then
                If rs1.fields("esta_revisada").value() = True Then
                    SestaRevisada = "Si"
                End If
            End If

            If Not rs1.fields("tiene_responsables").value() Is DBNull.Value Then
                If rs1.fields("tiene_responsables").value() = True Then
                    SvariablesRelevantes = "responsables o autores"
                End If

            End If

            If Not rs1.fields("tiene_victimas").value() Is DBNull.Value Then
                If rs1.fields("tiene_victimas").value() = True Then
                    If SvariablesRelevantes <> "," Then

                    End If
                    SvariablesRelevantes = " víctimas"
                End If
            End If


            If Not rs1.fields("entidad_nombre").value() Is DBNull.Value Then
                Sentidad = rs1.fields("entidad_nombre").value() & " - "
            End If

            If Not rs1.fields("nombre_fuente").value() Is DBNull.Value Then
                SnombreFuente = rs1.fields("nombre_fuente").value()
                'LblFuenteNombre.Text = rs1.fields("nombre_fuente").value()

                If Not rs1.fields("nombre_subfuente_tipo_f").value() Is DBNull.Value Then
                    SnombreFuente = SnombreFuente & "-" & rs1.fields("nombre_subfuente_tipo_f").value()
                End If
            End If


            '**************************************************************
            If Not rs1.fields("nombre_archivo").value() Is DBNull.Value Then
                SnombreArchivo = rs1.fields("nombre_archivo").value()
            End If

            If Not rs1.fields("autor_fuente").value() Is DBNull.Value Then
                SautorFuente = rs1.fields("autor_fuente").value()
            End If


            If Not rs1.fields("tipo_fuente").value() Is DBNull.Value Then
                StipoFuente = rs1.fields("tipo_fuente").value()
            End If

            If Not rs1.fields("desagreagacion").value() Is DBNull.Value Then
                Sdesagreagacion = rs1.fields("desagreagacion").value()
            End If

            If Not rs1.fields("tieneSoporte").value() Is DBNull.Value Then
                StieneSoporte = rs1.fields("tieneSoporte").value()
            End If

            If Not rs1.fields("responsableGestion").value() Is DBNull.Value Then
                SresponsableGestion = rs1.fields("responsableGestion").value()
            End If

            If Not rs1.fields("idiomaFuente").value() Is DBNull.Value Then
                SidiomaFuente = rs1.fields("idiomaFuente").value()
            End If

            If Not rs1.fields("radicadoOrfeo").value() Is DBNull.Value Then
                SradicadoOrfeo = rs1.fields("radicadoOrfeo").value()
            End If

            '**************************************************************



            If Not rs1.fields("forma_acceso").value() Is DBNull.Value Then
                SformasAcceso = rs1.fields("forma_acceso").value()
            End If

            If Not rs1.fields("responsable_custodio").value() Is DBNull.Value Then
                SresponsableFuente = rs1.fields("responsable_custodio").value()
            End If

            If Not rs1.fields("cantidad_registros").value() Is DBNull.Value Then
                ScantidadRegistrosAutor = rs1.fields("cantidad_registros").value()
            End If
            If Not rs1.fields("numero_variables").value() Is DBNull.Value Then
                SnumeroVariables = rs1.fields("numero_variables").value()
            End If

            If Not rs1.fields("formato").value() Is DBNull.Value Then
                Scontenido = " Formato:" & rs1.fields("formato").value()
            End If
            If Not rs1.fields("fecha_corte").value() Is DBNull.Value Then
                SfechaCorte = rs1.fields("fecha_corte").value()
            End If

            If Not rs1.fields("fecha_recepcion").value() Is DBNull.Value Then
                If rs1.fields("fecha_recepcion").value() <> "01/01/1900" Then
                    SfechaRecepcion = rs1.fields("fecha_recepcion").value()
                End If

            End If


            '<asp:LinkButton id="LinkButton2" CommandName="Show" text='<%# Eval("idFuente")%>'  runat="server"/>

            If Not rs1.fields("descripcion_fuente").value() Is DBNull.Value Then
                SdescripcionFuente = rs1.fields("descripcion_fuente").value()
            End If

            If Not rs1.fields("temporalidad_ano_inicia").value() Is DBNull.Value Then
                Stemporalidad = rs1.fields("temporalidad_ano_inicia").value()
            End If

            If Not rs1.fields("temporalidad_ano_termina").value() Is DBNull.Value Then
                Stemporalidad = Stemporalidad & " a " & rs1.fields("temporalidad_ano_termina").value()
            End If

            If Not rs1.fields("tiene_datos_sensibles").value() Is DBNull.Value Then
                sensibles = "No"
                If rs1.fields("tiene_datos_sensibles").value() = True Then
                    sensibles = "Si"
                End If
            End If

            If Not rs1.fields("tipo_informacion").value() Is DBNull.Value Then
                StipoInformacion = rs1.fields("tipo_informacion").value()
            End If

            If Not rs1.fields("fundamento_juridico").value() Is DBNull.Value Then
                SfundamentoJuridico = rs1.fields("fundamento_juridico").value()
            End If

            If Not rs1.fields("objetivo_juridico").value() Is DBNull.Value Then
                SobjetivoJuridico = rs1.fields("objetivo_juridico").value()
            End If

            If Not rs1.fields("fundamento_const_legal").value() Is DBNull.Value Then
                SfundamentoConstLegal = rs1.fields("fundamento_const_legal").value()
            End If

            If Not rs1.fields("NombreUsuario").value() Is DBNull.Value Then
                SnombreUsuario = rs1.fields("NombreUsuario").value()
            End If

            sql1 = "SELECT CLA.descripcion clasificadorFuente, CLA_PADRE.descripcion, CLA_PADRE.id_clasificador FROM [dbo].[aFuentes_clasificadores] FUE_CLA"
            sql1 = sql1 + " INNER JOIN [dbo].[vClasificadores] CLA ON FUE_CLA.id_clasificador = CLA.id_clasificador"
            sql1 = sql1 + " INNER Join [dbo].[vClasificadores] CLA_PADRE ON CLA_PADRE.id_clasificador = CLA.id_clasificador_padre"
            sql1 = sql1 + " WHERE FUE_CLA.Id_fuente_est_nest = '" & SIdFuente & "' AND CLA.estado = 1 AND FUE_CLA.estado = 1"

            conexiontmp2 = Server.CreateObject("ADODB.Connection")
            conexiontmp2.Open(Application("CadVJep"))
            rs2 = conexiontmp2.Execute(sql1)
            Do While Not rs2.EOF()
                If Not rs2.fields("id_clasificador").value() Is DBNull.Value Then
                    IdClasificadorFuente = rs2.fields("id_clasificador").value()
                End If
                If (IdClasificadorFuente = IdClasificadorCrimen) Then
                    If Not rs2.fields("clasificadorFuente").value() Is DBNull.Value Then
                        If SclasificadorCrimen <> String.Empty Then
                            SclasificadorCrimen = SclasificadorCrimen + "," + rs2.fields("clasificadorFuente").value()
                        Else
                            SclasificadorCrimen = rs2.fields("clasificadorFuente").value()
                        End If
                    End If
                End If
                If (IdClasificadorFuente = IdClasificadorActorArmado) Then
                    If Not rs2.fields("clasificadorFuente").value() Is DBNull.Value Then
                        If SclasificadorActorArmado <> String.Empty Then
                            SclasificadorActorArmado = SclasificadorActorArmado + "," + rs2.fields("clasificadorFuente").value()
                        Else
                            SclasificadorActorArmado = rs2.fields("clasificadorFuente").value()
                        End If
                    End If
                End If
                If (IdClasificadorFuente = IdClasificadorCoberturaDepartamental) Then
                    If Not rs2.fields("clasificadorFuente").value() Is DBNull.Value Then
                        If SclasificadorCoberturaDepartamental <> String.Empty Then
                            SclasificadorCoberturaDepartamental = SclasificadorCoberturaDepartamental + "," + rs2.fields("clasificadorFuente").value()
                        Else
                            SclasificadorCoberturaDepartamental = rs2.fields("clasificadorFuente").value()
                        End If
                    End If
                End If
                If (IdClasificadorFuente = IdClasificadorContieneEnfoqueDiferencial) Then
                    If Not rs2.fields("clasificadorFuente").value() Is DBNull.Value Then
                        If SclasificadorContieneEnfoqueDiferencial <> String.Empty Then
                            SclasificadorContieneEnfoqueDiferencial = SclasificadorContieneEnfoqueDiferencial + "," + rs2.fields("clasificadorFuente").value()
                        Else
                            SclasificadorContieneEnfoqueDiferencial = rs2.fields("clasificadorFuente").value()
                        End If
                    End If
                End If
                If (IdClasificadorFuente = IdClasificadorContieneEnfoqueTerritorial) Then
                    If Not rs2.fields("clasificadorFuente").value() Is DBNull.Value Then
                        If SclasificadorContieneEnfoqueTerritorial <> String.Empty Then
                            SclasificadorContieneEnfoqueTerritorial = SclasificadorContieneEnfoqueTerritorial + "," + rs2.fields("clasificadorFuente").value()
                        Else
                            SclasificadorContieneEnfoqueTerritorial = rs2.fields("clasificadorFuente").value()
                        End If
                    End If
                End If
                rs2.movenext()
            Loop
            rs2.close()
            conexiontmp2.close()
            conexiontmp2 = Nothing
            rs2 = Nothing

            dtReporte.Rows.Add(CreateRowReporte(Sconsecutivo, SIdFuente, SnombreFuente, Sentidad, SdescripcionFuente, SautorFuente, StipoFuente, Sdesagreagacion, SresponsableGestion, SidiomaFuente,
                                                SradicadoOrfeo, SformasAcceso, SresponsableFuente, SUniverso, SesEstructurada, Scontenido, Stemporalidad, SestaRevisada, SfechaCorte,
                                                SfechaRecepcion, sensibles, StipoInformacion, SfundamentoJuridico, SobjetivoJuridico,
                                                SfundamentoConstLegal,
                                                SclasificadorCrimen,
                                                SclasificadorActorArmado,
                                                SclasificadorCoberturaDepartamental,
                                                SclasificadorContieneEnfoqueDiferencial,
                                                SclasificadorContieneEnfoqueTerritorial,
                                                SnombreUsuario, dtReporte))
            rs1.movenext()

            Loop

        rs1.close()
        conexiontmp.close()
        conexiontmp = Nothing
        rs1 = Nothing

        Return dtReporte

    End Function
    Function CreateRowReporte(ByVal sConsecutivo As String, ByVal sIdFuente As String, ByVal sNombreFuente As String, ByVal sEntidadRemite As String, ByVal sDescripcionFuente As String, ByVal sEntidadGeneradora As String, ByVal sTipoFormatoFuente As String,
                              ByVal sNivelDesagregacion As String, ByVal sResponsableGestion As String, ByVal sIdiomaFuente As String, ByVal sRadicadoOrfeo As String, ByVal sFormaAcceso As String,
                              ByVal sResponsableCustodio As String, ByVal sUniversoEstudio As String, ByVal sEsEstructurada As String, ByVal sFormato As String, ByVal sTemporalidad As String,
                              ByVal sEstaRevisada As String, ByVal sFechaActualizacion As String, ByVal sFechaRecepcionJEP As String, ByVal sTieneDatosSensibles As String, ByVal sTipoInformacion As String,
                              ByVal sFundamentoJuridico As String, ByVal sObjetivoJuridico As String, ByVal sFundamentoLegal As String,
                              ByVal SclasificadorCrimen As String,
                              ByVal SclasificadorActorArmado As String,
                              ByVal SclasificadorCoberturaDepartamental As String,
                              ByVal SclasificadorContieneEnfoqueDiferencial As String,
                              ByVal SclasificadorContieneEnfoqueTerritorial As String,
                              ByVal sUsuario As String,
                              ByVal dt As DataTable) As DataRow
        Dim dr As DataRow = dt.NewRow()
        dr(0) = sConsecutivo
        dr(1) = sIdFuente
        dr(2) = sNombreFuente
        dr(3) = sEntidadRemite
        dr(4) = sDescripcionFuente
        dr(5) = sEntidadGeneradora
        dr(6) = sTipoFormatoFuente
        dr(7) = sNivelDesagregacion
        dr(8) = sResponsableGestion
        dr(9) = sIdiomaFuente
        dr(10) = sRadicadoOrfeo
        dr(11) = sFormaAcceso
        dr(12) = sResponsableCustodio
        dr(13) = sUniversoEstudio
        dr(14) = sEsEstructurada
        dr(15) = sFormato
        dr(16) = sTemporalidad
        dr(17) = sEstaRevisada
        dr(18) = sFechaActualizacion
        dr(19) = sFechaRecepcionJEP
        dr(20) = sTieneDatosSensibles
        dr(21) = sTipoInformacion
        dr(22) = sFundamentoJuridico
        dr(23) = sObjetivoJuridico
        dr(24) = sFundamentoLegal
        dr(25) = SclasificadorCrimen
        dr(26) = SclasificadorActorArmado
        dr(27) = SclasificadorCoberturaDepartamental
        dr(28) = SclasificadorContieneEnfoqueDiferencial
        dr(29) = SclasificadorContieneEnfoqueTerritorial
        dr(30) = sUsuario
        Return dr
    End Function

    Function TConsultarFuentes(tipo As String, clasificador As String, tipoDocumentoFuente As String, esEntidad As Boolean) As DataTable

        Dim conexiontmp As Object
        Dim rs1 As Object
        Dim sql As String
        Dim iReg As Integer
        Dim Sdato As String
        Dim Stipo As String
        Dim Stitulo As String
        Dim Saccesible As String
        Dim Scontenido As String
        Dim Scontenido2 As String
        Dim stituloAnexo As String

        Dim conexiontmp2 As Object
        Dim rs2 As Object



        iReg = 0
        Dim dt As DataTable = New DataTable()


        Dim SkeyVictima As Integer
        Dim SkeyCompareciente As Integer
        Dim SkeyLugar As Integer
        Dim SkeyHechos As Integer
        Dim Sidfuente As String
        Dim SkeyEfectos As Integer

        SkeyVictima = 0
        SkeyCompareciente = 0
        SkeyLugar = 0
        SkeyEfectos = 0
        SkeyHechos = 0
        Sidfuente = ""


        Dim delimiter As Char = " "c
        Dim substrings() As String = TxtBuscar.Text.Split(delimiter)
        Dim palabras As Integer
        Dim iu As Integer
        palabras = substrings.Length

        Dim numbersToRemove As String() = {"del", "la", "las", "el", "los", "este", "ese", "a", "de", "desde", "hace", "sobre", "un", "unos", "una", "unas", "lo", "."}

        For Each number As String In numbersToRemove
            substrings = substrings.Where(Function(val) val <> number).ToArray()
        Next

        palabras = substrings.Length
        iu = 0
        For iu = 0 To palabras - 1
            Dim substring As Object
            substring = substrings(iu)
            If substring.ToLower.Contains("ctima") = True Then
                SkeyVictima = 1
            End If
            If substring.ToLower.Contains("compareciente") Or substring.ToLower.Contains("sometido") Or substring.ToLower.Contains("indiciad") Or substring.ToLower.Contains("imputado") Or substring.ToLower.Contains("responsa") Then
                SkeyCompareciente = 1
            End If
            If substring.ToLower.Contains("lugar") = True Or substring.ToLower.Contains("munici") = True Or substring.ToLower.Contains("departam") = True Then
                SkeyLugar = 1
            End If
            If substring.ToLower.Contains("hechos") = True Or substring.ToLower.Contains("relato") = True Then
                SkeyHechos = 1
            End If
            If substring.ToLower.Contains("efectos") = True Or substring.ToLower.Contains("daño") = True Then
                SkeyEfectos = 1
            End If

        Next

        ' Define the columns of the table.
        dt.Columns.Add(New DataColumn("SeleccionarID", GetType(String)))
        dt.Columns.Add(New DataColumn("Reg", GetType(String)))
        dt.Columns.Add(New DataColumn("Idfuente", GetType(String)))
        dt.Columns.Add(New DataColumn("Detalle", GetType(String)))



        'construyendo filtro
        Dim sql3 As String
        Dim sqlEstruturado As String
        Dim sqlNoEstruturado As String
        sql3 = ""
        sqlEstruturado = ""
        sqlNoEstruturado = ""
        Dim clasificadores As String
        clasificadores = ""


        If Trim(TxtBuscar.Text) <> "" Then
            sql3 = sql3 & " and ((( "
            iu = 0
            For iu = 0 To palabras - 1
                Dim substring As Object
                substring = substrings(iu)
                sql3 = sql3 & " fne.descripcion_fuente Like '%" & substring & "%' "
                If iu <= palabras - 2 Then
                    sql3 = sql3 & " or "
                End If
            Next
            sql3 = sql3 & " ) "
        End If


        If Trim(TxtBuscar.Text) <> "" Then
            sql3 = sql3 & " or  ( "
            iu = 0
            For iu = 0 To palabras - 1
                Dim substring As Object
                substring = substrings(iu)
                sql3 = sql3 & " entidad_nombre Like '%" & substring & "%' "
                If iu <= palabras - 2 Then
                    sql3 = sql3 & " or "
                End If
            Next
            sql3 = sql3 & " ) "
        End If

        If Trim(TxtBuscar.Text) <> "" Then
            sql3 = sql3 & " or  ( "
            iu = 0
            For iu = 0 To palabras - 1
                Dim substring As Object
                substring = substrings(iu)
                sql3 = sql3 & " nombre_fuente Like '%" & substring & "%' "
                If iu <= palabras - 2 Then
                    sql3 = sql3 & " or "
                End If
            Next
            sql3 = sql3 & " ) "
        End If



        If Trim(TxtBuscar.Text) <> "" Then
            sql3 = sql3 & " or  ( "
            iu = 0
            For iu = 0 To palabras - 1
                Dim substring As Object
                substring = substrings(iu)
                sql3 = sql3 & " descripcion_fuente_d Like '%" & substring & "%' "
                If iu <= palabras - 2 Then
                    sql3 = sql3 & " or "
                End If
            Next
            sql3 = sql3 & " ) "
        End If


        If Trim(TxtBuscar.Text) <> "" Then
            sqlEstruturado = sqlEstruturado & " or  ( "
            iu = 0
            For iu = 0 To palabras - 1
                Dim substring As Object
                substring = substrings(iu)
                sqlEstruturado = sqlEstruturado & " fe.id_fuente_estructurada Like '%" & substring & "%' "
                If iu <= palabras - 2 Then
                    sqlEstruturado = sqlEstruturado & " or "
                End If
            Next
            sqlEstruturado = sqlEstruturado & " ) "
        End If


        If Trim(TxtBuscar.Text) <> "" Then
            sqlNoEstruturado = sqlNoEstruturado & " or  ( "
            iu = 0
            For iu = 0 To palabras - 1
                Dim substring As Object
                substring = substrings(iu)
                sqlNoEstruturado = sqlNoEstruturado & " fe.id_fuente_noestructurada Like '%" & substring & "%' "
                If iu <= palabras - 2 Then
                    sqlNoEstruturado = sqlNoEstruturado & " or "
                End If
            Next
            sqlNoEstruturado = sqlNoEstruturado & " ) "
        End If


        If SkeyVictima = 1 Then
            'tiene_responsables, tiene_victimas, tiene_lugares, tiene_hechos
            sql3 = sql3 & " or tiene_victimas=1 "
        End If

        If SkeyCompareciente = 1 Then
            'tiene_responsables, tiene_victimas, tiene_lugares, tiene_hechos
            sql3 = sql3 & " or tiene_responsables=1 "
        End If

        ''***************************************************************************************************************
        'If Trim(TxtBuscar.Text) <> "" Then
        '    sql3 = sql3 & " ) "
        'End If
        ''***************************************************************************************************************

        Dim sid_fuente As String
        sid_fuente = ""

        If Trim(TxtBuscar.Text) <> "" Then
            clasificadores = " ( "
            iu = 0
            For iu = 0 To palabras - 1
                Dim substring As Object
                substring = substrings(iu)
                clasificadores = clasificadores & " vc.descripcion Like '%" & substring & "%' "
                If iu <= palabras - 2 Then
                    clasificadores = clasificadores & " or "
                End If
            Next
            clasificadores = clasificadores & " ) "



            For iu = 0 To palabras - 1
                Dim substring As Object
                substring = substrings(iu)

                If substring.ToString.Contains(".") Then
                    If iu = 0 Then
                        sid_fuente = "'" & substring & "'"

                    Else
                        sid_fuente = sid_fuente & ",'" & substring & "'"
                    End If
                End If
            Next

        End If



        'buscando id de fuentes que tambien cumplen criterios de lugares y hechos (se busca por varibles del diccionario de datos)
        Dim sql5 As String
        sql5 = ""

        If SkeyLugar = 1 Then
            'tiene_responsables, tiene_victimas, tiene_lugares, tiene_hechos
            sql5 = "select distinct(id_fuente) from acampos_relevante_diccionario acrd , mCampos_relevantes mcr, mdiccionario_datos mdd, afuentes_diccionario afd "
            sql5 = sql5 & "where acrd.id_campo=mcr.id_campo and acrd.id_diccionario=mdd.id_diccionario and afd.nombre_variable=mdd.nombre_variable and ( pregunta_investigacion='donde' "

        End If

        Dim fuentesCumplen As String
        fuentesCumplen = ""
        If SkeyHechos = 1 Then
            If sql5 = "" Then
                sql5 = "select distinct(id_fuente) from acampos_relevante_diccionario acrd , mCampos_relevantes mcr, mdiccionario_datos mdd, afuentes_diccionario afd "
                sql5 = sql5 & " where acrd.id_campo=mcr.id_campo and acrd.id_diccionario=mdd.id_diccionario and afd.nombre_variable=mdd.nombre_variable and ( pregunta_investigacion='porque' "
            Else
                sql5 = sql5 & " or pregunta_investigacion='porque' "
            End If

        End If
        If SkeyEfectos = 1 Then
            If sql5 = "" Then
                sql5 = "select distinct(id_fuente) from acampos_relevante_diccionario acrd , mCampos_relevantes mcr, mdiccionario_datos mdd, afuentes_diccionario afd "
                sql5 = sql5 & " where acrd.id_campo=mcr.id_campo and acrd.id_diccionario=mdd.id_diccionario and afd.nombre_variable=mdd.nombre_variable and ( pregunta_investigacion='que efectos' "
            Else
                sql5 = sql5 & " or pregunta_investigacion='que efectos' "
            End If
        End If

        conexiontmp = Server.CreateObject("ADODB.Connection")
        conexiontmp.Open(Application("CadVJep"))
        conexiontmp2 = Server.CreateObject("ADODB.Connection")
        conexiontmp2.Open(Application("CadVJep"))


        If SkeyHechos = 1 Or SkeyLugar = 1 Or SkeyEfectos = 1 Then
            sql5 = sql5 & " ) "
            rs1 = conexiontmp.Execute(sql5)

            '---se consultan por pregunta de investigacion Campos relevantes
            Do While Not rs1.EOF()
                Dim yy As Integer
                yy = 0
                If Not rs1.fields("id_fuente").value() Is DBNull.Value Then
                    If yy = 0 Then
                        fuentesCumplen = "'" & rs1.fields("id_fuente").value() & "'"
                    Else
                        fuentesCumplen = fuentesCumplen & ",'" & rs1.fields("id_fuente").value() & "'"
                    End If
                End If
                rs1.movenext()
            Loop
            rs1.close()

        End If
        Dim Sfuentes_s As String
        Sfuentes_s = ""

        '----buscar si en los clasificadores
        Dim y As Integer

        If clasificadores <> "" Or clasificador <> "" Then

            If esEntidad Then
                'BUSQUEDA POR ENTIDAD Y TIPO DE DOCUMENTO
                sql5 = "SELECT mfe.id_fuente_estructurada as id_fuente_s"
                sql5 = sql5 & " From mfuentes f, mfuentes_estructuradas mfe"
                sql5 = sql5 & " where f.Id_fuente = mfe.id_fuente and "
                sql5 = sql5 & " f.Id_entidad in (" & clasificador & ")"
                sql5 = sql5 & " and f.tipoDocumento in (" & tipoDocumentoFuente & ")"
                sql5 = sql5 & " union "
                sql5 = sql5 & " select mfne.id_fuente_noestructurada as id_fuente_s "
                sql5 = sql5 & " From mfuentes f, mFuentes_no_estructuradas mfne "
                sql5 = sql5 & " where f.Id_fuente = mfne.id_fuente and "
                sql5 = sql5 & " f.Id_entidad in (" & clasificador & ")"
                sql5 = sql5 & " and f.tipoDocumento in (" & tipoDocumentoFuente & ")"
            Else
                'BUSQUEDA POR CLASIFICADOR
                sql5 = "select mfe.id_fuente_estructurada as id_fuente_s "
                sql5 = sql5 & " From aFuentes_clasificadores afc, vClasificadores vc, mfuentes f, mfuentes_estructuradas mfe "
                sql5 = sql5 & " where  mfe.id_fuente_estructurada=afc.Id_fuente_est_nest and vc.id_clasificador=afc.id_clasificador and vc.estado=1 and afc.estado=1  and mfe.id_fuente=f.id_fuente and "
                If clasificador <> "" Then
                    sql5 = sql5 & "  vc.id_clasificador in (" & clasificador & ")"
                    sql5 = sql5 & " and f.tipoDocumento in (" & tipoDocumentoFuente & ")"
                Else
                    sql5 = sql5 & clasificadores
                End If

                sql5 = sql5 & " union "
                sql5 = sql5 & " select mfne.id_fuente_noestructurada "
                sql5 = sql5 & " from aFuentes_clasificadores afc, vClasificadores vc, mfuentes f, mfuentes_no_estructuradas mfne "
                sql5 = sql5 & " where f.id_fuente=mfne.id_fuente and afc.Id_fuente_est_nest=mfne.id_fuente_noestructurada  and vc.id_clasificador=afc.id_clasificador and vc.estado=1 and afc.estado=1  and mfne.id_fuente=f.id_fuente and "

                If clasificador <> "" Then
                    sql5 = sql5 & "  vc.id_clasificador in (" & clasificador & ")"
                    sql5 = sql5 & " and f.tipoDocumento in (" & tipoDocumentoFuente & ")"
                Else
                    sql5 = sql5 & clasificadores
                End If
            End If

            rs1 = conexiontmp.Execute(sql5)

            y = 0

            Do While Not rs1.EOF()
                If Not rs1.fields("id_fuente_s").value() Is DBNull.Value Then
                    If y = 0 Then
                        Sfuentes_s = "'" & rs1.fields("id_fuente_s").value() & "'"
                    Else
                        Sfuentes_s = Sfuentes_s & ",'" & rs1.fields("id_fuente_s").value() & "'"
                    End If
                    y = y + 1
                End If
                rs1.movenext()
            Loop
            rs1.close()
        End If


        '-----

        sql = " SELECT id_fuente_estructurada as id_fuentes, nombre_fuente, nombre_subfuente as nombre_subfuente_tipo_f, formato, numero_de_variables as numero_variables, cantidad_registros as cantidad_registros_autor, descripcion_fuente_d, fecha_recepcion, fecha_corte, fecha_carga, esta_revisada, porcentaje_aplicabilidad, ec.entidad_nombre as entidad_nombre, fne.descripcion_fuente as descripcion_fuente, vc.nombre_custodio as responsable_custodio, accesible_para, es_estructurada,  fa.descripcion as forma_acceso "
        sql = sql & " , fs.descripcion as forma_soporte, ti.Descripcion as tipo_informacion,'' as ubicacion,temporalidad_ano_inicia, temporalidad_ano_termina "
        sql = sql & "  From mFuentes fne left join mentidades_convenios ec on ec.id_entidad = fne.id_entidad "
        sql = sql & "  left join vFormas_acceso fa on fa.id_forma_acceso=fne.id_forma_acceso "
        sql = sql & "  left join vTipos_informacion ti on ti.Id_tipo_informacion=fne.id_tipo_informacion "
        sql = sql & "  left join mFuentes_estructuradas fe on fe.id_fuente=fne.Id_fuente "
        sql = sql & "  left join vformas_soporte fs on fs.id_tipo_soporte=fe.id_tipo_soporte "
        sql = sql & " Left Join vcustodios vc on vc.id_custodio=fe.id_custodio "
        sql = sql & "  where  fe.esta_activa=1 "
        If sql3 <> "" Then
            sql = sql & sql3
            sql = sql & sqlEstruturado
            sql = sql & " ) "

            If sid_fuente <> "" Or Sfuentes_s <> "" Then
                sql = sql & " or "
            End If
        Else
            If sid_fuente <> "" Or Sfuentes_s <> "" Then
                sql = sql & " and "
            End If

        End If

        If sid_fuente <> "" Or Sfuentes_s <> "" Then
            sql = sql & "  (id_fuente_estructurada in (" & sid_fuente & Sfuentes_s & ") "
        End If


        'If fuentesCumplen <> "" Then
        '    sql = sql & "   or fne.id_fuente in (" & fuentesCumplen & ") "

        'End If

        If sid_fuente <> "" Or Sfuentes_s <> "" Or fuentesCumplen <> "" Then
            sql = sql & "  )"
        End If

        If sql3 <> "" Then
            sql = sql & " ) "
        End If

        '---fuentes no estructuradas
        sql = sql & " union "
        sql = sql & " SELECT id_fuente_noestructurada, nombre_fuente, vft.descripcion as tipo_fuente, formato, '',  autor_fuente,descripcion_fuente_d , fecha_recepcion, '', fecha_carga, esta_revisada, porcentaje_aplicabilidad,  ec.entidad_nombre as entidad_nombre, fne.descripcion_fuente as descripcion_fuente, vc.nombre_custodio as responsable_custodio, accesible_para, es_estructurada,fa.descripcion as forma_acceso, "
        sql = sql & " fs.descripcion as forma_soporte, ti.Descripcion as tipo_informacion, ubicacion, temporalidad_ano_inicia, temporalidad_ano_termina "
        sql = sql & " FROM mFuentes fne left join mentidades_convenios ec on ec.id_entidad = fne.id_entidad "
        sql = sql & "     left join vFormas_acceso fa on fa.id_forma_acceso=fne.id_forma_acceso  "
        sql = sql & " Left join vTipos_informacion ti on ti.Id_tipo_informacion=fne.id_tipo_informacion "
        sql = sql & " left join mFuentes_no_estructuradas fe on fe.id_fuente=fne.Id_fuente "
        sql = sql & " left join vfuentes_tipo vft on vft.id_tipo_fuente =fe.id_tipo_fuente"
        sql = sql & " left join vformas_soporte fs on fs.id_tipo_soporte=fe.id_tipo_soporte "
        sql = sql & " left join vubicaciones vu on vu.id_ubicacion =fe.id_ubicacion"
        sql = sql & " Left Join vcustodios vc on vc.id_custodio=fe.id_custodio "
        sql = sql & " where  fe.esta_activa=1 "
        If sql3 <> "" Then
            sql = sql & sql3
            sql = sql & sqlNoEstruturado
            sql = sql & " ) "

            If sid_fuente <> "" Or Sfuentes_s <> "" Then
                sql = sql & " Or "
            End If
        Else
            If sid_fuente <> "" Or Sfuentes_s <> "" Then
                sql = sql & " And "
            End If

        End If

        If sid_fuente <> "" Or Sfuentes_s <> "" Then
            sql = sql & " (id_fuente_noestructurada in (" & sid_fuente & Sfuentes_s & ") "
        End If


        'If fuentesCumplen <> "" Then
        '    sql = sql & "   or fne.id_fuente in (" & fuentesCumplen & ") "
        'End If

        If sid_fuente <> "" Or Sfuentes_s <> "" Or fuentesCumplen <> "" Then
            sql = sql & "  )"
        End If


        If sql3 <> "" Then
            sql = sql & " ) "
        End If

        sql = sql & " order by fecha_recepcion desc,entidad_nombre,es_estructurada "
        'MsgBox(sql)
        iReg = 0
        If clasificador <> "" And y = 0 Then
            iReg = 0
        Else
            rs1 = conexiontmp.Execute(sql)
            iReg = 0
            Do While Not rs1.EOF()
                iReg = iReg + 1
                Sdato = ""
                Stipo = ""
                Saccesible = ""
                Stitulo = ""
                Scontenido = ""
                Scontenido2 = ""
                Sidfuente = ""
                stituloAnexo = ""

                If Not rs1.fields("id_fuentes").value() Is DBNull.Value Then
                    Sidfuente = rs1.fields("id_fuentes").value()
                End If
                If Not rs1.fields("entidad_nombre").value() Is DBNull.Value Then
                    Stitulo = rs1.fields("entidad_nombre").value()
                End If

                If Not rs1.fields("nombre_fuente").value() Is DBNull.Value Then
                    Stitulo = Stitulo & " - Fuente:" & rs1.fields("nombre_fuente").value()
                    'stituloAnexo = rs1.fields("nombre_fuente").value()
                End If


                If Not rs1.fields("es_estructurada").value() Is DBNull.Value Then
                    If rs1.fields("es_estructurada").value() = 1 Then
                        If Not rs1.fields("nombre_subfuente_tipo_f").value() Is DBNull.Value Then
                            Stitulo = Stitulo & " - Subfuente:" & rs1.fields("nombre_subfuente_tipo_f").value()
                        End If
                    Else
                        'If Not rs1.fields("nombre_subfuente_tipo_f").value() Is DBNull.Value Then
                        '    Stitulo = Stitulo & " - formato:" & rs1.fields("nombre_subfuente_tipo_f").value()
                        'End If
                    End If
                End If

                If Not rs1.fields("fecha_recepcion").value() Is DBNull.Value Then
                    Stitulo = Stitulo & " - recibida (" & rs1.fields("fecha_recepcion").value() & ")"
                    'stituloAnexo = rs1.fields("nombre_fuente").value()
                End If


                If Not rs1.fields("accesible_para").value() Is DBNull.Value Then
                    Saccesible = " accesible para:" & rs1.fields("accesible_para").value()
                End If
                If Not rs1.fields("forma_acceso").value() Is DBNull.Value Then
                    Saccesible = Saccesible & ". acceso por:" & rs1.fields("forma_acceso").value()
                End If

                If Not rs1.fields("responsable_custodio").value() Is DBNull.Value Then
                    Saccesible = Saccesible & ", custodio:" & rs1.fields("responsable_custodio").value()
                End If

                If Not rs1.fields("tipo_informacion").value() Is DBNull.Value Then
                    Saccesible = Saccesible & ", tipo información:" & rs1.fields("tipo_informacion").value()
                End If

                If Not rs1.fields("es_estructurada").value() Is DBNull.Value Then
                    If rs1.fields("es_estructurada").value() = 1 Then
                        If Not rs1.fields("cantidad_registros_autor").value() Is DBNull.Value Then
                            Saccesible = Saccesible & " registros:" & rs1.fields("cantidad_registros_autor").value()
                        End If
                        If Not rs1.fields("numero_variables").value() Is DBNull.Value Then
                            Saccesible = Saccesible & "  Número de variables:" & rs1.fields("numero_variables").value()
                        End If
                    Else
                        If Not rs1.fields("formato").value() Is DBNull.Value Then
                            Scontenido = rs1.fields("formato").value()
                        End If

                        If Not rs1.fields("forma_soporte").value() Is DBNull.Value Then
                            Scontenido = Scontenido & " soporte en:" & rs1.fields("forma_soporte").value()
                        End If

                        If Not rs1.fields("ubicacion").value() Is DBNull.Value Then
                            If rs1.fields("ubicacion").value() <> "" Then
                                Scontenido = Scontenido & " ubicación:" & rs1.fields("ubicacion").value()
                            End If
                        End If

                        If Not rs1.fields("cantidad_registros_autor").value() Is DBNull.Value Then
                            If rs1.fields("cantidad_registros_autor").value() <> "" Then
                                Scontenido = Scontenido & " autor:" & rs1.fields("cantidad_registros_autor").value()
                            End If
                        End If

                    End If
                End If



                If Not rs1.fields("temporalidad_ano_inicia").value() Is DBNull.Value Then
                    Scontenido = Scontenido & "-" & rs1.fields("temporalidad_ano_inicia").value()
                End If
                If Not rs1.fields("temporalidad_ano_termina").value() Is DBNull.Value Then
                    Scontenido = Scontenido & "-" & rs1.fields("temporalidad_ano_termina").value()

                End If

                '<asp:LinkButton id="LinkButton2" CommandName="Show" text='<%# Eval("idFuente")%>'  runat="server"/>

                If Not rs1.fields("descripcion_fuente").value() Is DBNull.Value Then
                    Scontenido2 = Mid(rs1.fields("descripcion_fuente").value(), 1, 200)
                End If

                'If Not rs1.fields("descripcion_fuente_d").value() Is DBNull.Value Then
                '    Scontenido2 = Scontenido2 & rs1.fields("descripcion_fuente_d").value()
                'End If

                'consulta de las fuentes relacionadas:
                'sql6 = "Select nombre_fuente,detalle_asociacion From aFuentes_relacionadas fr , mFuentes_estructuradas fe "
                'sql6 = sql6 & " Where fr.estado = 1 And (fe.id_fuente_estructurada = fr.id_fuente_est_nest1 Or fe.id_fuente_estructurada = id_fuente_est_nest2) "
                'sql6 = sql6 & " And ( id_fuente_est_nest1='" & Sidfuente & "' or id_fuente_est_nest2='" & Sidfuente & "') "
                'sql6 = sql6 & " union "
                'sql6 = sql6 & " Select nombre_fuente, detalle_asociacion "
                'sql6 = sql6 & " From aFuentes_relacionadas fr, mFuentes_no_estructuradas fne "
                'sql6 = sql6 & " Where fr.estado = 1 And (fne.id_fuente_noestructurada = fr.id_fuente_est_nest1 Or fne.id_fuente_noestructurada = id_fuente_est_nest2) "
                'sql6 = sql6 & " And ( id_fuente_est_nest1='" & Sidfuente & "' or id_fuente_est_nest2='" & Sidfuente & "') "
                'rs2 = conexiontmp.Execute(sql6)
                'Do While Not rs2.EOF()
                '    If Not rs2.fields("nombre_fuente").value() Is DBNull.Value Then
                '        If rs1.fields("nombre_fuente").value() <> rs2.fields("nombre_fuente").value() Then
                '            Scontenido2 = Scontenido2 & " fuentes relacionadas:" & rs2.fields("nombre_fuente").value()
                '            If Not rs2.fields("detalle_asociacion").value() Is DBNull.Value Then
                '                Scontenido2 = Scontenido2 & " relación: " & rs2.fields("detalle_asociacion").value()
                '            End If
                '        End If
                '    End If
                '    rs2.movenext()
                'Loop
                'rs2.close()
                'rs2 = Nothing
                'dt.Rows.Add(CreateRowFuentes(Str(iReg), Sidfuente, Stitulo, "Titulo", dt))
                'dt.Rows.Add(CreateRowFuentes("", "", Saccesible, "Accesible", dt))
                'dt.Rows.Add(CreateRowFuentes("", "", Scontenido2, "Contenido", dt))

                dt.Rows.Add(CreateRowFuentes(Str(iReg), Sidfuente, Stitulo, "Titulo", dt))
                dt.Rows.Add(CreateRowFuentes("", "", Saccesible, "Accesible", dt))
                dt.Rows.Add(CreateRowFuentes("", "", Scontenido2, "Contenido", dt))
                dt.Rows.Add(CreateRowFuentes("", "", "", "", dt))

                ' dt.Rows.Add(CreateRowFuentes("", "", Scontenido, "Contenido", dt))
                'azul
                rs1.movenext()
            Loop
            Lblresultados.Text = "Resultados encontrados: " & Str(iReg)

            rs1.close()
        End If
        conexiontmp.close()
        conexiontmp = Nothing
        conexiontmp2.close()
        conexiontmp2 = Nothing
        rs1 = Nothing
        rs2 = Nothing
        If iReg = 0 Then
            Lblresultados.Text = "No hay resultados para esta busqueda"
        End If
        Return dt

    End Function

    Function EjecutarCadena(ByVal sql As String) As InidiceBusqueda
        'verificq si un documento ya ha sido asociado a un proceso
        Dim sw As String
        Dim sql2 As String
        Dim conexion As Object
        Dim rs2 As Object
        Dim _InidiceBusqueda = New InidiceBusqueda()
        sw = "1"
        sql2 = sql
        conexion = Server.CreateObject("ADODB.Connection")
        conexion.Open(Application("CadVJep"))
        rs2 = conexion.Execute(sql2)
        Do Until rs2.eof
            If Not rs2(0).value Is DBNull.Value Then
                _InidiceBusqueda.Homologo_ID = rs2(0).value
            End If
            If Not rs2(1).value Is DBNull.Value Then
                _InidiceBusqueda.TipoDocumento_ID = rs2(1).value
            End If
            If Not rs2(2).value Is DBNull.Value Then
                _InidiceBusqueda.EsEntidad = rs2(2).value
            End If
            rs2.movenext()
        Loop
        rs2.close()
        conexion.CLOSE()
        conexion = Nothing
        rs2 = Nothing
        Return _InidiceBusqueda
    End Function

    Function CreateRowFuentes(reg As String, fuente As String, dato As String, tipo As String, dt As DataTable) As DataRow
        Dim dr As DataRow = dt.NewRow()
        dr(1) = reg
        dr(2) = fuente
        dr(3) = dato
        Return dr
    End Function

    Function CreateRowDetalleFuente(dato As String, dt As DataTable) As DataRow
        Dim dr As DataRow = dt.NewRow()
        dr(0) = dato
        Return dr
    End Function


    Function TConsultarNovedades() As DataTable
        Dim sql As String
        Dim conexiontmp As Object
        Dim rs1 As Object
        Dim iReg As Integer

        Dim Snovedad As String
        Dim Sentidad As String
        Dim Sdetalle_fuente As String
        Dim Sfecha_carga As String
        Dim Snombre_fuente As String

        iReg = 0
        Dim dt As DataTable = New DataTable()
        dt.Columns.Add(New DataColumn("Detalle", GetType(String)))

        sql = " select n.descripcion as novedad,  ec.entidad_nombre as entidad,f.descripcion_fuente as descripcion_fuente, fe.nombre_fuente as nombre_fuente, fe.fecha_carga as fecha_carga "
        sql = sql & " from mnovedades n left join mfuentes_estructuradas fe on fe.id_fuente_estructurada=n.id_fuente_est_nest "
        sql = sql & " left join  mfuentes f on f.id_fuente=fe.id_fuente "
        sql = sql & " left join  mentidades_convenios ec on  ec.id_entidad=f.id_entidad "
        sql = sql & " where n.estado=1 and n.fecha_inicio<=getdate() and n.fecha_fin>=getdate() "
        sql = sql & " union "
        sql = sql & " select n.descripcion, ec.entidad_nombre,f.descripcion_fuente, fen.nombre_fuente, fen.fecha_carga "
        sql = sql & " from mnovedades n , mfuentes_no_estructuradas fen, mfuentes f, mentidades_convenios ec "
        sql = sql & " where  ec.id_entidad=f.id_entidad and f.id_fuente=fen.id_fuente and fen.id_fuente_noestructurada=n.id_fuente_est_nest and n.estado=1 and n.fecha_inicio<=getdate() and n.fecha_fin>=getdate()  "


        conexiontmp = Server.CreateObject("ADODB.Connection")
        conexiontmp.Open(Application("CadVJep"))
        rs1 = conexiontmp.Execute(sql)

        Do While Not rs1.EOF()
            iReg = iReg + 1
            Snovedad = ""
            Sentidad = ""
            Sdetalle_fuente = ""
            Sfecha_carga = ""
            Snombre_fuente = ""

            If Not rs1.fields("novedad").value() Is DBNull.Value Then
                Snovedad = rs1.fields("novedad").value()
            End If
            If Not rs1.fields("entidad").value() Is DBNull.Value Then
                Snovedad = Snovedad & " de " & rs1.fields("entidad").value()
            End If

            If Not rs1.fields("descripcion_fuente").value() Is DBNull.Value Then
                Sdetalle_fuente = rs1.fields("descripcion_fuente").value()
            End If

            If Not rs1.fields("nombre_fuente").value() Is DBNull.Value Then
                Snombre_fuente = rs1.fields("nombre_fuente").value()
            End If

            If Not rs1.fields("fecha_carga").value() Is DBNull.Value Then
                Sfecha_carga = Mid(rs1.fields("fecha_carga").value(), 1, 11)
            End If

            dt.Rows.Add(CreateRowDetalleFuente(Snovedad.ToUpperInvariant, dt))

            If Sfecha_carga <> "" Then
                dt.Rows.Add(CreateRowDetalleFuente("Fecha de publicación para consulta:" & Sfecha_carga, dt))
            Else
                dt.Rows.Add(CreateRowDetalleFuente("", dt))
            End If
            If Snombre_fuente <> "" Then
                dt.Rows.Add(CreateRowDetalleFuente("Nombre de la fuente:" & Snombre_fuente, dt))
            Else
                dt.Rows.Add(CreateRowDetalleFuente("", dt))
            End If
            If Sdetalle_fuente <> "" Then
                dt.Rows.Add(CreateRowDetalleFuente(Sdetalle_fuente, dt))
            Else
                dt.Rows.Add(CreateRowDetalleFuente("", dt))
            End If
            rs1.movenext()
        Loop

        rs1.close()
        conexiontmp.close()
        conexiontmp = Nothing
        rs1 = Nothing
        If iReg = 0 Then
            dt.Rows.Add(CreateRowDetalleFuente("No hay novedades", dt))
        End If

        Return dt

    End Function


    Public Function ConsultarNovedades()

        'LinkButton_Click

        If GVDatos2.Rows.Count > 0 Then
            Dim y As Integer
            Dim kb As Integer
            Dim kg As Integer

            y = 1
            For i As Integer = 0 To GVDatos2.Rows.Count - 1

                If y = 1 Then
                    GVDatos2.Rows(i).ForeColor = Color.FromName("Red")
                    GVDatos2.Rows(i).Font.Size = 11
                    GVDatos2.Rows(i).Font.Bold = True
                    kg = y + 4
                End If
                If y = 2 Then
                    GVDatos2.Rows(i).ForeColor = Color.FromName("Green")
                    GVDatos2.Rows(i).Font.Size = 10
                    kb = y + 4
                End If
                If y = kb Then
                    GVDatos2.Rows(i).ForeColor = Color.FromName("Green")
                    GVDatos2.Rows(i).Font.Size = 11
                    kb = kb + 4
                End If
                If y = kg Then
                    GVDatos2.Rows(i).ForeColor = Color.FromName("Red")
                    GVDatos2.Rows(i).Font.Bold = True
                    kg = kg + 4
                End If
                y = y + 1
            Next
        End If
        Return 0

    End Function




    Protected Sub LbtExportar_Click(sender As Object, e As EventArgs) Handles LbtExportar.Click

        Dim dtReporte As DataTable = New DataTable()
        dtReporte = TConsultarFuentesReporte()
        ExportarToExcel(dtReporte)
    End Sub

    Private Sub ExportarToExcel(dt As DataTable)

        If dt.Rows.Count > 0 Then
            Dim tw As StringWriter = New StringWriter()
            Dim dgGrid As GridView = New GridView()
            Dim hw As HtmlTextWriter = New HtmlTextWriter(tw)
            dgGrid.DataSource = dt
            dgGrid.DataBind()
            dgGrid.ViewStateMode = UI.ViewStateMode.Disabled
            dgGrid.RenderControl(hw)

            Try
                Response.Clear()
                Response.Buffer = True
                Response.ContentType = “application/vnd.ms-excel”
                Response.AddHeader(“Content-Disposition”, “attachment;filename=IndiceFuentes_" & Now().Year & Now().Month & Now().Day & ".xls”)
                Response.Charset = “UTF-8”
                Response.ContentEncoding = Encoding.Default
                Response.Write(tw.ToString())
                Response.End()

            Catch ex As Exception
                Dim menajeError As String
                menajeError = ex.Message
            End Try

        End If
    End Sub

    Private Sub Exportar(Datagrid As GridView)
        Dim sb As StringBuilder = New StringBuilder()
        Dim sw As StringWriter = New StringWriter(sb)
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        Dim pagina As Page = New Page()
        Dim form = New HtmlForm

        Datagrid.ViewStateMode = UI.ViewStateMode.Disabled
        pagina.EnableEventValidation = False
        pagina.DesignerInitialize()

        pagina.Controls.Add(form)
        Try
            form.Controls.Add(Lblresultados)
        Catch
        End Try

        'Try
        '    form.Controls.Add(lblPalabraClave)
        'Catch
        'End Try

        Try
            form.Controls.Add(TxtBuscar)
        Catch
        End Try
        Try
            form.Controls.Add(Datagrid)
        Catch ex As Exception

        End Try

        Try
            pagina.RenderControl(htw)
        Catch ex As Exception

        End Try

        Try
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = “application/vnd.ms-excel”
            Response.AddHeader(“Content-Disposition”, “attachment;filename=IndiceFuentes" & Now().Year & Now().Month & Now().Day & ".xls”)
            Response.Charset = “UTF-8”
            Response.ContentEncoding = Encoding.Default
            Response.Write(sb.ToString())
            'Response.Flush()
            'Response.Close()
            Response.End()

        Catch ex As Exception
            Dim menajeError As String
            menajeError = ex.Message
        End Try




    End Sub

    Protected Sub LbtAvanzadas_Click(sender As Object, e As EventArgs) Handles LbtAvanzadas.Click
        MultiView1.SetActiveView(Vbrowser)


    End Sub

    'Protected Sub LbtBorrar_Click(sender As Object, e As EventArgs) Handles LbtBorrar.Click
    '    MultiView1.SetActiveView(VResultados)
    '    TxtBuscar.Text = ""
    'End Sub

    Private Sub PopulateRootLevel()
        Dim strsql As String
        strsql = "select id_indice AS id,descripcion AS title, codigoHomologo AS codHomologo, 1 as tipoDocId, (select count(*) FROM vIndiceFuentes WHERE id_indice_padre=sc.id_indice) childnodecount FROM vIndiceFuentes sc where id_indice_padre = 0 AND estado = 1"
        Dim dt As DataTable = Me.GetData(strsql)
        PopulateNodes(dt, TvNavegar.Nodes)
    End Sub

    Public Class CategoryTreeNode
        Inherits TreeNode
        Public Homologo_ID As String
        Public TipoDocumento_ID As String
    End Class

    Public Class InidiceBusqueda
        Public Homologo_ID As String
        Public TipoDocumento_ID As String
        Public EsEntidad As Boolean
    End Class

    Public Class InidiceClasificador
        Public Clasificador As String
        Public TipoDocumentoClasificador As String
        Public EsEntidad As String
    End Class

    Private Sub PopulateNodes(ByVal dt As DataTable,
  ByVal nodes As TreeNodeCollection)
        For Each dr As DataRow In dt.Rows
            Dim tn As New CategoryTreeNode()
            tn.Text = dr("title").ToString()
            tn.Value = dr("id").ToString()
            tn.Homologo_ID = dr("codHomologo").ToString()
            tn.TipoDocumento_ID = dr("tipoDocId").ToString()
            nodes.Add(tn)

            'If node has child nodes, then enable on-demand populating
            tn.PopulateOnDemand = (CInt(dr("childnodecount")) > 0)
        Next
    End Sub

    Private Sub PopulateSubLevel(ByVal parentid As Integer,
      ByVal parentNode As TreeNode)
        Dim strsql As String
        'strsql = "select id_clasificador AS id, descripcion AS title,(select count(*) FROM vclasificadores WHERE id_clasificador_padre=sc.id_clasificador) childnodecount FROM vclasificadores sc where id_clasificador_padre=" & parentid & " order by descripcion "
        strsql = "select id_indice AS id, descripcion AS title, codigoHomologo AS codHomologo, 1 as tipoDocId, (select count(*) FROM vIndiceFuentes WHERE id_indice_padre=sc.id_indice) childnodecount FROM vIndiceFuentes sc where id_indice_padre=" & parentid & "  AND estado = 1 order by descripcion "
        Dim dt As DataTable = Me.GetData(strsql)
        PopulateNodes(dt, parentNode.ChildNodes)
    End Sub

    Private Function GetData(query As String) As DataTable
        Dim dt As New DataTable()
        Dim constr As String = ConfigurationManager.ConnectionStrings("PyachayConnectionString").ConnectionString
        Using con As New SqlConnection(constr)
            Using cmd As New SqlCommand(query)
                Using sda As New SqlDataAdapter()
                    cmd.CommandType = CommandType.Text
                    cmd.Connection = con
                    sda.SelectCommand = cmd
                    sda.Fill(dt)
                End Using
            End Using
            Return dt
        End Using
    End Function



    Protected Sub TvNavegar_SelectedNodeChanged(sender As Object, e As EventArgs) Handles TvNavegar.SelectedNodeChanged
        'Dim prueba As String
        'prueba = ""
        '
    End Sub

    Private Sub TvNavegar_TreeNodePopulate(sender As Object, e As TreeNodeEventArgs) Handles TvNavegar.TreeNodePopulate
        PopulateSubLevel(CInt(e.Node.Value), e.Node)
    End Sub

    Protected Sub LbtAgregar_Click(sender As Object, e As EventArgs) Handles LbtAgregar.Click

    End Sub

    Protected Sub cmdBuscarClasificador_Click(sender As Object, e As EventArgs) Handles cmdBuscarClasificador.Click
        Dim _InidiceClasificador = New InidiceClasificador()
        Dim clasificador As String
        Dim esEntidad As Boolean = False

        _InidiceClasificador = Clasificadores()

        If _InidiceClasificador.EsEntidad = "True" Then
            esEntidad = True
        End If
        BuscarFuentes("C", _InidiceClasificador.Clasificador, _InidiceClasificador.TipoDocumentoClasificador, esEntidad)
    End Sub

    Private Function Clasificadores() As InidiceClasificador
        TxtBuscar.Text = ""

        Dim Clasificador As String
        Dim TipoDocumentoClasificador As String
        Dim EsEntidad As String
        Dim sqlObtenerHomologo As String
        Dim _InidiceBusqueda = New InidiceBusqueda()
        Dim _InidiceClasificador = New InidiceClasificador()
        Clasificador = ""
        For Each node As TreeNode In TvNavegar.CheckedNodes
            If node.Checked = True Then
                sqlObtenerHomologo = "SELECT codigoHomologo, tipoDocumentoFuente, esEntidad FROM vIndiceFuentes WHERE id_indice = " & node.Value & " "
                _InidiceBusqueda = EjecutarCadena(sqlObtenerHomologo)

                If Clasificador = "" Then
                    Clasificador = _InidiceBusqueda.Homologo_ID
                Else
                    Clasificador = Clasificador + "," + _InidiceBusqueda.Homologo_ID
                End If

                If TipoDocumentoClasificador = "" Then
                    TipoDocumentoClasificador = _InidiceBusqueda.TipoDocumento_ID
                Else
                    TipoDocumentoClasificador = TipoDocumentoClasificador + "," + _InidiceBusqueda.TipoDocumento_ID
                End If

                If EsEntidad = "" Then
                    EsEntidad = _InidiceBusqueda.EsEntidad.ToString()
                Else
                    EsEntidad = EsEntidad + "," + _InidiceBusqueda.EsEntidad.ToString()
                End If
            End If
        Next
        _InidiceClasificador.Clasificador = Clasificador
        _InidiceClasificador.TipoDocumentoClasificador = TipoDocumentoClasificador
        _InidiceClasificador.EsEntidad = EsEntidad

        Return _InidiceClasificador

    End Function

    Private Sub TvNavegar_DataBound(sender As Object, e As EventArgs) Handles TvNavegar.DataBound

    End Sub

    'Private Sub TvNavegar_TreeNodeExpanded(sender As Object, e As TreeNodeEventArgs) Handles TvNavegar.TreeNodeExpanded
    '    e.Node.ExpandAll()

    'End Sub

    Private Sub TvNavegar_TreeNodeCollapsed(sender As Object, e As TreeNodeEventArgs) Handles TvNavegar.TreeNodeCollapsed

    End Sub

    Protected Sub GVResultados_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GVResultados.SelectedIndexChanged

    End Sub
End Class