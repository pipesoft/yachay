﻿<%@ Page Title="" aspcompat="true" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Wnuevafuente.aspx.vb" Inherits="SICA.Wnuevafuente" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

      <asp:Panel ID="PMensajes" runat="server" Visible="true" Width="1201px">
           <asp:HyperLink ID="RegisterHyperLink" runat="server" Font-Bold="True" ForeColor="#CC0000" NavigateUrl="~/Register.aspx" ViewStateMode="Disabled" Visible="False">Debe estar registrado para poder acceder. Registrese aqui.</asp:HyperLink>
            <br />
                <asp:HyperLink ID="IngreseHyperLink" runat="server" Font-Bold="True" ForeColor="#CC0000" NavigateUrl="~/login.aspx" ViewStateMode="Disabled" Visible="False">Si ya tiene usuario y esta registrado: Ingrese aqui.</asp:HyperLink>
                <br />
                <asp:Label ID="Lblaviso" runat="server" Text="Los datos aquí suministrados son activos de información con que cuenta la JEP disponible para su uso."></asp:Label>
                <br />
                <asp:Label ID="LblRol" runat="server" ForeColor="#006600" Visible="False"></asp:Label>
                <br />
           <asp:Label ID="LblMensajes" runat="server" BackColor="#FFCCFF" Font-Bold="True" Font-Size="Large" ForeColor="#990000"></asp:Label>
           <br />
                <br />
    </asp:Panel>

    <asp:Panel ID="PInicio" runat="server" Visible="false" Width="1204px">
        <h1><asp:Label ID="Lblresultados" runat="server" Text="Registro de la ficha de la fuente"></asp:Label>
            <asp:Label ID="LblFuente" runat="server"></asp:Label>
            &nbsp;<asp:Label ID="LblFuenteNombre" runat="server"></asp:Label>
            <asp:Label ID="LblFuentePadre" runat="server" Visible="False"></asp:Label>
            <asp:Label ID="LblEntidadPadre" runat="server" Visible="False"></asp:Label>
        </h1>
        <asp:Panel ID="POpciones" runat="server" Visible="true" Width="1199px">
        <table style="width: 1045px; ">
             <tr>
              
                 <td style="width: 296px" >                      
                                        <br />
                                        <br />
                         <br />
                     </td>
                     <td style="width: 296px"  >                      
                                        <asp:LinkButton ID="LbtBasicos" runat="server">Datos básicos</asp:LinkButton>
                         
                     </td>
                        <td style="width: 296px" >                      
                                        <asp:LinkButton ID="LbtAnexos" runat="server">Documentos anexos</asp:LinkButton>                                        
                     </td>
                  <td style="width: 296px" >                      
                                        <asp:LinkButton ID="LbtTiposInformacion" runat="server">Nivel de acceso – Ley 1712 de 2014</asp:LinkButton>                                        
                     </td>
                     <td style="width: 296px" >                      
                                        <asp:LinkButton ID="LbtControlAcceso" runat="server">Control de Acceso</asp:LinkButton>
                     </td>

                     <td style="width: 296px" >                      
                                                            
                                        <asp:LinkButton ID="LbtAvanzados" runat="server">Datos avanzados</asp:LinkButton>                   
                     
                     
                  
             
                 </tr>
                  
    </table>
    </asp:Panel>    
        </asp:Panel>

     
       <asp:MultiView ID="MultiView1" runat="server">
       
           <asp:View ID="Vbasicos" runat="server">
               <table style="width: 1192px">
                   <tr>
                       <td colspan="4" style="height: 3px">
                           <h2>  <asp:Label ID="lbltitulo1" runat="server" Text="Datos básicos de la fuente"></asp:Label></h2>
                       </td>
                   </tr>
                   <tr >
                       <td >
                           <h4>
                           <asp:Label ID="lblTipoEntidad" runat="server" Text="Tipo de Entidad que remite la fuente:"></asp:Label>
                            </h4>
                       </td>
                       <td style="width: 206px; height: 39px;">
                           <asp:ListBox ID="LstTipoEntidad" runat="server" AutoPostBack="True" CssClass="form-control" Rows="1" Width="363px"></asp:ListBox>
                       </td>     
                    </tr>
                   <tr >
                       <td >
                           <h4>
                           <asp:Label ID="Label4" runat="server" Text="Entidad que remite la fuente:"></asp:Label>
                            </h4>
                       </td>
                       <td style="width: 206px; height: 39px;">

                           <asp:ListBox ID="LstEntidad" runat="server" AutoPostBack="True" CssClass="form-control" Rows="1" Width="363px"></asp:ListBox>

                       </td>     
                       <td colspan="2">
                           
                           <asp:CheckBox ID="ChkOtraEntidad" runat="server" AutoPostBack="True" Text="Otra Entidad" Visible="False" /> &nbsp &nbsp
                           
                           <asp:TextBox ID="TxtOtraEntidad" runat="server" BackColor="#FFFFCC" Height="30px" Width="281px" Enabled="False" Visible="False"></asp:TextBox>

                       </td>
                       <%--<td colspan="2">

                           <asp:CheckBox ID="ChkNotificar" runat="server" Text="Notificar ingreso de la fuente" />

                       </td>--%>

                   </tr>                                   
                   <tr>
                       <td>
                            <h4>
                            <asp:Label ID="Label47" runat="server" Text="Tipo de documento:"></asp:Label>
                            </h4>
                       </td>
                       <td>
                             <asp:ListBox ID="LstTipoFuente" runat="server" AutoPostBack="True" CssClass="form-control" Rows="1" Width="357px" Height="34px"></asp:ListBox>
                       </td>
                       <td colspan="2">
                           
                           <asp:CheckBox ID="ChkOtroTipo" runat="server" AutoPostBack="True" Text="Otro tipo" Visible="False" />
                           
                           <asp:TextBox ID="TxtOtroTipoFuente" runat="server" BackColor="#FFFFCC" Height="30px" Width="281px" Enabled="False" Visible="False"></asp:TextBox>

                       </td>
                   </tr>

                   <tr>
                       <td>
                            <h4>
                            <asp:Label ID="lblTipoDatoArchivo" runat="server" Text="Tipo de dato:"></asp:Label>
                            </h4>
                       </td>
                       <td>
                             <asp:ListBox ID="LstTipoDatoArchivo" runat="server" AutoPostBack="True" CssClass="form-control" Rows="1" Width="357px" Height="34px"></asp:ListBox>
                       </td>  
                       <td  colspan="1" style="height: 20px" >
                            <h4>
                                <asp:Label ID="LblTipoDocumentoOtro" runat="server" Text="Por favor crear un tipo de Documento,si no existe." Visible="False"></asp:Label>
                            </h4>
                        </td>                       
                   </tr>

                     <tr>
                       <td >
                           <h4>
                           <asp:Label ID="LblNombreArchivo" runat="server" Text="Nombre del Archivo"></asp:Label>
                            </h4>
                       </td>
                       <td colspan="3" align="left">
                           <asp:TextBox ID="TxtNombreArchivo" runat="server" BackColor="#FFFFCC" Height="30px" Width="1000px"></asp:TextBox>
                       </td>    

                   </tr>
                   <tr>
                       <td >
                           <h4>
                           <asp:Label ID="LblNombreEntidadProductura" runat="server" Text="Entidad generadora de la fuente"></asp:Label>
                            </h4>
                       </td>
                       <td colspan="3" align="left">
                           <asp:TextBox ID="TxtNombreEntidadProductura" runat="server" BackColor="#FFFFCC" Height="30px" Width="1000px"></asp:TextBox>
                       </td>    

                   </tr>
                   <tr>
                       <td>
 <h4>
                           <asp:Label ID="Label41" runat="server" Text="Título del documento:"></asp:Label>
                            </h4>
                       </td>
                       <td colspan="3" align="left">

                           <asp:TextBox ID="TxtNombreFuente" runat="server" BackColor="#FFFFCC" Height="30px" Width="728px"></asp:TextBox>

                       </td>
                   </tr>
                         <tr>
                              <td> <h4><asp:Label ID="LblFormaIngreso" runat="server" Text="Medio de ingreso"></asp:Label> </h4>  </td>
                              <td style="height: 21px">
                                    <h4>  <asp:CheckBoxList ID="ChkFormasIngreso" runat="server" Height="22px" Width="309px">
                                        </asp:CheckBoxList>
                                    </h4>
                               </td>
                               <td   style="height: 20px">
                                    <h4>
                                        <asp:Label ID="Label37" runat="server" Text="Número radicado ORFEO:"></asp:Label>
                                    </h4>
                               </td>
                               <td  style="height: 21px">
                               <asp:TextBox ID="TxtMasDatosDeIngreso" runat="server" Height="45px" Width="199px" BackColor="#FFFFCC" MaxLength="18"></asp:TextBox>
                             </td>

                        </tr>

                        <tr>
                            <td>
                                <h4>
                                    <asp:Label ID="LbltipoFormato" runat="server" Text="Tipo de formato:"></asp:Label>
                                </h4>
                            </td>
                            <td>
                                <asp:ListBox ID="LstTipoFormato" runat="server" AutoPostBack="True" CssClass="form-control" Rows="1" Width="357px" Height="34px"></asp:ListBox>
                            </td>                       
                        </tr>

                        <tr>
                            <td>
                                <h4>
                                    <asp:Label ID="LblnivelDesagregacion" runat="server" Text="Nivel de desagregación territorial:"></asp:Label>
                                </h4>
                            </td>
                            <td>
                                <asp:ListBox ID="LstNivelDesagregacionTerritoria" runat="server" AutoPostBack="True" CssClass="form-control" Rows="1" Width="357px" Height="34px"></asp:ListBox>
                            </td>                       
                        </tr>
                        <tr>
                            <td>
                                <h4>
                                    <asp:Label ID="LblOpcionDocumentoSoporte" runat="server" Text="Documentos técnicos de soporte:"></asp:Label>
                                </h4>
                            </td>
                            <td>
                                <asp:ListBox ID="LstOpcionDocumentoSoporte" runat="server" AutoPostBack="True" CssClass="form-control" Rows="1" Width="357px" Height="34px"></asp:ListBox>
                            </td>                       
                        </tr>
                        <tr>
                            <td>
                                <h4>
                                    <asp:Label ID="LblResponsableGestion" runat="server" Text="Ubicación Actual:"></asp:Label>
                                </h4>
                            </td>
                            <td>
                                <asp:ListBox ID="LstResponsableGestion" runat="server" AutoPostBack="True" CssClass="form-control" Rows="1" Width="357px" Height="34px"></asp:ListBox>
                            </td>                       
                        </tr>                      
                       <tr>
                       <td  >
                           <h4>
                           <asp:Label ID="Label6" runat="server" Text="Idioma"></asp:Label>
                               </h4>
                       </td>
                       <td style="width: 206px">
                               <asp:ListBox ID="LstIdioma" runat="server" AutoPostBack="True" CssClass="form-control" Rows="1" Width="200px"></asp:ListBox>
                       </td>
                       <td >
                       <h4>
                           <asp:Label ID="Label38" runat="server" Text="Contiene base de datos"></asp:Label>
                            </h4>    
                       </td>
                             <td>
                             
                           <asp:RadioButton ID="RbtEsBd" runat="server" Text="Si" AutoPostBack="True" />
                           <asp:RadioButton ID="RbtNoBd" runat="server" Text="No" AutoPostBack="True" Checked="True" />
  
                             </td>
                   </tr>
                         <tr>
                       <td   ><h4>   <asp:Label ID="LblFuenteTipo" runat="server" Text="Tipo de fuente"></asp:Label> </h4>
                       </td>
                       <td style="width: 206px">
                            <asp:ListBox ID="LstFuenteTipo" runat="server" AutoPostBack="False" CssClass="form-control" Rows="1" Width="200px"></asp:ListBox>
                       </td>
                       <td > <h4> <asp:Label ID="LblResponsableCustodio" runat="server" Text="Responsable Custodio"></asp:Label></h4>  </td>
                       <td> <asp:ListBox ID="LstCustodios" runat="server" AutoPostBack="False" CssClass="form-control" Rows="1" Width="191px" Height="32px"></asp:ListBox>                        
                        
                   </tr>
                   <tr>                       
                       <td  >
                            <h4><asp:Label ID="Label10" runat="server" Text="Fecha de recepción"></asp:Label></h4>
                       </td>
                       <td style="width: 206px" align="left">
                           <asp:TextBox ID="txtFecPres" runat="server"  BackColor="#FFFFCC" Height="30px" Width="340px" MaxLength="10"></asp:TextBox>
                       </td>
                       <td  >
                        <h4><asp:Label ID="LblUbicacionOriginal" runat="server" Text="Ubicación del original"></asp:Label></h4>
                       </td>
                       <td>

                           <asp:ListBox ID="LstUbicacion" runat="server" AutoPostBack="True" CssClass="form-control" Rows="1" Width="200px"></asp:ListBox>

                       </td>
                   </tr>
                      
                   <tr>
                       <td><h4>
                           <asp:Label ID="LblDescripcion" runat="server" Text="Resumen del Contenido"></asp:Label></h4>
                       </td>
                       <td colspan="3">                            
                           <br />
                            
                           <asp:TextBox ID="TxtDescripcionFuente" cols="40" rows="5" onKeyUp="return limitar(event,this.value,1000)" onKeyDown="return limitar(event,this.value,1000)" runat="server" Height="143px" TextMode="MultiLine" Width="676px" BackColor="#FFFFCC" MaxLength="500"></asp:TextBox>                                                       
                       </td>

                    </tr>
                   <tr>
                       <td colspan="3">                                                       
                            Número máximo de caracteres 1000 : <asp:Label ID="lblContadorCaracteres" runat="server" Text="0"></asp:Label>                                                                                                             
                       </td>                       
                   </tr>
                      <tr>
                                    <td style="height: 39px">
                           <h4>
                           <asp:Label ID="Label39" runat="server" Text="Tiene nombres de responsables"></asp:Label>
                            </h4>
                           </td>
                       <td style="height: 39px">

                           <asp:RadioButton ID="RbtSiResponsables" runat="server" Checked="True" Text="Si" AutoPostBack="True" />
                           <asp:RadioButton ID="RbtNoResponsables" runat="server" Text="No" AutoPostBack="True" />

                       </td>
                                <td style="height: 39px">
                           <h4>
                           <asp:Label ID="Label40" runat="server" Text="Tiene nombres de víctimas"></asp:Label>
                            </h4>
                           </td>
                       <td style="height: 39px">

                           <asp:RadioButton ID="RbtSiVictimas" runat="server" Checked="True" Text="Si" AutoPostBack="True" />
                           <asp:RadioButton ID="RbtNoVictimas" runat="server" Text="No" AutoPostBack="True" />

                       </td>

                         
                                </tr>
                   <tr>
                       <td>
                           <h4>
                           <asp:Label ID="lblAnexosNoDigitales" runat="server" Text="Relación de otros anexos" Visible="False"></asp:Label></h4>
                       </td>
                       <td colspan="3">

                           <asp:TextBox ID="TxtAnexos" runat="server" BackColor="#FFFFCC" Height="66px" Width="677px" Visible="False" MaxLength="50" TextMode="MultiLine"></asp:TextBox>

                       </td>
                   </tr>
                   

                    <tr>
                       <td colspan="4" >
                           <h2>  <asp:Label ID="LblClasificadores" runat="server" Text="Clasificadores"></asp:Label>
                               
                           </h2>
                           </td>
                        </tr>
                   <tr>
                       <td colspan="4" >
                           <h4> <asp:Label ID="LblClasifi" runat="server" Text=""></asp:Label>
                               
                           </h4>
                           </td>
                        </tr>

                   

                   <tr>
                       <td colspan="4">
                           <asp:TreeView ID="TvNavegar" runat="server" cssclass="item" ImageSet="Arrows" ShowCheckBoxes="Leaf" Width="16px">
                               <HoverNodeStyle Font-Bold="True" Font-Size="Small" Font-Underline="True" ForeColor="#5555DD" />
                               <LeafNodeStyle Font-Size="Medium" ForeColor="#133e65" />
                               <NodeStyle Font-Names="Tahoma" Font-Size="10pt" ForeColor="Black" HorizontalPadding="5px" NodeSpacing="0px" VerticalPadding="0px" />
                               <ParentNodeStyle BackColor="#133e65" Font-Bold="False" Font-Size="Large" ForeColor="White" />
                               <RootNodeStyle Font-Size="Large" Font-Underline="False" ForeColor="#003300" />
                               <SelectedNodeStyle Font-Underline="True" ForeColor="#5555DD" HorizontalPadding="0px" VerticalPadding="0px" />
                           </asp:TreeView>
                       </td>
                   </tr>
                    <tr>
                            <td colspan="2">

                                <asp:Label ID="LblMensajesBasicos" runat="server" BackColor="#FFCCFF" Font-Bold="True" Font-Size="Large" ForeColor="#990000"></asp:Label>

                            </td>
                        </tr>
                     <tr>
                                    <td colspan="4">

                                                                    <br />



                                        <asp:Button ID="cmdGuardarBasicos" runat="server" BackColor="#133e65" class="btn btn-primary btn-lg" Height="47px" Text="Guardar" Width="150px" />
                              

                                                                    <br />
                              

                                                                    <br />
                              

                                    </td>
                                </tr>

                   </table>
           </asp:view>
         
               
           <asp:View ID="VDocumentos" runat="server">
             <table>
                   <tr>
                       <td colspan="4" >
                           <h2>  <asp:Label ID="Label2" runat="server" Text="Documentos anexos"></asp:Label></h2>
                       </td>
                   </tr>
                  </table>
                <table style="width: 950px">
                   
                      <tr>
                        <td colspan="2">
                            &nbsp;</td>
                        
                    </tr>

                      <tr>
                        <td colspan="2" style="height: 22px">
                              <asp:Label ID="LblErrorAnexos" runat="server" BackColor="#FFCCFF" Font-Bold="True" Font-Size="Large" ForeColor="#990000" Visible="False"></asp:Label>
                              </td>
                                          
                                <tr>
                                    <td>
                                        <h4>&nbsp;</h4>
                                        <h4>
                                            <asp:Label ID="LblNaturaleza" runat="server" Text="Naturaleza Documento"></asp:Label>
                                         </h4>
                                    </td>
                                    <td>
                                        <asp:ListBox ID="LstTipoNaturaleza" runat="server" CssClass="form-control" Rows="1" Width="364px" BackColor="#FFFFCC" Font-Size="Medium" AutoPostBack="True" ></asp:ListBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h4>&nbsp;</h4>
                                        <h4>
                                            <asp:Label ID="LblTtipoDocumeto" runat="server" Text="Clase de documento"></asp:Label>
                                         </h4>
                                    </td>
                                    <td>
                                        <asp:ListBox ID="LstTipoDocumento" runat="server" CssClass="form-control" Rows="1" Width="364px" BackColor="#FFFFCC" Font-Size="Medium" AutoPostBack="True" ></asp:ListBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h4>&nbsp;</h4>
                                        <h4>
                                            <asp:Label ID="LbltipoInformacion" runat="server" Text="Tipo de Informacion"></asp:Label>
                                         </h4>
                                    </td>
                                    <td>
                                        <asp:ListBox ID="LstTipoInformacionArchivo" runat="server" CssClass="form-control" Rows="1" Width="364px" BackColor="#FFFFCC" Font-Size="Medium" AutoPostBack="True" ></asp:ListBox>
                                    </td>
                                </tr>
                                <tr>                        
                                    <td style="width: 941px" >
                                        <h4> <asp:Label ID="LbldescripcionArchivo" runat="server" Text="Descripcion Anexo"></asp:Label> </h4>
                                    </td>
                                    <td style="width: 673px"> 
                                        
                                        <asp:TextBox ID="TxtDescrArchivo" onKeyUp="return limitarTextoArchivo(event,this.value,1000)" onKeyDown="return limitarTextoArchivo(event,this.value,1000)" runat="server" Height="99px" TextMode="MultiLine" Width="745px" BackColor="#FFFFCC" MaxLength="1000"></asp:TextBox>        
                                    </td>
                                </tr>    
                                <tr>
                                   <td colspan="3">                                                       
                                        Número máximo de caracteres 1000 : <asp:Label ID="lblContadorCaracteresArchivo" runat="server" Text="0"></asp:Label>                                                                                                             
                                   </td>                       
                               </tr>                               
                               
                                <tr>                                   
                                    <td style="height: 36px">
                                        <h4><asp:Label ID="LblTSeleccionar" runat="server" Text="Seleccionar documento(s)"></asp:Label>
                                            </h4>
                                    </td>                                     
                                    <td style="height: 36px">
                                        <asp:FileUpload ID="FileUpload1" runat="server" AllowMultiple="True" ForeColor="Maroon" Height="33px" Width="381px" BackColor="#FFFFCC"  />
                                    </td>                                    
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="Label3" runat="server" Text="No cargue bases de datos, estas las puede cargar y consultar en Metabase"></asp:Label>
                                    </td>
                                </tr>    
                            
                                <tr>
                                    <td colspan="2">
                                        <br />
                                        <br />
                                        <asp:Button ID="CmdAsociarDocumento" runat="server" CssClass=" btn btn-default btn-lg" Height="48px" Text="Anexar documento" Width="235px" ForeColor="White"  BackColor="#133e65" />
                                        &nbsp;<br />
                                        <br />
                                        <br />
                                    </td>
                                </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="LblMensajeTDocumentos" runat="server" Text="Total Anexos:0"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="LblMensajeAlerta" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    </table>
                    <table>
                                <tr>
                                    <td colspan="2">
                                        <asp:GridView ID="GVAnexos" runat="server" AutoGenerateColumns="False" Caption="Documentos aportados" CellPadding="3" CssClass="form-control" Height="138px" ShowFooter="True" style="margin-right: 0px; margin-bottom: 0px;" Width="1175px" Font-Italic="False" Font-Size="Medium" AutoGenerateDeleteButton="True" PageSize="60" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellSpacing="3" GridLines="Vertical">
                                            <AlternatingRowStyle Wrap="True" />
                                            <Columns>
                                                <asp:BoundField DataField="TipoDocumento" HeaderText="Tipo de Documento">
                                                
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderStyle-Width="5%" HeaderText="Descargar">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="HyperLink2" runat="server" ImageUrl="~/Imagenes/descargar.png" NavigateUrl='<%# String.Format("~/Wdownloader.aspx?guid={0}&fuente={1}", Eval("certificado"), LblFuente.Text) %>' Text='<%# Eval("certificado")%>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="5%" />
                                                 </asp:TemplateField>
                                                <asp:BoundField DataField="NombreDocumento" HeaderText="Nombre documento" />
                                                <asp:BoundField DataField="Certificado" HeaderText="Certificado">
                                               
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Descripcion" HeaderText="Descripción" />
                                                <asp:BoundField DataField="clasificacion" HeaderText="Clasificación" />
                                            </Columns>
                                            <FooterStyle BackColor="White" ForeColor="#000066" />
                                            <HeaderStyle BackColor="#133e65" Font-Bold="True" ForeColor="White" />
                                            <PagerSettings PageButtonCount="60" />
                                            <PagerStyle BackColor="White" ForeColor="#000066" />
                                            <RowStyle ForeColor="#000066" />
                                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" Width="40px" />
                                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                            <SortedDescendingHeaderStyle BackColor="#00547E" />
                                        </asp:GridView>
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <asp:Label ID="LbLFileName" runat="server" CssClass="col-md-10 control-label colored-text" Visible="False"></asp:Label>
                                        <asp:Label ID="LblNombArch" runat="server" CssClass="col-md-10 control-label colored-text" Visible="False"></asp:Label>
                                        <asp:Label ID="LblHash" runat="server" CssClass="col-md-10 control-label colored-text" Visible="False"></asp:Label>
                                        <br />
                                        <br />
                                    </td>
                                </tr>
                        <tr>
                            <td colspan="2">

                                <asp:Label ID="LblMensajesDocumentos" runat="server" BackColor="#FFCCFF" Font-Bold="True" Font-Size="Large" ForeColor="#990000"></asp:Label>

                            </td>
                        </tr>
                              
                          <tr>
                       <td colspan="4" >

                           <br />

                       </td>
                   </tr>

                            </table>
                    



                 
  
              </asp:View>
              <asp:View ID="Vjuridicos" runat="server">
                  <table style="width: 1045px">
                      <tr>
                          <td colspan ="2">
                            <h2>  <asp:Label ID="Label11" runat="server" Text="Fundamentos Juridicos de clasificación de la fuente como reservada o clasificada"></asp:Label></h2>
                              <p>
                                  &nbsp;</p>
                          </td>
                      </tr>
                      <tr>
                           
                           <td style="width: 556px">
<h4><asp:Label ID="Label7" runat="server" Text="Tipo de información" ></asp:Label></h4>
                       </td>
                       <td style="width: 673px">
    <asp:ListBox ID="LstTipoInformacion" runat="server" AutoPostBack="True" CssClass="form-control" Rows="1" Width="316px"></asp:ListBox>

                       </td>
                      </tr>
                      <tr>
                           <td style="width: 556px">
<h4><asp:Label ID="Label43" runat="server" Text="¿Contiene datos sensibles?" ></asp:Label></h4>
                       </td>
                          <td align=left>
                               <asp:RadioButton ID="RbtSiSensible" runat="server" Checked="True" Text="Si" AutoPostBack="True" />
                               <asp:RadioButton ID="RbtNoSensible" runat="server" Text="No" AutoPostBack="True" />
                              
                          </td>
                      </tr>
                      </table>
                 <asp:Panel ID="PanelTiposInformacion" runat="server" Visible="false" Width="1204px">

                  
                  <table style="width: 1199px">
                      <tr>
                        <td style="width: 941px"> <h4><asp:Label ID="Label12" runat="server" Text="Fundamento Juridico"></asp:Label> </h4>
                            <br />
                          </td>
                        <td style="width: 673px"> <asp:ListBox ID="LstFundamentoJuridico" runat="server" AutoPostBack="True" CssClass="form-control" Rows="1" Width="729px"></asp:ListBox>                        
                          </td>
                          </tr>
                         <tr>
                            <td style="width: 941px"> <h4><asp:Label ID="Label14" runat="server" Text="Objetivo Legitimo"></asp:Label></h4> 
                            <br />
                          </td>
                        <td style="width: 673px"> <asp:ListBox ID="LstObjetivoLegitimo" runat="server" AutoPostBack="True" CssClass="form-control" Rows="1" Width="729px"></asp:ListBox>                        
                          </td>
                        
                      </tr>
                      <tr>
                        <td style="width: 941px"><h4> <asp:Label ID="Label15" runat="server" Text="Fundamento constitucional y legal"></asp:Label> </h4>
                            <br />
                          </td>
                        <td style="width: 673px"> <asp:ListBox ID="LstFundamentoLegal" runat="server" AutoPostBack="True" CssClass="form-control" Rows="1" Width="729px"></asp:ListBox>                        
                          </td>
                        
                      </tr>
                            <tr>
                        
                          <td style="width: 941px" ><h4> <asp:Label ID="Label13" runat="server" Text="Ampliación de la justificación"></asp:Label> </h4></td>
                          <td style="width: 673px"> <asp:TextBox ID="TxtJustificacion" runat="server" Height="99px" TextMode="MultiLine" Width="745px" BackColor="#FFFFCC"></asp:TextBox>
                        </td>
                      </tr>
                      
                </table>
                     
                      </asp:Panel>
                  <table style="width: 1035px">

                      <tr>
                          <td colspan="2">

                              <br />
                              <asp:Label ID="LblMensajesJuridicos" runat="server" BackColor="#FFCCFF" Font-Bold="True" Font-Size="Large" ForeColor="#990000"></asp:Label>
                              <br />

                          </td>
                      </tr>

                      <tr>
                          <td colspan="2">     
                              <br />
                              <asp:Button ID="CmdGuardarJuridico" runat="server" BackColor="#133e65" class="btn btn-primary btn-lg" Height="47px" Text="Guardar" Width="150px" />
                              <br />
                              <br />
          </td>
                      </tr>

                  </table>

               </asp:View>

               <asp:View ID="VDatosAvanzados" runat="server">
                   <table aria-multiline="True" aria-orientation="vertical" style="width: 1241px" >
                    
                   <tr>
                       <td colspan="4" style="height: 40px">
                              <h2>  <asp:Label ID="Label5" runat="server" Text="Datos avanzados"></asp:Label></h2>
                         
                       </td>
                   </tr>

                       <tr>
                           <td style="width: 501px"> <h4> <asp:Label ID="Label18" runat="server" Text="Forma de acceso"></asp:Label> </h4> </td>
                           <td> <asp:ListBox ID="LstFormaAcceso" runat="server" AutoPostBack="False" CssClass="form-control" Rows="1" Width="248px" Height="32px"></asp:ListBox> </td>
                       </tr>

                       <tr>
                                              
                           <td style="width: 501px"><h4> <asp:Label ID="Label19" runat="server" Text="Es fuente primaria"></asp:Label> </h4> </td>
                           <td> 
                               <asp:RadioButton ID="RbtSiPrimaria" runat="server" Checked="True" Text="Si" AutoPostBack="True" />
                               <asp:RadioButton ID="RbtNoPrimaria" runat="server" Text="No" AutoPostBack="True" />
                           </td>
                              <td style="width: 539px"><h4> <asp:Label ID="LblSubfuentes" runat="server" Text="Subfuentes contenidas"></asp:Label> </h4> </td>
                           <td> 
                               <asp:TextBox ID="TxtSubfuentes" runat="server" Width="181px" Height="28px"></asp:TextBox>
                           </td> 
                       </tr>

                       
                          <tr>
                           <td style="width: 539px"> <h4> <asp:Label ID="Label23" runat="server" Text="Certificado de origen"></asp:Label> </h4> </td>
                           <td> 
                               <asp:TextBox ID="TxtCertificado" runat="server" Width="181px" Height="28px" ></asp:TextBox>
                              </td>                        
                           <td style="width: 501px"> <h4> <asp:Label ID="Label24" runat="server" Text="Algoritmo de certificado"></asp:Label> </h4> </td>
                           <td> 
                               <asp:TextBox ID="TxtAlgoritmoCertificado" runat="server" Width="181px" Height="28px"></asp:TextBox>
                              </td>
                          </tr>

                         <tr>
                           <td style="width: 539px"> <h4><asp:Label ID="LblnumeroVariables" runat="server" Text="Número de variables"></asp:Label>  </h4></td>
                           <td> 
                               <asp:TextBox ID="TxtNumeroVariables" runat="server" Width="181px" Height="28px"></asp:TextBox>
                             </td>                        
                           <td style="width: 501px"><h4> <asp:Label ID="LblCantidadRegistros" runat="server" Text="Cantidad de registros"></asp:Label> </h4> </td>
                           <td> 
                               <asp:TextBox ID="TxtCantidadRegistros" runat="server" Width="181px" Height="28px"></asp:TextBox>
                             </td>
                          </tr>


                        
                        <tr>
                           <td style="width: 539px"><h4> <asp:Label ID="Label27" runat="server" Text="Delimitación inicial temporal de la fuente"></asp:Label> </h4> </td>
                           <td align="left">                                                               
                               <asp:TextBox ID="txtFecInicia" runat="server"  BackColor="#FFFFCC" Height="30px" Width="250px" MaxLength="10"></asp:TextBox>
                            </td>                        
                           <td style="width: 501px"><h4> <asp:Label ID="Label28" runat="server" Text="Delimitación terminación temporal de la fuente"></asp:Label> </h4> </td>
                           <td align="left">                                
                               <asp:TextBox ID="txtFecTermina" runat="server"  BackColor="#FFFFCC" Height="30px" Width="250px" MaxLength="10"></asp:TextBox>
                            </td>
                        </tr>
                       <caption>
                           <hr />
                           <tr>
                               <td style="width: 539px">
                                   <h4>
                                       <asp:Label ID="Label29" runat="server" Text="Fecha de corte"></asp:Label>
                                   </h4>
                               </td>
                               <td align="left">                                                                      
                                   <asp:TextBox ID="txtFecCarga" runat="server"  BackColor="#FFFFCC" Height="30px" Width="250px" MaxLength="10"></asp:TextBox>
                               </td>
                               <td style="width: 501px">
                                   <h4>
                                       <asp:Label ID="Label30" runat="server" Text="Fecha de carga"></asp:Label>
                                   </h4>
                               </td>
                               <td align="left">                                   
                                   <asp:TextBox ID="txtFecCorte" runat="server"  BackColor="#FFFFCC" Height="30px" Width="250px" MaxLength="10"></asp:TextBox>
                               </td>
                           </tr>
                           <tr>
                               <td style="width: 539px">
                                   <h4>
                                       <asp:Label ID="Label21" runat="server" Text="Se encuentra vigente"></asp:Label>
                                   </h4>
                               </td>
                               <td>
                                   <asp:RadioButton ID="RbtSiActiva" runat="server" AutoPostBack="True" Checked="True" Text="SI" />
                                   <asp:RadioButton ID="RbtNoActiva" runat="server" AutoPostBack="True" Text="NO" />
                               </td>
                               <td style="width: 501px">
                                   <h4>
                                       <asp:Label ID="Label22" runat="server" Text="Tiene revisión de calidad"></asp:Label>
                                   </h4>
                               </td>
                               
                               <td>
                                   <br />
                                   <br />
                                   <asp:ListBox ID="LstTiposRevisonCalidad" runat="server" AutoPostBack="False" CssClass="form-control" Rows="1" Width="248px" Height="32px"></asp:ListBox> 
                                   <br />
                                   <asp:Label ID="Label9" runat="server" Text="Solo aplica para bases de datos"></asp:Label>
                               </td>
                           </tr>
                           <tr>
                               <td style="width: 539px">
                                   <h4>
                                       <asp:Label ID="Label8" runat="server" Text="Se encuentra disponible"></asp:Label>
                                   </h4>
                               </td>
                               <td>
                                   <asp:RadioButton ID="RbtSiDisponible" runat="server" AutoPostBack="True" Checked="True" Text="SI" />
                                   <asp:RadioButton ID="RbtNoDisponible" runat="server" AutoPostBack="True" Text="NO" />
                               </td>
                           </tr>
                           <tr>
                               <td style="width: 539px">
                                   <h4>
                                       <asp:Label ID="Label36" runat="server" Text="Forma de tradicción"></asp:Label>
                                   </h4>
                               </td>
                               <td>
                                   <asp:ListBox ID="LstFormaTradiccion" runat="server" AutoPostBack="True" CssClass="form-control" Rows="1" Width="200px"></asp:ListBox>
                               </td>
                               <td>
                                   <h4>
                                       <asp:Label ID="LblAplicabilidad" runat="server" Height="28px" Text="Porcentaje de aplicabilidad"></asp:Label>
                                   </h4>
                               </td>
                               <td>
                                   <asp:Label ID="LblPorcentajeAplicabilidad" runat="server"></asp:Label>
                               </td>
                           </tr>
                           <tr>
                               <td style="width: 539px">
                                   <h4>
                                       <asp:Label ID="Label31" runat="server" Text="Universo de estudio"></asp:Label>
                                   </h4>
                               </td>
                               <td colspan="3">
                                   <asp:TextBox ID="TxtUniverso" runat="server" BackColor="#FFFFCC" Height="134px" TextMode="MultiLine" Width="756px"></asp:TextBox>
                                   <br />
                                   <br />
                               </td>
                           </tr>
                           <tr>
                               <td style="width: 501px">
                                   <h4>
                                       <asp:Label ID="Label32" runat="server" Text="Limitaciones"></asp:Label>
                                   </h4>
                               </td>
                               <td colspan="3">
                                   <asp:TextBox ID="TxtLimitaciones" runat="server" BackColor="#FFFFCC" Height="134px" TextMode="MultiLine" Width="758px"></asp:TextBox>
                                   <br />
                                   <br />
                               </td>
                           </tr>
                           <tr>
                               <td style="width: 501px">
                                   <h4>
                                       <asp:Label ID="LblSobreEstructura" runat="server" Text="Sobre la estructura y relacionamiento"></asp:Label>
                                   </h4>
                               </td>
                               <td colspan="3">
                                   <asp:TextBox ID="TxtSobreEstructura" runat="server" BackColor="#FFFFCC" Height="134px" TextMode="MultiLine" Width="757px"></asp:TextBox>
                                   <br />
                               </td>
                           </tr>
                       </caption>
                       </table>
                   <asp:Panel ID="PanelDiccionario" runat="server" Visible="true" Width="1201px">

                   
                   <table>
                       <tr>
                           <td>
                               <h2>  <asp:Label ID="Label35" runat="server" Text="Diccionario de datos"></asp:Label></h2>
                               </td>
                       </tr>
                       
                       <tr>
                           <td>
                               <asp:GridView ID="GvDiccionario" runat="server" AutoGenerateColumns="False" Caption="Diccionario de datos" CellPadding="4" Font-Italic="False" Font-Size="Medium" ForeColor="#333333" GridLines="None" Height="166px" HorizontalAlign="Justify" PageSize="60" ShowFooter="True" ShowHeaderWhenEmpty="True" style="margin-right: 0px; margin-bottom: 0px;" Width="1172px">
                                   <AlternatingRowStyle BackColor="White" Wrap="True" />
                                   <Columns>
                                       <asp:TemplateField>
                    <HeaderTemplate>
                        Seleccionar
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkSelect" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                                       <asp:BoundField DataField="Variable" HeaderText="Nombre Variable">
                                       <HeaderStyle HorizontalAlign="Left" />
                                       <ItemStyle HorizontalAlign="Left" Width="50%" />
                                       </asp:BoundField>
                                       <asp:BoundField DataField="Descripcion" HeaderText="Descripción">
                                       <HeaderStyle HorizontalAlign="Left" />
                                       <ItemStyle HorizontalAlign="Left" Width="50%" />
                                       
                                       </asp:BoundField>
                                   </Columns>
                                   <EditRowStyle BackColor="#7C6F57" />
                                   <FooterStyle BackColor="#FFD700" Font-Bold="True" ForeColor="White" />
                                   <HeaderStyle BackColor="#133e65" Font-Bold="True" ForeColor="White" />
                                   <PagerSettings PageButtonCount="60" />
                                   <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                                   <RowStyle BackColor="#E3EAEB" />
                                   <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" Width="40px" />
                                   <SortedAscendingCellStyle BackColor="#F8FAFA" />
                                   <SortedAscendingHeaderStyle BackColor="#246B61" />
                                   <SortedDescendingCellStyle BackColor="#D4DFE1" />
                                   <SortedDescendingHeaderStyle BackColor="#15524A" />
                               </asp:GridView>
                           </td>
                          
                       </tr>
                       <tr>
                           <td>

                            <h4>   <asp:Label ID="LblTotalColumnas" runat="server" Text="Total Columnas:"></asp:Label></h4>

                           </td>
                       </tr>
                       </table>
                       </asp:Panel>                   
                       <table>     
                           <tr>
                               <td>

                                   <br />
                                   <asp:Label ID="LblMensajesAvanzados" runat="server" BackColor="#FFCCFF" Font-Bold="True" Font-Size="Large" ForeColor="#990000"></asp:Label>
                                   <br />

                               </td>
                           </tr>
                       
                            <tr>
                           <td colspan="2">

                               <br />
                               <asp:Button ID="CmdGuardarAvanzado" runat="server" BackColor="#133e65" class="btn btn-primary btn-lg" Height="47px" Text="Guardar" Width="150px" />
                               <br />
                               <br />

                           </td>

                       </tr>



                   </table>
               </asp:View>

             <asp:View ID="VcontrolAcceso" runat="server">
                    <table aria-multiline="True" aria-orientation="vertical" style="width: 1241px" >
                    
                   <tr>
                       <td colspan="2" style="height: 40px">
                              <h2>  <asp:Label ID="Label1" runat="server" Text="Control de acceso"></asp:Label></h2>
                         
                       </td>
                   </tr>

                       <tr>
                           <td style="width: 501px" colspan="2"> <h4> 
                               <asp:CheckBox ID="ChkSeleccionarTodos" runat="server" Text="Seleccionar todos" AutoPostBack="True" />
                               </h4> </td>                           
                       </tr>
                    </table>
                 <table>
                     <tr>
                         <td>
                             <asp:GridView ID="GvControlAcceso" runat="server" AutoGenerateColumns="False" CellPadding="4" Font-Italic="False" Font-Size="Medium" ForeColor="#333333" GridLines="None" Height="166px" HorizontalAlign="Justify" PageSize="60" ShowFooter="True" ShowHeaderWhenEmpty="True" style="margin-right: 0px; margin-bottom: 0px;" Width="1172px">
                                   <AlternatingRowStyle BackColor="White" Wrap="True" />
                                   <Columns>
                                       <asp:TemplateField>
                    <HeaderTemplate>
                        Autorizar
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkSelect" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                                       <asp:BoundField DataField="Despacho" HeaderText="Despacho autorizado">
                                       <HeaderStyle BackColor="#133e65" ForeColor="White" HorizontalAlign="Left" VerticalAlign="Middle" Width="200px" />
                                       <ItemStyle ForeColor="#133e65" HorizontalAlign="Left" Width="50%"  />
                                       </asp:BoundField>
                                       <asp:BoundField DataField="Titular" HeaderText="Titular">
                                       <HeaderStyle HorizontalAlign="Left" />
                                       <ItemStyle HorizontalAlign="Left" Width="50%" />
                                       </asp:BoundField>
                                       <asp:BoundField DataField="Id" HeaderText="Id" />
                                   </Columns>
                                   <EditRowStyle BackColor="#7C6F57" />
                                   <FooterStyle BackColor="#FFD700" Font-Bold="True" ForeColor="White" />
                                   <HeaderStyle BackColor="#133e65" Font-Bold="True" ForeColor="White" />
                                   <PagerSettings PageButtonCount="60" />
                                   <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                                   <RowStyle BackColor="#E3EAEB" />
                                   <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" Width="40px" />
                                   <SortedAscendingCellStyle BackColor="#F8FAFA" />
                                   <SortedAscendingHeaderStyle BackColor="#246B61" />
                                   <SortedDescendingCellStyle BackColor="#D4DFE1" />
                                   <SortedDescendingHeaderStyle BackColor="#15524A" />
                               </asp:GridView>
                         </td>
                     </tr>
                     <tr>
                         <td colspan="2">

                             <br />
                             <asp:Label ID="LblMensajesPermisos" runat="server" BackColor="#FFCCFF" Font-Bold="True" Font-Size="Large" ForeColor="#990000"></asp:Label>
                             <br />

                         </td>
                     </tr>
                     <tr>
                         
                         <td colspan="2">

                             <br />
                             <asp:Button ID="CmdGuardarAccesos" runat="server" BackColor="#133e65" class="btn btn-primary btn-lg" Height="47px" Text="Guardar" Width="150px" />
                             <br />
                             <br />

                         </td>
                     </tr>
                 </table>
             </asp:View>

           </asp:MultiView>

</asp:Content>
