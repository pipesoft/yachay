﻿<%@ Page Title="Acerca de JEP" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.vb" Inherits="SICA.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Acerca de la SEJEP</h2>
        <h4><span class="colored-text">Acerca de la Secretaria Ejecutiva de la Jurisdicción Especial para la paz</span> </h4>

    <p>El "acuerdo especial de ejecución para seleccionar al Secretario Ejecutivo de la Jurisdicción Especial para la Paz y asegurar su oportuna puesta en funcionamiento”, suscrito el pasado 19 de agosto de 2016 en la Habana, integrado al Acuerdo Final de Paz, confió a la Organización de las Naciones Unidas (en adelante ONU) la designación provisional del Secretario Ejecutivo de la Jurisdicción Especial para la Paz (en adelante JEP), para que actúe transitoriamente como funcionario de dicha organización, mientras la jurisdicción es creada dentro de la estructura del Estado.</p>

<p> El 26 de enero de 2017, el Jefe de la Misión de Naciones Unidas en Colombia, designó provisionalmente en el cargo al señor  Néstor Raúl Correa Henao. Posteriormente, el pasado 12 de mayo del mismo año, el Comité de Selección de integrantes del Sistema Integral de Verdad, Justicia, Reparación y No Repetición previsto en el Acuerdo Final, confirmó la designación en el cargo de Secretario Ejecutivo de la JEP, momento desde el cual quedaron atribuidas de forma permanente las funciones previstas en el Acuerdo de Paz y el Acto Legislativo 1 de 2017, dentro de las que se destacan: (i) la creación y puesta en marcha de la JEP, en coordinación con el Gobierno Nacional (Ministerio de Justicia y del Derecho); (ii) la suscripción y recolección de actas de compromiso, (iii) la preparación de un informe con destino a las salas de la Jurisdicción, de acuerdo a los parámetros del Acuerdo Final, y (iv) la realización provisional de verificación del contenido reparador de las actuaciones de los potenciales procesados y adoptar medidas cautelares sobre archivos cuando se requiera. </p>

<p>Con el fin de cumplir a cabalidad las funciones anteriormente mencionadas, se vienen desarrollando herramientas tecnológicas e informáticas, entre las que se encuentra la consulta web para que las autoridades judiciales y los organismos de control puedan validar la información de las personas que han firmado acta formal de compromiso.</p>

    
<p>&nbsp;</p>
</asp:Content>
