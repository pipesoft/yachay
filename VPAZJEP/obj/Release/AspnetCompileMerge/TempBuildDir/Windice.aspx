﻿<%@ Page Title="Índice" aspcompat=true  Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Windice.aspx.vb" Inherits="SICA.Windice" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Panel ID="PMensajes" runat="server" Visible="true" Width="1201px">
           <asp:HyperLink ID="RegisterHyperLink" runat="server" Font-Bold="True" ForeColor="#CC0000" NavigateUrl="~/Register.aspx" ViewStateMode="Disabled" Visible="False">Debe estar registrado para poder acceder. Registrese aqui.</asp:HyperLink>
            <br />
                <asp:HyperLink ID="IngreseHyperLink" runat="server" Font-Bold="True" ForeColor="#CC0000" NavigateUrl="~/login.aspx" ViewStateMode="Disabled" Visible="False">Si ya tiene usuario y esta registrado: Ingrese aqui.</asp:HyperLink>
                <br />
                <asp:Label ID="Lblaviso" runat="server" Text="Se manifiesta que los datos aquí suministrados quedaran incorporados en el índice de fuentes estructuradas y no estructuradas con que cuenta la JEP, sus características y el órgano de la JEP que la tiene disponible para su uso."></asp:Label>
                <br />
                <br />
     </asp:Panel>

    <style type="text/css">
.item td { vertical-align:top;}
</style>

    <asp:Panel ID="PInicio" runat="server" Visible="false" Width="1204px">
        <table style="width: 1042px">
             <tr>                    
                 <td>

                     <asp:TextBox ID="TxtBuscar" runat="server" Height="36px" Width="900px" BackColor="White"></asp:TextBox>
                     <asp:ImageButton ID="ImgBuscar" runat="server" Height="37px" ImageUrl="~/Imagenes/lupa.jpg" Width="39px" />            
                 </td>                 
                </tr>                                                    
                <tr>
                    <td colspan="3">                     
                        <asp:Label ID="Label1" runat="server" Text="Digite palabras claves de la fuente registrada. Ejemplo:Titulo de la fuente"></asp:Label>
                    </td>
                </tr>
             </table>
                     

     
 
    <asp:Panel ID="POpciones" runat="server" Visible="false" Width="1199px">
        <table style="width: 1045px; ">
             <tr>
              
              

                 <td style="width: 296px" >                      
                                        <br />
                                        <br />
                         <br />
                     </td>
                     <td style="width: 296px"  >                      
                                        <asp:LinkButton ID="LbtExportar" runat="server">Exportar índice</asp:LinkButton>
                         
                     </td>
                  <td style="width: 296px" >                      
                                        <asp:LinkButton ID="LbtAvanzadas" runat="server">Índice</asp:LinkButton>                                        
                     </td>
                     <td style="width: 296px" >                      
                                                            
                                        <asp:LinkButton ID="LbtAgregar" runat="server" PostBackUrl="~/Wnuevafuente.aspx">Nueva Ficha de Fuente</asp:LinkButton>                   
                     
                     
                  
                 <td style="width: 296px" >                      
                                        <%--<asp:LinkButton ID="LbtBorrar" runat="server">Borrar</asp:LinkButton>--%>
                                        <asp:Label ID="LblRol" runat="server" Visible="False" ForeColor="#006600"></asp:Label>
                     </td>
                 </tr>
                  
    </table>

        
    </asp:Panel>
    
        <asp:MultiView ID="MultiView1" runat="server">
         <asp:view ID="VResultados" runat="server">
             <div>
             <div style="padding: 10px; float: left; width: 70%; text-align: justify;">
             <table >
                 <tr>
                      <td >
                         <h2>&nbsp;<asp:Label ID="Lblresultados" runat="server" Text="Resultados encontrados:" ForeColor="Maroon"></asp:Label>
                          </h2>
                     </td>
                 </tr>

                 <tr>
                     <td colspan="2" >
                            


                         <asp:GridView ID="GVResultados" runat="server" AutoGenerateColumns="False" BorderStyle="None" Font-Italic="False" Font-Size="Medium" GridLines="None" Height="195px" HorizontalAlign="Justify" PageSize="60" ShowFooter="True" ShowHeaderWhenEmpty="True" style="margin-right: 0px; margin-bottom: 0px;" Width="800px">
                             <AlternatingRowStyle Wrap="True" />
                             <Columns>
                                 <asp:BoundField DataField="Reg">
                                 <HeaderStyle Width="3%" Wrap="False" />
                                 <ItemStyle Width="3%" Wrap="False" />
                                 </asp:BoundField>
                                 <asp:TemplateField Visible="False">
                                     <ItemTemplate>
                                         <asp:CheckBox ID="seleccionar" runat="server" Visible="true" />
                                     </ItemTemplate>
                                     <ItemStyle Width="1%" />
                                 </asp:TemplateField>
                                 
                                 <asp:TemplateField HeaderStyle-Width="3%" HeaderText="" Visible="False">
                                     <ItemTemplate>
                                         <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# String.Format("Wficha_tecnica.aspx?guid={0}", Eval("idfuente")) %>' Text='<%# Eval("idFuente")%>'></asp:HyperLink>
                                     </ItemTemplate>
                                     <HeaderStyle Width="3%" />
                                     <ItemStyle HorizontalAlign="Center" Width="3px" />
                                 </asp:TemplateField>

                                 <asp:TemplateField HeaderStyle-Width="50%" HeaderText="">
                                     <ItemTemplate>
                                     <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl='<%# String.Format("Wficha_tecnica.aspx?guid={0}", Eval("idfuente")) %>' Text='<%# Eval("Detalle")%>'></asp:HyperLink>
                                     </ItemTemplate>
                                     <HeaderStyle Width="50%" />
                                     <ItemStyle HorizontalAlign="Left" Width="3px" ForeColor="#000099" />
                                 </asp:TemplateField>
                                 
                             </Columns>
                             <PagerSettings PageButtonCount="60" />
                             <SelectedRowStyle Width="40px" />
                         </asp:GridView>
                            


                     </td>
                   
                 </tr>
                 </table>
                 </div>
             <div style="padding: 10px; float: right; width: 30%; text-align: justify;">
                 <table >
                     <tr>
                         <td>

                         
              <asp:Panel ID="PdetalleFuente" runat="server" Height="400px" style="margin-left: 0px" Width="280px">
                             <asp:GridView ID="GVDatos2" runat="server" AutoGenerateColumns="False" BorderStyle="None" Font-Italic="False" Font-Size="Medium" GridLines="None" Height="166px" HorizontalAlign="Justify" PageSize="60" ShowFooter="True" ShowHeaderWhenEmpty="True" style="margin-right: 0px; margin-bottom: 0px;" Width="272px">
                                 <AlternatingRowStyle Wrap="True" />
                                 <Columns>
                                     <asp:BoundField DataField="Detalle" HeaderText="Novedades:">
                                     <HeaderStyle BackColor="#133e65" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" Width="500px" />
                                     <ItemStyle HorizontalAlign="Left" Width="500px" />
                                     </asp:BoundField>
                                 </Columns>
                                 <PagerSettings PageButtonCount="60" />
                                 <SelectedRowStyle Width="40px" />
                             </asp:GridView>
                         </asp:Panel>
                             </td>
                     </tr>
                     </table>
                 </div>
                 </div>
             <table>


                 <tr>
                     <td colspan="2" >
                         <asp:Label ID="LblMensajes" runat="server" BackColor="#FFCCFF" Font-Bold="True" Font-Size="Large" ForeColor="#990000" Visible="False"></asp:Label>
                     </td>
                 </tr>
                 <tr>
                     <td colspan="2">

                         &nbsp;</td>
               
                     <td>

                         &nbsp;</td>
                    
                     
                 </tr>
                 </table>
             </asp:view>

            

             <asp:view ID="Vbrowser" runat="server">


                 <br />
                 <asp:TreeView ID="TvNavegar" runat="server" cssclass="item" ImageSet="Arrows" ShowCheckBoxes="Leaf" SkipLinkText="" Width="481px">
                     <HoverNodeStyle Font-Bold="True" Font-Size="Large" Font-Underline="True" ForeColor="#5555DD" />
                     <LeafNodeStyle Font-Size="Medium" ForeColor="#133e65" />
                     <NodeStyle Font-Names="Tahoma" Font-Size="10pt" ForeColor="Black" HorizontalPadding="5px" NodeSpacing="0px" VerticalPadding="0px" />
                     <ParentNodeStyle BackColor="#133e65" Font-Bold="False" Font-Size="Large" ForeColor="White" />
                     <RootNodeStyle Font-Size="Large" Font-Underline="False" ForeColor="#003300" />
                     <SelectedNodeStyle Font-Underline="True" ForeColor="#5555DD" HorizontalPadding="0px" VerticalPadding="0px" />
                 </asp:TreeView>
                 <br />
                 <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:PYachayConnectionString %>" SelectCommand="SELECT [id_clasificador], [descripcion], [id_clasificador_padre], [estado] FROM [vClasificadores]"></asp:SqlDataSource>
                 <asp:Button ID="cmdBuscarClasificador" runat="server" BackColor="#133e65" class="btn btn-primary btn-lg" Height="47px" Text="Buscar" Width="207px" />
                 <br />
                 <br />
                 <br />


             </asp:view>
            </asp:MultiView>
          
               
                    </asp:Panel>

        
 
   
</asp:Content>
