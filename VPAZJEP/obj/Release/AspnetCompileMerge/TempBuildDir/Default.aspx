﻿<%@ Page Title="YACHAY" Language="VB" MasterPageFile="~/Site.Master" aspcompat="true" AutoEventWireup="true" CodeBehind="Default.aspx.vb" Inherits="SICA._Default" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
     <div class="row">
           
     <asp:Panel ID="Panel1" runat="server">
                        
    
            <div class="jumbotron">
                 <h3><%=Application("NombreSistema")%>- SISTEMA DE REGISTRO DE FUENTES</h3>
        <p class="lead">Este sistema permite el registro de fuentes de datos que llegan oficialmente a la JEP.</p>
                        
                   </div>     
  

   
        <div class="col-md-4">
            <h2>Dirigido a:</h2>
            
                Todos los funcionarios y contratistas autorizados que apoyan las actividades misionales de la JEP. 
                        
        </div>
        <div class="col-md-8">
            <h2>¿Cómo funciona?</h2>
            <p align="left">
               1. Las personas petenecientes a los grupos de trabajo de la JEP podrán obtener un usuario realizando el registro de sus datos, aportando su correo institucional y asignando una contraseña segura. 
              <br> 2. El sistema confirmará si la persona esta autorizada y le enviará un correo con su usuario de acceso.
              <br> 3. Si usted es parte de la JEP y tiene correo institucional pero no logra registrarse escriba al correo soporteti@jep.gov.co para que autoricen su correo.
              <br> 4. Una vez creado el usuario y la contraseña podrá iniciar sesión para acceder al sistema.
              <br> 5. Si olvida su clave tendrá que ir a la opción de Olvido de clave para reestablecer el registro de su usuario.
            </p>
         </div>
         <br />
          <br />    <br />
            <br />    <br />
            <br />
             <br />
           
   
                 </asp:Panel>
          </div>


</asp:Content>
