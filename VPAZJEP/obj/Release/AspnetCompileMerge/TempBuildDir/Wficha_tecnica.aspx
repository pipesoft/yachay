﻿<%@ Page Title="" Language="vb" AspCompat="true" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Wficha_tecnica.aspx.vb" Inherits="SICA.Wficha_tecnica" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Panel ID="PMensajes" runat="server" Visible="true" Width="1201px">
           <asp:HyperLink ID="RegisterHyperLink" runat="server" Font-Bold="True" ForeColor="#CC0000" NavigateUrl="~/Register.aspx" ViewStateMode="Disabled" Visible="False">Debe estar registrado para poder acceder. Registrese aqui.</asp:HyperLink>
            <br />
                <asp:HyperLink ID="IngreseHyperLink" runat="server" Font-Bold="True" ForeColor="#CC0000" NavigateUrl="~/login.aspx" ViewStateMode="Disabled" Visible="False">Si ya tiene usuario y esta registrado: Ingrese aqui.</asp:HyperLink>
                <br />
                <asp:Label ID="Lblaviso" runat="server" Text="Los datos aquí suministrados son activos de información con que cuenta la JEP disponible para su uso."></asp:Label>
                <br />
                <asp:Label ID="LblRol" runat="server" ForeColor="#006600" Visible="False"></asp:Label>
                <br />
                <br />
    </asp:Panel>

    <asp:Panel ID="PInicio" runat="server" Visible="false" Width="1204px">
        <h1><asp:Label ID="Lblresultados" runat="server" Text="Ficha técnica de la fuente:"></asp:Label>
            <asp:Label ID="LblFuente" runat="server"></asp:Label>
            &nbsp;<asp:Label ID="LblFuenteNombre" runat="server"></asp:Label>
        </h1>
        <asp:Panel ID="POpciones" runat="server" Visible="true" Width="1199px">
        <table style="width: 1045px; ">
             <tr>
              
              

                 <td style="width: 296px" >                      
                                        <br />
                                        <br />
                         <br />
                     </td>
                     <td style="width: 296px"  >                      
                                        <asp:LinkButton ID="LbtExportar" runat="server">Exportar ficha</asp:LinkButton>
                     </td>
                     <td style="width: 296px" >                      
                                        <asp:LinkButton ID="LbtCitar" runat="server">Agregar cita o comentario</asp:LinkButton>                                        
                     </td>
                     <td style="width: 296px" >                      
                                        <asp:LinkButton ID="LbtEditar" runat="server" PostBackUrl='<%# String.Format("Wnuevafuente.aspx?guid={0}", Eval("lblfuente.text")) %>'> Editar Ficha Fuente</asp:LinkButton>                   
                     </td>   
                     <td style="width: 296px" >                      
                                        <asp:LinkButton ID="LbtConsultar" runat="server" PostBackUrl='<%# String.Format("Wnuevafuente.aspx?guid={0}", Eval("lblfuente.text")) %>'> Consultar Ficha Fuente</asp:LinkButton>                   
                     </td>   
                     <td style="width: 296px" >                      
                                        <asp:Label ID="Label1" runat="server" Visible="False" ForeColor="#006600"></asp:Label>
                     </td>
                 </tr>
                  
    </table>

        
    </asp:Panel>

     
       <asp:MultiView ID="MultiView1" runat="server">
       
           <asp:View ID="VFichaTecnica" runat="server">
                          
            <asp:Label ID="LblFuentePadre" runat="server" Visible="False"></asp:Label>
              
            
            <asp:Label ID="LblFuente0" runat="server"></asp:Label>
               
            
       
        <asp:GridView ID="GVDatos2" runat="server" AutoGenerateColumns="False" Font-Italic="False" Font-Size="Medium" GridLines="None" Height="166px" HorizontalAlign="Justify" PageSize="60" ShowFooter="True" ShowHeaderWhenEmpty="True" style="margin-right: 0px; margin-bottom: 0px;" Width="1172px" CellPadding="4" ForeColor="#333333">
            <AlternatingRowStyle Wrap="True" BackColor="White" />
            <Columns>
                <asp:BoundField DataField="Tema" HeaderText="Característica">
                <HeaderStyle BackColor="#133e65" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" Width="200px" />
                <ItemStyle HorizontalAlign="Left" Width="200px" ForeColor="#006666" />
                </asp:BoundField>
                <asp:BoundField DataField="Detalle" HeaderText="Detalle" >
                <ItemStyle HorizontalAlign="Left" Width="800px" />
                </asp:BoundField>               

            </Columns>
            <EditRowStyle BackColor="#7C6F57" />
            <FooterStyle BackColor="#FFD700" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#133e65" Font-Bold="True" ForeColor="White" />
            <PagerSettings PageButtonCount="60" />
            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#E3EAEB" />
            <SelectedRowStyle Width="40px" BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F8FAFA" />
            <SortedAscendingHeaderStyle BackColor="#246B61" />
            <SortedDescendingCellStyle BackColor="#D4DFE1" />
            <SortedDescendingHeaderStyle BackColor="#15524A" />
        </asp:GridView>
        <br />
        <br />
     
        <br />


        <asp:GridView ID="GvDiccionario" runat="server" AutoGenerateColumns="False" Font-Italic="False" Font-Size="Medium" GridLines="None" Height="166px" HorizontalAlign="Justify" PageSize="60" ShowFooter="True" ShowHeaderWhenEmpty="True" style="margin-right: 0px; margin-bottom: 0px;" Width="1172px" CellPadding="4" ForeColor="#333333" Caption="Diccionario de datos" Visible="False">
            <AlternatingRowStyle Wrap="True" BackColor="White" />
            <Columns>
                <asp:BoundField DataField="nombre" HeaderText="Nombre Variable">
                <HeaderStyle BackColor="#006600" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" Width="200px" />
                <ItemStyle HorizontalAlign="Left" Width="200px" ForeColor="#006666" />
                </asp:BoundField>
                <asp:BoundField DataField="Descripcion" HeaderText="Descripción" >
                <ItemStyle HorizontalAlign="Left" Width="400px" />
                </asp:BoundField>               

            </Columns>
            <EditRowStyle BackColor="#7C6F57" />
            <FooterStyle BackColor="#FFD700" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#006600" Font-Bold="True" ForeColor="White" />
            <PagerSettings PageButtonCount="60" />
            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#E3EAEB" />
            <SelectedRowStyle Width="40px" BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F8FAFA" />
            <SortedAscendingHeaderStyle BackColor="#246B61" />
            <SortedDescendingCellStyle BackColor="#D4DFE1" />
            <SortedDescendingHeaderStyle BackColor="#15524A" />
        </asp:GridView>


               <br />


           <asp:GridView ID="GvComentarios" runat="server" AutoGenerateColumns="False" Font-Italic="False" Font-Size="Medium" GridLines="None" Height="166px" HorizontalAlign="Justify" PageSize="60" ShowFooter="True" ShowHeaderWhenEmpty="True" style="margin-right: 0px; margin-bottom: 0px;" Width="1172px" CellPadding="4" ForeColor="#333333" Caption="Comentarios y citas" Visible="False">
            <AlternatingRowStyle Wrap="True" BackColor="White" />
            <Columns>
                <asp:BoundField DataField="comentario">
                <HeaderStyle BackColor="#006600" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" Width="200px" />
                <ItemStyle HorizontalAlign="Left" Width="200px" ForeColor="#006666" />
                </asp:BoundField>
                <asp:BoundField DataField="dato">
                <ItemStyle HorizontalAlign="Left" Width="400px" />
                </asp:BoundField>               
         

            </Columns>
            <EditRowStyle BackColor="#7C6F57" />
            <FooterStyle BackColor="#FFD700" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#006600" Font-Bold="True" ForeColor="White" />
            <PagerSettings PageButtonCount="60" />
            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#E3EAEB" />
            <SelectedRowStyle Width="40px" BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F8FAFA" />
            <SortedAscendingHeaderStyle BackColor="#246B61" />
            <SortedDescendingCellStyle BackColor="#D4DFE1" />
            <SortedDescendingHeaderStyle BackColor="#15524A" />
        </asp:GridView>   
    </asp:View>
       <asp:View ID="Vcomentar" runat="server">

          <table >
              <tr align="left">
                  <td style="width: 99px" align="left">
                                  <asp:RadioButton ID="RbtCitar" Text="Citar" runat="server" Checked="True" AutoPostBack="True" />
                  </td>
                  <td align="left">
                      <asp:RadioButton ID="RbtComentar" Text="Comentar" runat="server" AutoPostBack="True" />
                  </td>

              </tr>
              <tr>
                  <td style="height: 20px; width: 99px;">
      <asp:Label ID="LblCitadaEn" runat="server" Text="Citada en:"></asp:Label>
         
                  </td>
                  <td style="height: 20px">
                         <asp:TextBox ID="TxtCitaComentario" runat="server" Height="29px" Width="389px" BackColor="#FFFFCC"></asp:TextBox>

                         <br />

                  </td>
              </tr>

              <tr>
                  <td style="width: 99px">
                               <asp:Label ID="LblObservacionPuntaje" runat="server" Text="Observación:"></asp:Label>
               
              
                  </td>
                  <td>
                                    <asp:TextBox ID="TxtObservacionPuntaje" runat="server" Height="103px" MaxLength="2000" TextMode="MultiLine" Width="621px" BackColor="#FFFFCC"></asp:TextBox>
    
                                    <asp:RadioButtonList ID="RbtPuntaje" runat="server" Visible="False">
                                        <asp:ListItem>1</asp:ListItem>
                                        <asp:ListItem>2</asp:ListItem>
                                        <asp:ListItem>3</asp:ListItem>
                                        <asp:ListItem>4</asp:ListItem>
                                        <asp:ListItem>5</asp:ListItem>
                                    </asp:RadioButtonList>
    
                  </td>
              </tr>
              <tr>
                  <td colspan="2">

                      <br />
                      <asp:Label ID="LblMensajes" runat="server" BackColor="#FFCCFF" Font-Bold="True" Font-Size="Large" ForeColor="#990000"></asp:Label>
                      <br />

                  </td>
              </tr>
          
              <tr>
                  <td colspan="2">
                            <asp:Button runat="server" Text="Guardar" ID="cmdGuardar" BackColor="#57992B" class="btn btn-primary btn-lg" Height="47px" Width="207px" />
                  </td>
              </tr>
                  
              </table>
              
          

           <br />

       </asp:View>
       </asp:MultiView>

    </asp:Panel>
</asp:Content>
