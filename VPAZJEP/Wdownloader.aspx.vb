﻿Imports Microsoft.AspNet.Identity
Imports System.IO
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.SqlTypes
Imports itextsharp
Imports itextsharp.text
Imports itextsharp.text.pdf
Imports System.Web.UI.WebControls.ListItem




Public Class Wdownloader
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Context.User.Identity.GetUserName() <> "" Then
            Dim guid As String = Request.QueryString("guid")
            Dim fuente As String = Request.QueryString("fuente")



            If String.IsNullOrEmpty(guid) Then Return
            ' aquí puedes poner las validaciones que quieras 
            'document_attachment_GetGuid(guid)
            '............   .................... 
            Dim dt As DataTable = Document_attachment_GetGuid(guid)
            If dt.Rows.Count = 0 Then Return
            Dim OriginalFileName As String = dt.Rows(0)("filename")
            DownloadFile2(OriginalFileName, HttpUtility.HtmlDecode(OriginalFileName))
        Else
            lblestado.Text = "Debe tener permisos, inicie seccion"

        End If
    End Sub



    Public Sub DownloadFile2(ByVal FilePath As String, ByVal OriginalFileName As String)
        Dim fs As IO.FileStream = Nothing
        'obtenemos el archivo del servidor 
        Try
            fs = IO.File.Open(FilePath, IO.FileMode.Open, IO.FileAccess.Read)
            'Dim streams As IO.Stream
            'Dim st2 As IO.Stream = IO.File.Open(FilePath, IO.FileMode.Open, IO.FileAccess.Read)
            'Dim bw2 As New IO.BinaryWriter(st2)
            'Dim byteBuffer(CInt(bw2.BaseStream.Length - 1)) As Byte

            Dim byteBuffer(CInt(fs.Length - 1)) As Byte
            fs.Read(byteBuffer, 0, CInt(fs.Length))
            fs.Close()

            Using ms As New IO.MemoryStream(byteBuffer)
                'descargar con su nombre original 
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment; filename=" & OriginalFileName)
                '  Response.AddHeader("Content-Disposition", "attachment;filename=\"" & OriginalFileName & " \ "")
                ' Response.AddHeader("Content-Disposition", "inline; filename=" & OriginalFileName)
                'Dim MyHtml As String = MyStreamReader.ReadToEnd

                '  Response.AddHeader("Pragma", "attachment; filename=" & OriginalFileName)
                'Response.AddHeader("Pragma", "no-cache")
                'Response.AddHeader("WWW-Authenticate", "BASIC")
                '      Response.ContentType = "image/jpeg"
                ' Buffer response so that page is sent
                ' after processing is complete.
                Response.BufferOutput = True
                Response.BinaryWrite(byteBuffer)
                'ms.WriteTo(Response.OutputStream)
                Response.Flush()
                Response.End()

            End Using
        Catch
            lblestado.Text = "El archivo no se encuentra disponible, reporte la anomalia a soporte técnico "
        End Try
    End Sub

    Private Function MakeValidFileName(name As String) As String
        Dim invalidChars As String = Regex.Escape(New String(System.IO.Path.GetInvalidFileNameChars()))
        Dim invalidReStr As String = String.Format("[{0}]+", invalidChars)
        Dim replace As String = Regex.Replace(name, invalidReStr, "_").Replace(";", "").Replace(",", "")
        Return replace
    End Function

    Public Sub DownloadFile(ByVal FilePath As String, ByVal OriginalFileName As String)
        Dim fs As IO.FileStream = Nothing
        'obtenemos el archivo del servidor 
        Try
            fs = IO.File.Open(FilePath, IO.FileMode.Open, IO.FileAccess.Read)
            Dim byteBuffer(CInt(fs.Length - 1)) As Byte
            fs.Read(byteBuffer, 0, CInt(fs.Length))
            fs.Close()

            Using ms As New IO.MemoryStream(byteBuffer)
                'descargar con su nombre original 
                Response.AddHeader("Content-Disposition", "attachment; filename=" & OriginalFileName)
                ms.WriteTo(Response.OutputStream)
            End Using
        Catch
            lblestado.Text = "El archivo no se encuentra disponible, reporte la anomalia a soporte técnico"
        End Try

    End Sub


    Function Document_attachment_GetGuid(ByVal guid As String)
        Dim hash As String
        Dim conexion As Object
        Dim sql2 As String
        Dim rs2 As Object
        hash = Mid(guid, 1, 16) & Mid(guid, 18, 16) & Mid(guid, 35, 16) & Mid(guid, 52, 16)
        Dim dt As DataTable = New DataTable
        dt.Columns.Add(New DataColumn("filename"))
        dt.Columns.Add(New DataColumn("certificado"))
        conexion = Server.CreateObject("ADODB.Connection")
        conexion.Open(Application("CadVJep"))

        'rs1 = conexion.Execute(sql1)
        sql2 = "select Id_fuente_est_nest,nombre_documento from aFuentes_documentos afd, mfuentes_estructuradas mfe, mFuentes mf where mf.Id_fuente=mfe.id_fuente and mfe.id_fuente_estructurada=afd.Id_fuente_est_nest and certificado_documento='" & hash & "'"
        sql2 = sql2 + " union "
        sql2 = sql2 + " Select Id_fuente_est_nest, nombre_documento from aFuentes_documentos afd, mfuentes_no_estructuradas mfe, mFuentes mf where mf.Id_fuente=mfe.id_fuente and mfe.id_fuente_noestructurada=afd.Id_fuente_est_nest and certificado_documento='" & hash & "'  "
        rs2 = conexion.Execute(sql2)
        ' j = 0
        lblestado.Text = hash
        Dim dato As String
        dato = ""
        Do Until rs2.eof
            dato = Application("DefaultFileName") & rs2("Id_fuente_est_nest").value & "\" & rs2("nombre_documento").value
            dt.Rows.Add(CreateRowDocumentoss(dato, guid, dt))
            '  dt.Rows.Add(CreateRowDocumentos(rs2("A121FECHDOCU").value, rs2("A121DESCDOCU").value, Server.MapPath(Application("DefaultFileName")) & rs2("A121NOMBDOCU").value, rs2("A121FUNCHASH").value, calcularTamaño(Server.MapPath(Application("DefaultFileName")) & rs2("A121NOMBDOCU").value), dt))
            lblestado.Text = dato
            rs2.movenext()
        Loop

        rs2.close()
        conexion.close()
        Return dt


    End Function

    Private Sub unirPDF(sfile1 As String, sfile2 As String, nombreArch As String)


        ' Creamos una lista de archivos para concatenar

        Dim Lista As New List(Of String)


        ' Los añadimos a la lista

        Lista.Add(sfile1)

        Lista.Add(sfile2)

        ' Nombre del documento resultante


        Dim sFileJoin As String = nombreArch

        Dim Doc As New Document()

        Try

            Dim fs As New FileStream(sFileJoin, FileMode.Create, FileAccess.Write, FileShare.None)

            Dim copy As New PdfCopy(Doc, fs)

            Doc.Open()

            Dim Rd As PdfReader

            Dim n As Integer 'Número de páginas de cada pdf

            For Each file In Lista

                Rd = New PdfReader(file)

                n = Rd.NumberOfPages

                Dim page As Integer = 0

                Do While page < n

                    page += 1

                    copy.AddPage(copy.GetImportedPage(Rd, page))

                Loop

                copy.FreeReader(Rd)

                Rd.Close()

            Next

        Catch ex As Exception

        Finally

            '    Cerramos el documento

            Doc.Close()

        End Try

    End Sub



    Public Function getCell(text As String, alignment As Integer, color As String) As PdfPCell
        Dim cell As PdfPCell
        Dim fuente As Font

        ' fuente = New Font(Font.FontFamily.COURIER, Font.NORMAL, 11)
        'fuente.SetColor(255, 0, 0)


        Dim selector1 As FontSelector = New FontSelector()

        fuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, 12)
        If color = "RED" Then
            fuente.SetColor(255, 0, 0)
        End If
        If color = "BLUE" Then
            fuente.SetColor(0, 0, 128)
        End If
        If color = "BLACK" Then
            fuente.SetColor(0, 0, 0)
        End If


        selector1.AddFont(fuente)




        'cell = New PdfPCell(New Phrase(text, fuente))
        cell = New PdfPCell(New Phrase(selector1.Process(text)))

        'cell = New PdfPCell(New Phrase(text, fuente))

        cell.HorizontalAlignment = alignment
        cell.Border = 0
        cell.Padding = 10
        Return cell
    End Function

    'Function Document_attachment_GetByGuid(ByVal guid As String) As DataTable
    '    Dim hash As String
    '    Dim conexion As Object
    '    Dim sql2 As String
    '    Dim rs2 As Object


    '    hash = Mid(guid, 1, 16) & Mid(guid, 18, 16) & Mid(guid, 35, 16) & Mid(guid, 52, 16)
    '    Dim dt As DataTable = New DataTable
    '    dt.Columns.Add(New DataColumn("filename"))
    '    dt.Columns.Add(New DataColumn("certificado"))
    '    conexion = Server.CreateObject("ADODB.Connection")
    '    conexion.Open(Application("CadVJep"))
    '    'rs1 = conexion.Execute(sql1)
    '    sql2 = "select fechaactualiza, NombreDocumento, certificadodocumento from aInscripcionDocumentos='" & hash & "'  "
    '    'sql2 = "SELECT A121FECHDOCU, A121NOMBDOCU, A121DESCDOCU FROM  T121DRDOCSPROC WHERE  a121llavproc = '" & session("cproceso") & "' order by a121fechdocu "
    '    rs2 = conexion.Execute(sql2)
    '    ' j = 0
    '    lblestado.Text = hash
    '    Dim dato As String
    '    dato = ""
    '    Do Until rs2.eof

    '        ' Lblruta.Text = subcarpetas(rs2("A121fechDOCU").value)
    '        'Lblarchivo.Text = rs2("A121NOMBDOCU").value


    '        dato = subcarpetas(rs2("fechaactualiza").value) & HttpUtility.HtmlDecode(rs2("nombredocumento").value)
    '        dt.Rows.Add(CreateRowDocumentoss(dato, guid, dt))
    '        '  dt.Rows.Add(CreateRowDocumentos(rs2("A121FECHDOCU").value, rs2("A121DESCDOCU").value, Server.MapPath(Application("DefaultFileName")) & rs2("A121NOMBDOCU").value, rs2("A121FUNCHASH").value, calcularTamaño(Server.MapPath(Application("DefaultFileName")) & rs2("A121NOMBDOCU").value), dt))
    '        lblestado.Text = dato
    '        rs2.movenext()


    '    Loop

    '    rs2.close()
    '    conexion.close()

    '    Return dt
    'End Function

    Function CreateRowDocumentoss(ByVal x As String, ByVal y As String, ByVal dt As DataTable) As DataRow
        ' Create a DataRow using the DataTable defined in the 
        ' CreateDataSource method.
        Dim dr As DataRow = dt.NewRow()
        dr(0) = x
        dr(1) = y
        Return dr
    End Function


End Class