﻿Public Class WConsultaRadicados
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub LstBuscar_SelectedIndexChanged(sender As Object, e As EventArgs) Handles LstBuscar.SelectedIndexChanged
        txtBuscar1.Visible = True

        TxtBuscar2.Visible = False
        lblcriterio.Text = LstBuscar.SelectedItem.Text
        LstTipoComunicacion.Visible = False
        LblError.Text = ""

        If LstBuscar.SelectedValue = "AP" Then
            'Apellidos del suscriptor
            TxtBuscar2.Visible = True
            TxtBuscar2.Text = ""
        ElseIf LstBuscar.SelectedValue = "DI" Then

        ElseIf LstBuscar.SelectedValue = "NO" Then
            'Nombre de una Entidad
        ElseIf LstBuscar.SelectedValue = "NR" Then
            'Número de radicacion
        ElseIf LstBuscar.SelectedValue = "TD" Then
            'Tipo Documental
            txtBuscar1.Visible = False
            LstTipoComunicacion.Visible = True

        End If





    End Sub

    Protected Sub cmdBuscar_Click(sender As Object, e As EventArgs) Handles cmdBuscar.Click


        If txtBuscar1.Text <> "" Or LstTipoComunicacion.Visible = True Or TxtBuscar2.Text <> "" Then
            LblError.Text = ""
            GVprocesos.Visible = True
            GVprocesos.DataSource = consultarRadicados(txtBuscar1.Text, TxtBuscar2.Text, LstBuscar.SelectedValue)
            GVprocesos.DataBind()

        Else
            LblError.Text = "Debe ingresar el dato a buscar"
        End If



    End Sub
    Private Function consultarRadicados(parametro1 As String, parametro2 As String, criterio As String) As DataTable
        Dim conexion As Object
        Dim rs1 As Object
        Dim sql As String
        Dim i As Integer
        Dim radicacion As String
        Dim tipodocumento As String
        Dim suscriptor As String
        Dim fechapresentacion As String
        Dim fecharadicacion As String
        Dim encargado As String
        Dim estado As String
        radicacion = ""
        tipodocumento = ""
        suscriptor = ""
        fechapresentacion = ""
        fecharadicacion = ""
        encargado = ""
        estado = ""

        Dim dt As DataTable = New DataTable
        dt.Columns.Add(New DataColumn("Reg"))
        dt.Columns.Add(New DataColumn("Radicacion"))
        dt.Columns.Add(New DataColumn("TipoDocumento"))
        dt.Columns.Add(New DataColumn("Suscriptor"))
        dt.Columns.Add(New DataColumn("FechaPresentacion"))
        dt.Columns.Add(New DataColumn("FechaRadicacion"))
        dt.Columns.Add(New DataColumn("Encargado"))
        dt.Columns.Add(New DataColumn("Estado"))

        sql = "select r.idradicacion as NumeroRadicacion,  r.tIPORADICADO as TipoComunicacion,  Primernombre, SegundoNombre, PrimerApellido, SegundoApellido, r.FechaPresentacion as FechaPresentacion, R.FechaRadicacion as FechaRadicacion, NombreParametro aS EstadoRadicacion, rt.iduser as UsuarioResponsable  from radicados r LEFT JOIN RadicadosAsociados ON NumeroRadicadoPadre=IdRadicacion LEFT JOIN Parametros P ON P.IDParametro=EstadoRadicacion "
        sql = sql & "  Left join RadicadosPersonas rp on rp.numeroradicado=r.NumeroRadicado left join personas pe on pe.IdPersona=rp.IdPersona and rp.PersonaActiva=1 left join radicadostrazabilidad rt on rt.numeroradicado=r.NumeroRadicado and rt.estadotraza=1 "
        'Apellidos del suscriptor

        If criterio = "AP" And Len(txtBuscar1.Text) > 2 Then
            sql = sql & "  where  PrimerApellido like '%" & txtBuscar1.Text & "%'"
            If Trim(Txtbuscar2.Text) <> "" Then
                sql = sql & " and segundoapellido like '%" & Txtbuscar2.Text & "%'"
            End If
        End If
        'Nombre de una Entidad

        If criterio = "NO" And Len(txtBuscar1.Text) >= 6 Then
            sql = sql & " where  primernombre like '%" & txtBuscar1.Text & "%'"
        End If

        'Documento de identificación

        If criterio = "DI" And Len(txtBuscar1.Text) >= 6 Then
            sql = sql & " where  Pe.IDDOCUMENTO like '%" & txtBuscar1.Text & "%'"
        End If
        'Número de radicacion

        If criterio = "NR" And Len(txtBuscar1.Text) >= 6 Then
            sql = sql & " where  r.idradicacion LIKE '%" & txtBuscar1.Text & "%'"
        End If

        'TIPO DE DOCUMENTO
        If criterio = "TD" Then
            sql = sql & " where  R.TIPORADICADO = '" & LstTipoComunicacion.SelectedValue & "'"
        End If

        'NUMERO DE ORIGEN
        If criterio = "RD" Then
            sql = sql & " where R.OBSERVACION LIKE '%" & txtBuscar1.Text & "%' OR R.NUMEROORIGEN LIKE '%" & txtBuscar1.Text & "'"
        End If


        sql = sql & "  order by r.IdRadicacion "



        conexion = Server.CreateObject("ADODB.Connection")
        conexion.Open(Application("CadVJep"))
        rs1 = conexion.Execute(sql)
        i = 0

        Do Until rs1.eof()
            i = i + 1

            radicacion = ""
            tipodocumento = ""
            suscriptor = ""
            fechapresentacion = ""
            fecharadicacion = ""
            encargado = ""
            estado = ""
            If Not rs1("NumeroRadicacion").value Is DBNull.Value Then
                radicacion = rs1("NumeroRadicacion").value
            End If
            If Not rs1("TipoComunicacion").value Is DBNull.Value Then
                tipodocumento = rs1("TipoComunicacion").value
            End If
            If Not rs1("Primernombre").value Is DBNull.Value Then
                suscriptor = rs1("Primernombre").value
            End If
            If Not rs1("Segundonombre").value Is DBNull.Value Then
                If rs1("Segundonombre").value <> "" Then
                    suscriptor = suscriptor & " " & Trim(rs1("Segundonombre").value)
                End If
            End If

            If Not rs1("PrimerApellido").value Is DBNull.Value Then
                If rs1("PrimerApellido").value <> "" Then
                    suscriptor = suscriptor & " " & Trim(rs1("PrimerApellido").value)
                End If
            End If
            If Not rs1("SegundoApellido").value Is DBNull.Value Then
                If rs1("SegundoApellido").value <> "" Then
                    suscriptor = suscriptor & " " & Trim(rs1("SegundoApellido").value)
                End If
            End If
            If Not rs1("FechaRadicacion").value Is DBNull.Value Then
                fecharadicacion = rs1("FechaRadicacion").value

            End If
            If Not rs1("FechaPresentacion").value Is DBNull.Value Then
                fechapresentacion = rs1("FechaPresentacion").value

            End If
            If Not rs1("EstadoRadicacion").value Is DBNull.Value Then

                estado = rs1("EstadoRadicacion").value
            End If
            If Not rs1("UsuarioResponsable").value Is DBNull.Value Then
                encargado = rs1("UsuarioResponsable").value

            End If






            dt.Rows.Add(CreateRowRadicados(i, radicacion, tipodocumento, suscriptor, fechapresentacion, fecharadicacion, encargado, estado, dt))


                rs1.movenext()
        Loop
        rs1.close()
        conexion.close()
        conexion = Nothing
        rs1 = Nothing
        If i = 0 Then
            LblError.Text = "No se encontro ningun resultado"
        Else
            LblError.Text = i & "Registros encontrados"
        End If
        Return dt


    End Function


    Function CreateRowRadicados(registro As String, ByVal radicado As String, ByVal tipodocumento As String, ByVal suscriptor As String, ByVal fechapresentacion As String, ByVal fecharadicacion As String, ByVal encargado As String, estado As String, ByVal dt As DataTable) As DataRow

        ' Create a DataRow using the DataTable defined in the 
        ' CreateDataSource method.
        Dim dr As DataRow = dt.NewRow()
        dr(0) = registro
        dr(1) = radicado
        dr(2) = tipodocumento
        dr(3) = suscriptor
        dr(4) = fechapresentacion
        dr(5) = fecharadicacion
        dr(6) = encargado
        dr(7) = estado
        Return dr
    End Function

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        txtBuscar1.Text = ""
        TxtBuscar2.Text = ""
        GVprocesos.Visible = False
        LblError.Text = ""


    End Sub




End Class