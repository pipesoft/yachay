﻿<%@ Page Title="" Language="vb" aspcompat="true" AutoEventWireup="false" EnableEventValidation="false" MasterPageFile="~/Site.Master" CodeBehind="WConsultaRadicados.aspx.vb" Inherits="VPAZJEP.WConsultaRadicados" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
     <div class="row">
        <div class="col-md-1O">
           &nbsp;<h4><span class="colored-text">Consulta de Documentos Radicados</span> </h4>

               
              
             <h6><%: Now() %> </h6>
              
     <div class="row">
        <div class="col-md-0">
            
           <p>Seleccione el criterio por el cual desea ubicar la radicación: </p>     
            
         </div>
         
             
                    </div>
              
    
           
        <div class="col-md-4 text-left">
            <h4>Datos de Busqueda:
    
                    </h4>
                           
                <asp:ListBox ID="LstBuscar"  CssClass="form-control col-md-4 " runat="server" Rows="1"  Width="300px"
                                  AutoPostBack="True"   >
                                  <asp:ListItem Value="AP">Apellidos del usuario</asp:ListItem>
                                  <asp:ListItem Value="DI">Documento de identificación</asp:ListItem>
                                  <asp:ListItem Value="FA">Persona que atiende</asp:ListItem>
                                  <asp:ListItem Value="UJ">Fechas</asp:ListItem>
                                  </asp:ListBox>
        </div>
          <div class="form-group">
            <asp:Label ID="lblcriterio" runat="server"  CssClass="col-md-5 control-label">Apellidos del Suscriptor</asp:Label>
            <div class="col-md-5">
                <asp:TextBox runat="server" ID="txtBuscar1" CssClass="form-control" pattern="[a-zA-ZñÑ0-9 ,.-]{2,50}"  />
                <asp:TextBox runat="server" ID="TxtBuscar2" CssClass="form-control" pattern="[a-zA-ZñÑ0-9 ,.-]{2,50}" />
                   <asp:ListBox ID="LstTipoComunicacion"  CssClass="form-control  " runat="server" Rows="1"  Width="200px"
                                  AutoPostBack="False" visible="false"  >
                                  <asp:ListItem Value="CO">Comunicación general</asp:ListItem>
                                  <asp:ListItem Value="DP">Derecho de petición</asp:ListItem>
                                  <asp:ListItem Value="DPAU">Derecho de petición Amnistia IURE</asp:ListItem>
                                  <asp:ListItem Value="DPZV">Derecho de petición Zona Veredal</asp:ListItem>
                                  <asp:ListItem Value="DPLC">Derecho de petición Libertad Condicional</asp:ListItem>
                                  <asp:ListItem Value="TU">Tutela</asp:ListItem>
                                  <asp:ListItem Value="SO">Solicitud de información de otras autoridades u organizaciones internacionales</asp:ListItem>
                                  <asp:ListItem Value="RP">Respuesta</asp:ListItem>
                                   <asp:ListItem Value="RQ">Requerimiento</asp:ListItem>
                                  </asp:ListBox>
                </div>
             
              </div>  
                 <div class="form-group">
           
                        <asp:Label id="LblError" runat="server" CssClass="col-md-10 control-label colored-text" text=""/>
                            
                       </div>
                 <br />
             
                  <p>
                  <p />


              <div class="form-group col-md-12">
       
                <asp:Button ID="cmdBuscar" runat="server" Text="Buscar" class="btn btn-primary btn-lg" Width="207px" Height="47px" />
                  
        </div>
           
        
           <div class="form-group">
     
                  <br />
            <asp:GridView ID="GVprocesos" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" Caption="Radicados que Cumplen con el Criterio de Busqueda" CellPadding="3" Height="132px" Width="1015px">
                                <Columns>
                    <asp:BoundField DataField="Reg" HeaderText="Reg">
                    <ItemStyle Width="10px" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderStyle-Width="15%" HeaderText="Radicacion">
                        <ItemTemplate>
                            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# String.Format("WTramiteRadicado.aspx?guid={0}", Eval("radicacion")) %>' Text='<%# Eval("radicacion")%>'></asp:HyperLink>

                        </ItemTemplate>
                        <HeaderStyle Width="15%" />
                        <ItemStyle HorizontalAlign="Center" Width="150px" />
                    </asp:TemplateField>
                                    <asp:BoundField DataField="TipoDocumento" HeaderText="TipoDocumento" />
                    <asp:BoundField DataField="Suscriptor" HeaderText="Suscriptor(es)">
                    <ItemStyle HorizontalAlign="Left" Width="400px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="FechaPresentacion" HeaderText="FechaPresentacion">
                    </asp:BoundField>
                    <asp:BoundField DataField="FechaRadicacion" HeaderText="Fecharadicacion">
                    <ItemStyle Width="30px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Encargado" HeaderText="Encargado">
                    <ItemStyle Width="200px" />
                    <ItemStyle HorizontalAlign="Left" Width="150px" />
                    </asp:BoundField>
                                    <asp:BoundField DataField="Estado" HeaderText="Estado" />
                </Columns>
                <FooterStyle BackColor="White" ForeColor="#000066" />
                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                <RowStyle ForeColor="#000066" />
                <SelectedRowStyle BackColor="#669999" BorderWidth="5px" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                <SortedDescendingHeaderStyle BackColor="#00547E" />
            </asp:GridView>
            <br />
           
           </div>
           

                     <div class="form-group col-md-12">
       
                <asp:Button ID="Button1" runat="server" Text="Limpiar" class="btn btn-primary btn-lg" Width="207px" Height="47px" />
                  
        </div>

     
                  <br />
                  <br />
            <br />
                  <br />



           
        
           
     
                </div>
               


        
               
        

    </div>
    

</asp:Content>
