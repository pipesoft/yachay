﻿'Imports System
'Imports System.Collections
'Imports itextsharp.text
'Imports itextsharp.text.pdf

'Public Class VerticalPositionMark
'    Implements IDrawInterface, IElement

'            /** Another implementation of the DrawInterface; its draw method will overrule LineSeparator.Draw(). */
'        Protected IDrawInterface drawInterface = null


'        /** The offset for the line. */
'        Protected float offset = 0

'        /**
'        * Creates a vertical position mark that won't draw anything unless
'        * you define a DrawInterface.
'        */
'        Public VerticalPositionMark()


'        /**
'        * Creates a vertical position mark that won't draw anything unless
'        * you define a DrawInterface.
'        * @param   drawInterface   the drawInterface for this vertical position mark.
'        * @param   offset          the offset for this vertical position mark.
'        */
'        Public VerticalPositionMark(IDrawInterface drawInterface, float offset) 
'            this.drawInterface = drawInterface
'            this.offset = offset


'        /**
'        * @see com.lowagie.text.pdf.draw.DrawInterface#draw(com.lowagie.text.pdf.PdfContentByte, float, float, float, float, float)
'        */
'        Public virtual void Draw(PdfContentByte canvas, float llx, float lly, float urx, float ury, float y) {
'            If (drawInterface!= null) {
'                drawInterface.Draw(canvas, llx, lly, urx, ury, y + offset);
'            }
'        }

'        /**
'        * @see com.lowagie.text.Element#process(com.lowagie.text.ElementListener)
'        */
'        Public bool Process(IElementListener listener) {
'            Try {
'                Return listener.Add(this);
'            } catch (DocumentException) {
'                Return False;
'            }
'        }


'        /**
'        * @see com.lowagie.text.Element#type()
'        */
'        Public int Type {
'            Get {
'                Return Element.YMARK;
'            }
'        }


'        /**
'        * @see com.lowagie.text.Element#isContent()
'        */
'       Public Function IsContent() As Boolean
'        Return True
'    End Function


'    Public Function IsNestable() As Boolean
'        Return False
'    End Function

'    Public Function Chunks() As ArrayList
'        ArrayList List = New ArrayList()
'                List.Add(New Chunk(this, True))
'        Return List

'    End Function

'    Public virtual IDrawInterface DrawInterface {

'        Set {
'                drawInterface = value;
'            }
'            Get {
'                Return drawInterface;
'            }
'        }

'        Public virtual float Offset {
'            Set {
'                offset = value;
'            }
'            Get {
'                Return offset;
'            }
'        }
'    }
'}


'End Class
