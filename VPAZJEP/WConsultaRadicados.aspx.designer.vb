﻿'------------------------------------------------------------------------------
' <generado automáticamente>
'     Este código fue generado por una herramienta.
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código. 
' </generado automáticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace VPAZJEP
    
    Partial Public Class WConsultaRadicados
        
        '''<summary>
        '''Control LstBuscar.
        '''</summary>
        '''<remarks>
        '''Campo generado automáticamente.
        '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        '''</remarks>
        Protected WithEvents LstBuscar As Global.System.Web.UI.WebControls.ListBox
        
        '''<summary>
        '''Control lblcriterio.
        '''</summary>
        '''<remarks>
        '''Campo generado automáticamente.
        '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        '''</remarks>
        Protected WithEvents lblcriterio As Global.System.Web.UI.WebControls.Label
        
        '''<summary>
        '''Control txtBuscar1.
        '''</summary>
        '''<remarks>
        '''Campo generado automáticamente.
        '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        '''</remarks>
        Protected WithEvents txtBuscar1 As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Control TxtBuscar2.
        '''</summary>
        '''<remarks>
        '''Campo generado automáticamente.
        '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        '''</remarks>
        Protected WithEvents TxtBuscar2 As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Control LstTipoComunicacion.
        '''</summary>
        '''<remarks>
        '''Campo generado automáticamente.
        '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        '''</remarks>
        Protected WithEvents LstTipoComunicacion As Global.System.Web.UI.WebControls.ListBox
        
        '''<summary>
        '''Control LblError.
        '''</summary>
        '''<remarks>
        '''Campo generado automáticamente.
        '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        '''</remarks>
        Protected WithEvents LblError As Global.System.Web.UI.WebControls.Label
        
        '''<summary>
        '''Control cmdBuscar.
        '''</summary>
        '''<remarks>
        '''Campo generado automáticamente.
        '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        '''</remarks>
        Protected WithEvents cmdBuscar As Global.System.Web.UI.WebControls.Button
        
        '''<summary>
        '''Control GVprocesos.
        '''</summary>
        '''<remarks>
        '''Campo generado automáticamente.
        '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        '''</remarks>
        Protected WithEvents GVprocesos As Global.System.Web.UI.WebControls.GridView
        
        '''<summary>
        '''Control Button1.
        '''</summary>
        '''<remarks>
        '''Campo generado automáticamente.
        '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        '''</remarks>
        Protected WithEvents Button1 As Global.System.Web.UI.WebControls.Button
    End Class
End Namespace
