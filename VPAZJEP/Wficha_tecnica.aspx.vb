﻿Imports Microsoft.AspNet.Identity
Imports System.IO
Imports itextsharp
Imports itextsharp.text
Imports itextsharp.text.pdf
Imports System.Web.UI.WebControls.ListItem

Public Class Wficha_tecnica

    Inherits System.Web.UI.Page



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        MaintainScrollPositionOnPostBack = True
        LblRol.Text = ConsultarRol()

        If Not Page.IsPostBack Then
            LblFuente.Text = Request.QueryString("guid")

            If Context.User.Identity.GetUserName() <> "" Then
                PInicio.Visible = True
                PMensajes.Visible = False
                GVDatos2.DataSource = TConsultarFuentes(LblFuente.Text)
                GVDatos2.DataBind()
                GvComentarios.DataSource = TConsutarComentariosCitas(LblFuente.Text)
                GvComentarios.DataBind()
                If GvComentarios.Rows.Count() > 0 Then
                    GvComentarios.Visible = True
                End If

                GvDiccionario.DataSource = TConsultarDiccionario(LblFuente.Text)
                GvDiccionario.DataBind()
                If GvDiccionario.Rows.Count() > 0 Then
                    GvDiccionario.Visible = True
                End If


                MultiView1.SetActiveView(VFichaTecnica)
                If LblRol.Text = Application("FuentesYachay") Or LblRol.Text = Application("EditorBasico") Or LblRol.Text = Application("EditorJuridico") Then
                    LbtEditar.Visible = True
                Else
                    LbtEditar.Visible = False
                End If
                If LblRol.Text = Application("Consulta") Then
                    LbtCitar.Visible = False
                End If

                If ValidarPermisosEscritura(LblFuente.Text) Then
                    LbtEditar.Visible = True
                    LbtConsultar.Visible = True
                Else
                    LbtEditar.Visible = False
                    LbtConsultar.Visible = True
                End If


            Else
                PMensajes.Visible = True
                RegisterHyperLink.Visible = True
                IngreseHyperLink.Visible = True
                PInicio.Visible = False
                If Context.User.Identity.GetUserName() = "" Then
                    LblMensajes.Text = "Debe iniciar sesión para acceder a la utilidad"
                Else
                    LblMensajes.Text = "No tiene acceso al sistema, favor solicitar activación de su usuario"
                End If
            End If
        End If

    End Sub

    Private Function ValidarPermisosEscritura(fuente As String) As Boolean
        Dim conexion As Object
        Dim rs2 As Object
        Dim sqlEstructura As String
        Dim tienePermisoEscritura As Boolean = False
        Dim usuario As String

        usuario = Context.User.Identity.GetUserName().ToString()

        conexion = Server.CreateObject("ADODB.Connection")
        conexion.Open(Application("CadVJep"))

        sqlEstructura = " Select id_fuente_estructurada from mFuentes_estructuradas where esta_activa = 1 and id_fuente_estructurada='" & fuente & "' and id_usuario='" & usuario & "'"
        rs2 = conexion.Execute(sqlEstructura)
        Do Until rs2.eof
            If Not rs2("id_fuente_estructurada").value Is DBNull.Value Then
                tienePermisoEscritura = True
            End If
            rs2.movenext()
        Loop
        sqlEstructura = " Select id_fuente_noestructurada from mFuentes_no_estructuradas where esta_activa = 1 and id_fuente_noestructurada='" & fuente & "' and id_usuario='" & usuario & "'"
        rs2 = conexion.Execute(sqlEstructura)
        Do Until rs2.eof
            If Not rs2("id_fuente_noestructurada").value Is DBNull.Value Then
                tienePermisoEscritura = True
            End If
            rs2.movenext()
        Loop
        rs2.close()
        rs2 = Nothing

        Return tienePermisoEscritura

    End Function
    Private Function ConsultarRol() As String
        Dim conexion As Object
        Dim rs2 As Object
        Dim sql2 As String
        Dim usuario As String
        Dim rol As String
        rol = ""
        conexion = Server.CreateObject("ADODB.Connection")
        conexion.Open(Application("CadVJep"))
        usuario = Context.User.Identity.GetUserName()

        If usuario IsNot Nothing Then

            sql2 = "Select NOMBREROL FROM mUsuariosSistema U, mRolSistema R WHERE R.IDROL=U.IDROL And U.EstadoUser=1 And U.IDUSER='" & usuario & "'"
            rs2 = conexion.Execute(sql2)

            Do Until rs2.eof
                If Not rs2(0).value Is DBNull.Value Then
                    rol = rs2("nombrerol").value

                End If

                rs2.movenext()
            Loop


        End If

        conexion.close()
        conexion = Nothing
        rs2 = Nothing
        Return rol
    End Function

    Function TConsultarDiccionario(fuente As String) As DataTable
        Dim conexiontmp As Object
        Dim rs1 As Object
        Dim sql As String
        Dim iReg As Integer
        Dim variable As String
        Dim descripcion As String
        Dim contenido As String
        variable = ""
        descripcion = ""
        contenido = ""
        iReg = 0
        Dim dt As DataTable = New DataTable()
        dt.Columns.Add(New DataColumn("Nombre", GetType(String)))
        dt.Columns.Add(New DataColumn("Descripcion", GetType(String)))
        conexiontmp = Server.CreateObject("ADODB.Connection")
        conexiontmp.Open(Application("CadVJep"))

        sql = "select md.nombre_variable as nombre_variable , md.descripcion_variable as descripcion_variable, af.contenido_segun_diccionario_entidad as contenido  from aFuentes_diccionario af,  mdiccionario_datos md "
        sql = sql & " where id_fuente_estructurada='" & fuente & "' and md.nombre_variable=af.nombre_variable"
        rs1 = conexiontmp.Execute(sql)

        Do While Not rs1.EOF()
            iReg = iReg + 1
            If Not rs1.fields("nombre_variable").value() Is DBNull.Value Then
                variable = rs1.fields("nombre_variable").value()
            End If

            If Not rs1.fields("descripcion_variable").value() Is DBNull.Value Then
                descripcion = rs1.fields("descripcion_variable").value()
            End If

            If Not rs1.fields("contenido").value() Is DBNull.Value Then
                contenido = rs1.fields("contenido").value()
            End If
            dt.Rows.Add(CreateRowDiccionarios(variable, contenido, dt))
            rs1.movenext()
        Loop

        rs1.close()
        conexiontmp.close()
        conexiontmp = Nothing
        rs1 = Nothing
        Return dt



    End Function
    Function TConsultarFuentes(fuente As String) As DataTable

        Dim conexiontmp As Object
        Dim rs1 As Object
        Dim sql As String
        Dim iReg As Integer
        Dim Sentidad As String
        Dim SnombreFuente As String
        Dim SnombreArchivo As String        'NUEVO
        Dim SautorFuente As String          'NUEVO        
        Dim StipoFuente As String           'NUEVO    
        Dim Sdesagreagacion As String       'NUEVO    
        Dim StieneSoporte As String         'NUEVO    
        Dim SresponsableGestion As String   'NUEVO    
        Dim SidiomaFuente As String         'NUEVO    
        Dim SradicadoOrfeo As String        'NUEVO    


        Dim SfechaCorte As String
        Dim SfechaRecepcion As String
        Dim SformasAcceso As String
        Dim SresponsableFuente As String
        Dim ScantidadRegistrosAutor As String
        Dim SnumeroVariables As String
        Dim Scontenido As String
        Dim SdescripcionFuente As String
        Dim StemasRelacionados As String
        Dim Stemporalidad As String
        Dim SvariablesRelevantes As String
        Dim SestaRevisada As String
        Dim SesPrimaria As String
        Dim SesEstructurada As String
        Dim SUniverso As String
        Dim SLimitaciones As String
        Dim SPorcentajeAplicabilidad As String
        Dim SaccesiblePara As String
        Dim sensibles As String

        iReg = 0
        Dim dt As DataTable = New DataTable()
        Dim dtReporte As DataTable = New DataTable()

        dt.Columns.Add(New DataColumn("Tema", GetType(String)))
        dt.Columns.Add(New DataColumn("Detalle", GetType(String)))

        dtReporte.Columns.Add(New DataColumn("NombreFuente", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("EntidadRemite", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("DescripcionFuente", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("EntidadGeneradora", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("TipoFormatoFuente", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("NivelDesagregacion", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("ResponsableGestion", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("IdiomaFuente", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("RadicadoOrfeo", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("FormaAcceso", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("ResponsableCustodio", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("UniversoEstudio", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("EsEstructurada", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("Formato", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("Temporalidad", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("EstaRevisada", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("FechaActualizacion", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("FechaRecepcionJEP", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("TieneDatosSensibles", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("TipoInformacion", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("FundamentoJuridico", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("ObjetivoJuridico", GetType(String)))
        dtReporte.Columns.Add(New DataColumn("FundamentoLegal", GetType(String)))

        sql = " SELECT id_fuente_estructurada as id_fuentes, nombre_fuente, nombre_subfuente as nombre_subfuente_tipo_f, nombre_archivo, autor_fuente, vtf.Descripcion as tipo_fuente, vdt.Descripcion as desagreagacion, vds.Descripcion as tieneSoporte, vrg.Descripcion as responsableGestion, vi.descripcion_idioma as idiomaFuente,  fe.sobre_forma_ingreso as radicadoOrfeo, formato, numero_de_variables as numero_variables, cantidad_registros, fne.descripcion_fuente as descripcion_registros_autor, tamaño_bytes as tamaño_bytes_ubicacion, universo_estudio, limitaciones, fecha_recepcion, fecha_corte, fecha_carga, anexos_documentales, sobre_estructura, temporalidad_ano_inicia, temporalidad_ano_termina, esta_revisada, porcentaje_aplicabilidad, ec.entidad_nombre as entidad_nombre, fne.descripcion_fuente as descripcion_fuente, vc.nombre_custodio as responsable_custodio, accesible_para, es_estructurada, fi.descripcion as forma_ingreso, fa.descripcion as forma_acceso "
        sql = sql & " , ft.descripcion as forma_tradiccion, fs.descripcion as forma_soporte, ti.Descripcion as tipo_informacion, es_fuente_primaria, tiene_datos_sensibles, fundamento_juridico,objetivo_juridico , fundamento_const_legal, ampliacion_justificacion_ds, tiene_victimas, tiene_responsables,fne.id_fuente as id_fuente "
        sql = sql & " FROM mFuentes fne left join mentidades_convenios ec on ec.id_entidad = fne.id_entidad  "
        sql = sql & " left join vFormas_ingreso fi on fi.id_forma_ingreso=fne.id_forma_ingreso "
        sql = sql & " left join vFormas_acceso fa on fa.id_forma_acceso=fne.id_forma_acceso "
        sql = sql & " left join vFormas_tradiccion ft on ft.id_tradiccion=fne.id_tradiccion "
        sql = sql & " left join vTipos_informacion ti on ti.Id_tipo_informacion=fne.id_tipo_informacion "
        sql = sql & " left join mFuentes_estructuradas fe on fe.id_fuente=fne.Id_fuente "
        sql = sql & " left join vformas_soporte fs on fs.id_tipo_soporte=fe.id_tipo_soporte "
        sql = sql & " Left join vfundamentos_juridicos_ti vfj on vfj.id_fundamento_juridico=fne.id_fundamento_juridico_ti "
        sql = sql & " left join vObjetivos_legitimos_ti volt on volt.id_objetivo_juridico=fne.id_objetivo_legitimo_ti "
        sql = sql & " left join vFundamentos_const_legal_ti vfc on vfc.id_fundamento_const_legal=fne.id_fundamento_constitucional_ti "
        sql = sql & " left join vcustodios vc on vc.id_custodio=fe.id_custodio "
        sql = sql & " left Join vFuentes_tipoFormato vtf on vtf.Id_tipo_formato = fe.id_tipo_fuente "
        sql = sql & " left Join vFuentes_desagregacionTerritorial vdt on vdt.Id_desagregacion_territorial = fe.id_nivel_desagrega_territorial "
        sql = sql & " left Join vFuentes_opcionDocumentosSoporte vds on vds.Id_opcion_documento = fe.id_documento_soporte "
        sql = sql & " left Join vFuentes_responsableGestion vrg on vrg.Id_reponsable_gestion = fe.id_responsable_gestion "
        sql = sql & " left Join vIdiomas vi on vi.id_idioma = fne.id_idioma "
        sql = sql & " where  fe.esta_activa=1  And id_fuente_estructurada ='" & fuente & "'"
        sql = sql & " union "
        sql = sql & " SELECT id_fuente_noestructurada, nombre_fuente, vft.descripcion as tipo_fuente, nombre_archivo, autor_fuente, vtf.Descripcion as tipo_fuente, vdt.Descripcion as desagreagacion, vds.Descripcion as tieneSoporte, vrg.Descripcion as responsableGestion, vi.descripcion_idioma as idiomaFuente,  fe.sobre_forma_ingreso as radicadoOrfeo, formato, '', '', autor_fuente,vu.ubicacion, '','', fecha_recepcion, '', fecha_carga, '', '',temporalidad_ano_inicia,temporalidad_ano_termina, esta_revisada, porcentaje_aplicabilidad,  ec.entidad_nombre as entidad_nombre, fne.descripcion_fuente as descripcion_fuente, vc.nombre_custodio as responsable_custodio, accesible_para, es_estructurada, fi.descripcion as forma_ingreso, fa.descripcion as forma_acceso "
        sql = sql & " , ft.descripcion as forma_tradiccion, fs.descripcion as forma_soporte, ti.Descripcion as tipo_informacion, es_fuente_primaria, tiene_datos_sensibles, fundamento_juridico,objetivo_juridico , fundamento_const_legal,ampliacion_justificacion_ds,tiene_victimas, tiene_responsables, fne.id_fuente as id_fuente "
        sql = sql & " FROM mFuentes fne left join mentidades_convenios ec on ec.id_entidad = fne.id_entidad  "
        sql = sql & " left join vFormas_ingreso fi on fi.id_forma_ingreso=fne.id_forma_ingreso  "
        sql = sql & " left join vFormas_acceso fa on fa.id_forma_acceso=fne.id_forma_acceso  "
        sql = sql & " left join vFormas_tradiccion ft on ft.id_tradiccion=fne.id_tradiccion  "
        sql = sql & " left join vTipos_informacion ti on ti.Id_tipo_informacion=fne.id_tipo_informacion "
        sql = sql & " left join mFuentes_no_estructuradas fe on fe.id_fuente=fne.Id_fuente "
        sql = sql & " left join vUbicaciones vu on vu.id_ubicacion=fe.id_ubicacion "
        sql = sql & " left join vformas_soporte fs on fs.id_tipo_soporte=fe.id_tipo_soporte "
        sql = sql & " left join vfuentes_tipo vft on vft.id_tipo_fuente =fe.id_tipo_fuente "
        sql = sql & " left join vfundamentos_juridicos_ti vfj on vfj.id_fundamento_juridico=fne.id_fundamento_juridico_ti "
        sql = sql & " left join vObjetivos_legitimos_ti volt on volt.id_objetivo_juridico=fne.id_objetivo_legitimo_ti "
        sql = sql & " left join vFundamentos_const_legal_ti vfc on vfc.id_fundamento_const_legal=fne.id_fundamento_constitucional_ti "
        sql = sql & " left join vcustodios vc on vc.id_custodio=fe.id_custodio "
        sql = sql & " left Join vFuentes_tipoFormato vtf on vtf.Id_tipo_formato = fe.id_tipo_fuente "
        sql = sql & " left Join vFuentes_desagregacionTerritorial vdt on vdt.Id_desagregacion_territorial = fe.id_nivel_desagrega_territorial "
        sql = sql & " left Join vFuentes_opcionDocumentosSoporte vds on vds.Id_opcion_documento = fe.id_documento_soporte "
        sql = sql & " left Join vFuentes_responsableGestion vrg on vrg.Id_reponsable_gestion = fe.id_responsable_gestion "
        sql = sql & " left Join vIdiomas vi on vi.id_idioma = fne.id_idioma "
        sql = sql & " where  fe.esta_activa=1 And id_fuente_noestructurada ='" & fuente & "'"

        conexiontmp = Server.CreateObject("ADODB.Connection")
        conexiontmp.Open(Application("CadVJep"))

        rs1 = conexiontmp.Execute(sql)

        Do While Not rs1.EOF()
            iReg = iReg + 1
            Sentidad = ""
            SnombreFuente = ""
            SfechaCorte = ""
            SfechaRecepcion = ""
            SformasAcceso = ""
            SresponsableFuente = ""
            ScantidadRegistrosAutor = ""
            SnumeroVariables = ""
            Scontenido = ""
            SdescripcionFuente = ""
            StemasRelacionados = ""
            Stemporalidad = ""
            SvariablesRelevantes = ""
            SestaRevisada = ""
            SesPrimaria = ""
            SesEstructurada = ""
            SUniverso = ""
            SLimitaciones = ""
            SPorcentajeAplicabilidad = ""
            SaccesiblePara = ""


            If Not rs1.fields("id_fuente").value() Is DBNull.Value Then
                LblFuentePadre.Text = rs1.fields("id_fuente").value()
            End If

            If Not rs1.fields("accesible_para").value() Is DBNull.Value Then
                SaccesiblePara = rs1.fields("accesible_para").value()
            End If

            If Not rs1.fields("porcentaje_aplicabilidad").value() Is DBNull.Value Then
                SPorcentajeAplicabilidad = rs1.fields("porcentaje_aplicabilidad").value()
            End If

            If Not rs1.fields("limitaciones").value() Is DBNull.Value Then
                SLimitaciones = rs1.fields("limitaciones").value()
            End If

            If Not rs1.fields("universo_estudio").value() Is DBNull.Value Then
                SUniverso = rs1.fields("universo_estudio").value()
            End If

            If Not rs1.fields("es_estructurada").value() Is DBNull.Value Then
                If rs1.fields("es_estructurada").value() = True Then
                    SesEstructurada = "Si"
                End If
            End If

            If Not rs1.fields("es_fuente_primaria").value() Is DBNull.Value Then
                If rs1.fields("es_fuente_primaria").value() = True Then
                    SesPrimaria = "Si"
                End If
            End If


            If Not rs1.fields("esta_revisada").value() Is DBNull.Value Then
                If rs1.fields("esta_revisada").value() = True Then
                    SestaRevisada = "Si"
                End If
            End If

            If Not rs1.fields("tiene_responsables").value() Is DBNull.Value Then
                If rs1.fields("tiene_responsables").value() = True Then
                    SvariablesRelevantes = "responsables o autores"
                End If

            End If

            If Not rs1.fields("tiene_victimas").value() Is DBNull.Value Then
                If rs1.fields("tiene_victimas").value() = True Then
                    If SvariablesRelevantes <> "," Then

                    End If
                    SvariablesRelevantes = " víctimas"
                End If
            End If


            If Not rs1.fields("entidad_nombre").value() Is DBNull.Value Then
                Sentidad = rs1.fields("entidad_nombre").value() & " - "
            End If

            If Not rs1.fields("nombre_fuente").value() Is DBNull.Value Then
                SnombreFuente = rs1.fields("nombre_fuente").value()
                LblFuenteNombre.Text = rs1.fields("nombre_fuente").value()

                If Not rs1.fields("nombre_subfuente_tipo_f").value() Is DBNull.Value Then
                    SnombreFuente = SnombreFuente & "-" & rs1.fields("nombre_subfuente_tipo_f").value()
                End If
            End If


            '**************************************************************
            If Not rs1.fields("nombre_archivo").value() Is DBNull.Value Then
                SnombreArchivo = rs1.fields("nombre_archivo").value()
            End If

            If Not rs1.fields("autor_fuente").value() Is DBNull.Value Then
                SautorFuente = rs1.fields("autor_fuente").value()
            End If

            If Not rs1.fields("tipo_fuente").value() Is DBNull.Value Then
                StipoFuente = rs1.fields("tipo_fuente").value()
            End If

            If Not rs1.fields("desagreagacion").value() Is DBNull.Value Then
                Sdesagreagacion = rs1.fields("desagreagacion").value()
            End If

            If Not rs1.fields("tieneSoporte").value() Is DBNull.Value Then
                StieneSoporte = rs1.fields("tieneSoporte").value()
            End If

            If Not rs1.fields("responsableGestion").value() Is DBNull.Value Then
                SresponsableGestion = rs1.fields("responsableGestion").value()
            End If

            If Not rs1.fields("idiomaFuente").value() Is DBNull.Value Then
                SidiomaFuente = rs1.fields("idiomaFuente").value()
            End If

            If Not rs1.fields("radicadoOrfeo").value() Is DBNull.Value Then
                SradicadoOrfeo = rs1.fields("radicadoOrfeo").value()
            End If

            '**************************************************************



            If Not rs1.fields("forma_acceso").value() Is DBNull.Value Then
                SformasAcceso = rs1.fields("forma_acceso").value()
            End If

            If Not rs1.fields("responsable_custodio").value() Is DBNull.Value Then
                SresponsableFuente = rs1.fields("responsable_custodio").value()
            End If

            If Not rs1.fields("cantidad_registros").value() Is DBNull.Value Then
                ScantidadRegistrosAutor = rs1.fields("cantidad_registros").value()
            End If
            If Not rs1.fields("numero_variables").value() Is DBNull.Value Then
                SnumeroVariables = rs1.fields("numero_variables").value()
            End If

            If Not rs1.fields("formato").value() Is DBNull.Value Then
                Scontenido = " Formato:" & rs1.fields("formato").value()
            End If
            If Not rs1.fields("fecha_corte").value() Is DBNull.Value Then
                SfechaCorte = rs1.fields("fecha_corte").value()
            End If

            If Not rs1.fields("fecha_recepcion").value() Is DBNull.Value Then
                If rs1.fields("fecha_recepcion").value() <> "01/01/1900" Then
                    SfechaRecepcion = rs1.fields("fecha_recepcion").value()
                End If

            End If


            '<asp:LinkButton id="LinkButton2" CommandName="Show" text='<%# Eval("idFuente")%>'  runat="server"/>

            If Not rs1.fields("descripcion_fuente").value() Is DBNull.Value Then
                SdescripcionFuente = rs1.fields("descripcion_fuente").value()
            End If

            If Not rs1.fields("temporalidad_ano_inicia").value() Is DBNull.Value Then
                Stemporalidad = rs1.fields("temporalidad_ano_inicia").value()
                'If rs1.fields("temporalidad_ano_inicia").value() <> 0 Then
                '    Stemporalidad = rs1.fields("temporalidad_ano_inicia").value()
                'End If

            End If
            If Not rs1.fields("temporalidad_ano_termina").value() Is DBNull.Value Then
                Stemporalidad = Stemporalidad & " a " & rs1.fields("temporalidad_ano_termina").value()
                'If rs1.fields("temporalidad_ano_termina").value() <> 0 Then
                '    Stemporalidad = Stemporalidad & " a " & rs1.fields("temporalidad_ano_termina").value()
                'End If
            End If

            '----impresiones

            If SnombreFuente <> "" Then
                dt.Rows.Add(CreateRowFuentes("Nombre de la fuente", SnombreFuente, dt))
            End If
            If Sentidad <> "" Then
                dt.Rows.Add(CreateRowFuentes("Entidad que remite", Sentidad, dt))
            End If
            If SdescripcionFuente <> "" Then
                dt.Rows.Add(CreateRowFuentes("Descripción de la fuente", SdescripcionFuente, dt))
            End If

            '*************************************************************************************
            If SnombreArchivo <> "" Then
                dt.Rows.Add(CreateRowFuentes("Nombre del Archivo", SnombreArchivo, dt))
            End If

            If SautorFuente <> "" Then
                dt.Rows.Add(CreateRowFuentes("Entidad generadora", SautorFuente, dt))
            End If

            If StipoFuente <> "" Then
                dt.Rows.Add(CreateRowFuentes("Tipo de formato de la fuente", StipoFuente, dt))
            End If

            If Sdesagreagacion <> "" Then
                dt.Rows.Add(CreateRowFuentes("Nivel desagregaciòn territorial", Sdesagreagacion, dt))
            End If

            If StieneSoporte <> "" Then
                dt.Rows.Add(CreateRowFuentes("Tiene soporte", StieneSoporte, dt))
            End If

            If SresponsableGestion <> "" Then
                dt.Rows.Add(CreateRowFuentes("Responsable Gestión", SresponsableGestion, dt))
            End If

            If SidiomaFuente <> "" Then
                dt.Rows.Add(CreateRowFuentes("Idioma Fuente", SidiomaFuente, dt))
            End If

            If SradicadoOrfeo <> "" Then
                dt.Rows.Add(CreateRowFuentes("Radicado ORFEO", SradicadoOrfeo, dt))
            End If

            '*************************************************************************************


            If SaccesiblePara <> "" Then
                dt.Rows.Add(CreateRowFuentes("Accesible para", SaccesiblePara, dt))
            End If

            If SformasAcceso <> "" Then
                dt.Rows.Add(CreateRowFuentes("Forma de acceso", SformasAcceso, dt))
            End If

            If SresponsableFuente <> "" Then
                dt.Rows.Add(CreateRowFuentes("Responsable custodio", SresponsableFuente, dt))
            End If


            If SUniverso <> "" Then
                dt.Rows.Add(CreateRowFuentes("Universo de estudio", SUniverso, dt))
            End If

            If SesPrimaria <> "" Then
                dt.Rows.Add(CreateRowFuentes("Es primaria", SesPrimaria, dt))
            End If
            If SesEstructurada <> "" Then
                dt.Rows.Add(CreateRowFuentes("Es estructurada", SesEstructurada, dt))
            End If

            If SesEstructurada = "Si" Then
                If ScantidadRegistrosAutor <> "" Then
                    dt.Rows.Add(CreateRowFuentes("Número de registros", ScantidadRegistrosAutor, dt))
                End If
                If SvariablesRelevantes <> "" Then
                    dt.Rows.Add(CreateRowFuentes("Variables relevantes", SvariablesRelevantes, dt))
                End If
                If SnumeroVariables <> "" Then
                    dt.Rows.Add(CreateRowFuentes("Número de variables", SnumeroVariables, dt))
                End If
                If Scontenido <> "" Then
                    dt.Rows.Add(CreateRowFuentes("Variables", Scontenido, dt))
                End If
                If Not rs1.fields("descripcion_registros_autor").value() Is DBNull.Value Then
                    If rs1.fields("descripcion_registros_autor").value() <> "" Then
                        dt.Rows.Add(CreateRowFuentes("descripción de registros", rs1.fields("descripcion_registros_autor").value(), dt))
                    End If
                End If

            Else
                If Not rs1.fields("descripcion_registros_autor").value() Is DBNull.Value Then
                    If rs1.fields("descripcion_registros_autor").value() <> "" Then
                        dt.Rows.Add(CreateRowFuentes("Autor ", rs1.fields("descripcion_registros_autor").value(), dt))
                    End If
                End If

            End If

            If Scontenido <> "" Then
                dt.Rows.Add(CreateRowFuentes("Formato", Scontenido, dt))
            End If

            If Not rs1.fields("tamaño_bytes_ubicacion").value() Is DBNull.Value Then
                If rs1.fields("tamaño_bytes_ubicacion").value() <> "" And SesEstructurada = "No" Then
                    dt.Rows.Add(CreateRowFuentes("Tamaño bytes ", rs1.fields("tamaño_bytes_ubicacion").value(), dt))
                Else
                    dt.Rows.Add(CreateRowFuentes("Ubicación ", rs1.fields("tamaño_bytes_ubicacion").value(), dt))
                End If
            End If


            If Not rs1.fields("sobre_estructura").value() Is DBNull.Value Then
                If rs1.fields("sobre_estructura").value() <> "" Then
                    dt.Rows.Add(CreateRowFuentes("Sobre estructura ", rs1.fields("sobre_estructura").value(), dt))
                End If
            End If

            If StemasRelacionados <> "" Then
                dt.Rows.Add(CreateRowFuentes("Alcance temático", StemasRelacionados, dt))
            End If
            If Stemporalidad <> "" Then
                dt.Rows.Add(CreateRowFuentes("Temporalidad", Stemporalidad, dt))
            End If
            If SLimitaciones <> "" Then
                dt.Rows.Add(CreateRowFuentes("Limitaciones", SLimitaciones, dt))
            End If
            If SestaRevisada <> "" Then
                dt.Rows.Add(CreateRowFuentes("Esta revisada", SestaRevisada, dt))
            End If
            If SPorcentajeAplicabilidad <> "" Then
                dt.Rows.Add(CreateRowFuentes("Porcentaje de aplicabilidad", SPorcentajeAplicabilidad, dt))
            End If



            If Not rs1.fields("anexos_documentales").value() Is DBNull.Value Then
                If rs1.fields("anexos_documentales").value() <> "" Then
                    dt.Rows.Add(CreateRowFuentes("Anexos documentales  ", rs1.fields("anexos_documentales").value(), dt))
                End If
            End If

            If SfechaCorte <> "01/01/1900" Then
                dt.Rows.Add(CreateRowFuentes("Fecha de actualización", SfechaCorte, dt))
            End If
            If SfechaRecepcion <> "" Then
                dt.Rows.Add(CreateRowFuentes("Fecha de recepción en la JEP", SfechaRecepcion, dt))
            End If



            If Not rs1.fields("fecha_carga").value() Is DBNull.Value Then
                If rs1.fields("fecha_carga").value() <> "01/01/1900" Then
                    dt.Rows.Add(CreateRowFuentes("Fecha en que se disponibiliza ", rs1.fields("fecha_carga").value(), dt))
                End If
            End If

            If Not rs1.fields("forma_ingreso").value() Is DBNull.Value Then
                If rs1.fields("forma_ingreso").value() <> "" Then
                    dt.Rows.Add(CreateRowFuentes("Forma de ingreso ", rs1.fields("forma_ingreso").value(), dt))
                End If
            End If

            If Not rs1.fields("forma_tradiccion").value() Is DBNull.Value Then
                If rs1.fields("forma_tradiccion").value() <> "" Then
                    dt.Rows.Add(CreateRowFuentes("Forma de tradicción ", rs1.fields("forma_tradiccion").value(), dt))
                End If
            End If

            If Not rs1.fields("forma_soporte").value() Is DBNull.Value Then
                If rs1.fields("forma_soporte").value() <> "" Then
                    dt.Rows.Add(CreateRowFuentes("Forma de soporte ", rs1.fields("forma_soporte").value(), dt))
                End If
            End If

            If Not rs1.fields("tiene_datos_sensibles").value() Is DBNull.Value Then
                sensibles = "No"
                If rs1.fields("tiene_datos_sensibles").value() = True Then
                    sensibles = "Si"
                End If
                dt.Rows.Add(CreateRowFuentes("Tiene datos sensibles", sensibles, dt))
            End If

            If Not rs1.fields("tipo_informacion").value() Is DBNull.Value Then
                dt.Rows.Add(CreateRowFuentes("Tipo de información", rs1.fields("tipo_informacion").value(), dt))

            End If

            If Not rs1.fields("fundamento_juridico").value() Is DBNull.Value Then
                dt.Rows.Add(CreateRowFuentes("Fundamento jurídico clasificación tipo de información", rs1.fields("fundamento_juridico").value(), dt))

            End If


            If Not rs1.fields("objetivo_juridico").value() Is DBNull.Value Then
                dt.Rows.Add(CreateRowFuentes("Objetivo jurídico clasificación tipo de información ", rs1.fields("objetivo_juridico").value(), dt))

            End If

            If Not rs1.fields("fundamento_const_legal").value() Is DBNull.Value Then
                dt.Rows.Add(CreateRowFuentes("Fundamento constitucional y legal clasificación tipo de información", rs1.fields("fundamento_const_legal").value(), dt))

            End If

            If Not rs1.fields("ampliacion_justificacion_ds").value() Is DBNull.Value Then
                If rs1.fields("ampliacion_justificacion_ds").value() <> "" Then
                    dt.Rows.Add(CreateRowFuentes("Ampliación de Justificación", rs1.fields("ampliacion_justificacion_ds").value(), dt))
                End If
            End If

            rs1.movenext()

        Loop

        rs1.close()
        conexiontmp.close()
        conexiontmp = Nothing
        rs1 = Nothing

        Return dt

    End Function

    Function TConsutarComentariosCitas(fuente As String) As DataTable
        'consulta de las fuentes relacionadas:
        Dim conexiontmp As Object
        Dim rs2 As Object
        Dim sql6 As String

        Dim dt As DataTable = New DataTable()

        dt.Columns.Add(New DataColumn("Comentario", GetType(String)))
        dt.Columns.Add(New DataColumn("Dato", GetType(String)))
        conexiontmp = Server.CreateObject("ADODB.Connection")
        conexiontmp.Open(Application("CadVJep"))
        sql6 = "Select nombre_fuente,detalle_asociacion,'relacionada'  as tipo From aFuentes_relacionadas fr , mFuentes_estructuradas fe "
        sql6 = sql6 & " where  fr.estado=1 and ( fe.id_fuente=id_fuente1 or fe.id_fuente=id_fuente2) "
        sql6 = sql6 & " And ( id_fuente1='" & LblFuentePadre.Text & "' or id_fuente2='" & LblFuentePadre.Text & "') "
        sql6 = sql6 & " union "
        sql6 = sql6 & " Select nombre_fuente, detalle_asociacion,'relacionada'  "
        sql6 = sql6 & " From aFuentes_relacionadas fr, mFuentes_no_estructuradas fne "
        sql6 = sql6 & " Where fr.estado=1 and ( fne.id_fuente=id_fuente1 or fne.id_fuente=id_fuente2) "
        sql6 = sql6 & " And ( id_fuente1='" & LblFuentePadre.Text & "' or id_fuente2='" & LblFuentePadre.Text & "') "
        sql6 = sql6 & " union"
        sql6 = sql6 & " Select cast(puntaje As varchar(2)) , motivo ,'puntaje' from afuentes_puntaje afp, mFuentes_estructuradas mfe "
        sql6 = sql6 & " where afp.id_fuente_est_nest = mfe.id_fuente_estructurada And mfe.id_fuente_estructurada ='" & fuente & "' "
        sql6 = sql6 & " union "
        sql6 = sql6 & " Select cast(puntaje As varchar(2)), motivo,'puntaje' from afuentes_puntaje afp, mFuentes_no_estructuradas mfe "
        sql6 = sql6 & " where afp.id_fuente_est_nest = mfe.id_fuente_noestructurada And mfe.id_fuente_noestructurada ='" & fuente & "' "
        sql6 = sql6 & " union "
        sql6 = sql6 & " Select citada_en + ' ' + observaciones, cast(fecha_cita As varchar(10)), 'cita' from afuentes_cita afc, mFuentes_estructuradas mfe "
        sql6 = sql6 & " where afc.id_fuente_est_nest = mfe.id_fuente_estructurada And mfe.id_fuente_estructurada ='" & fuente & "' "
        sql6 = sql6 & " union "
        sql6 = sql6 & " Select citada_en + ' ' + observaciones, cast(fecha_cita As varchar(10)),'cita' from afuentes_cita afc, mFuentes_estructuradas mfe "
        sql6 = sql6 & " where afc.id_fuente_est_nest = mfe.id_fuente_estructurada And mfe.id_fuente_estructurada ='" & fuente & "' "
        sql6 = sql6 & " order by 3 "
        rs2 = conexiontmp.Execute(sql6)
        Do While Not rs2.EOF()
            If Not rs2.fields("nombre_fuente").value() Is DBNull.Value Then
                If rs2.fields("tipo").value() = "relacionada" Then
                    If LblFuenteNombre.Text <> rs2.fields("nombre_fuente").value() Then
                        dt.Rows.Add(CreateRowFuentes("Fuentes relacionadas", rs2.fields("nombre_fuente").value() & "-" & rs2.fields("detalle_asociacion").value(), dt))
                    End If
                End If
                End If

            If rs2.fields("tipo").value() = "puntaje" Then
                dt.Rows.Add(CreateRowFuentes("Puntaje", rs2.fields("nombre_fuente").value() & "-" & rs2.fields("detalle_asociacion").value(), dt))

            End If
            If rs2.fields("tipo").value() = "cita" Then
                'If rs1.fields("nombre_fuente").value() <> rs2.fields("nombre_fuente").value() Then
                dt.Rows.Add(CreateRowFuentes("Citada en:", rs2.fields("nombre_fuente").value() & "-" & rs2.fields("detalle_asociacion").value(), dt))
                'End If
            End If

            rs2.movenext()
        Loop
        rs2.close()
        rs2 = Nothing
        conexiontmp.close()
        conexiontmp = Nothing
        Return dt
    End Function

    Function CreateRowFuentes(tema As String, dato As String, dt As DataTable) As DataRow
        Dim dr As DataRow = dt.NewRow()
        dr(0) = tema
        dr(1) = dato
        Return dr
    End Function

    Function CreateRowDiccionarios(variable As String, contenido As String, dt As DataTable) As DataRow
        Dim dr As DataRow = dt.NewRow()
        dr(0) = variable
        dr(1) = contenido
        Return dr
    End Function



    Private Sub Exportar(Datagrid As GridView, datagrid2 As GridView)
        Dim sb As StringBuilder = New StringBuilder()
        Dim sw As StringWriter = New StringWriter(sb)
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        Dim pagina As Page = New Page()
        Dim form = New HtmlForm

        Datagrid.ViewStateMode = UI.ViewStateMode.Disabled
        datagrid2.ViewStateMode = UI.ViewStateMode.Disabled

        pagina.EnableEventValidation = False
        pagina.DesignerInitialize()

        pagina.Controls.Add(form)
        Try
            form.Controls.Add(Lblresultados)
        Catch
        End Try
        Try
            form.Controls.Add(LblFuente)

        Catch ex As Exception

        End Try
        Try
            form.Controls.Add(Datagrid)
        Catch ex As Exception

        End Try

        Try
            form.Controls.Add(datagrid2)
        Catch ex As Exception

        End Try

        Try
            pagina.RenderControl(htw)
        Catch ex As Exception

        End Try

        Try
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = “application/vnd.ms-excel”
            Response.AddHeader(“Content-Disposition”, “attachment;filename=FichaTecnicaFuente" & LblFuente.Text & ".xls”)
            Response.Charset = “UTF-8”
            Response.ContentEncoding = Encoding.Default
            Response.Write(sb.ToString())
            Response.End()

        Catch ex As Exception
            Dim menajeError As String
            menajeError = ex.Message
        End Try




    End Sub



    Protected Sub LbtExportar_Click(sender As Object, e As EventArgs) Handles LbtExportar.Click
        Exportar(GVDatos2, GvDiccionario)
    End Sub

    Protected Sub LbtCitar_Click(sender As Object, e As EventArgs) Handles LbtCitar.Click
        MultiView1.SetActiveView(Vcomentar)

    End Sub

    Protected Sub cmdGuardar_Click(sender As Object, e As EventArgs) Handles cmdGuardar.Click
        '  Try
        cmdGuardar.Enabled = True
            If TxtCitaComentario.Text <> "" Then
                SGrabarComentarioCita()
                MultiView1.SetActiveView(VFichaTecnica)

            Else
                LblMensajes.Text = "Faltan datos para poder ingreser el comentario"
            End If

        'Catch
        '     LblMensajes.Text = "Error, aun no se han guardado su comentario."
        'End Try
    End Sub


    Private Function SGrabarComentarioCita() As String
        Dim usuario As String
        Dim conexiontmp As Object
        Dim auditoria As String
        Dim sql2 As String
        usuario = Context.User.Identity.GetUserName()
        auditoria = ""
        sql2 = ""
        If RbtCitar.Checked = True Then
            sql2 = "insert into afuentes_cita (id_fuente_est_nest, observaciones, citada_en, fecha_cita,  id_usuario) values ('" & LblFuente.Text & "','" & HttpUtility.HtmlDecode(TxtObservacionPuntaje.Text) & "','" & HttpUtility.HtmlDecode(TxtCitaComentario.Text) & "',getdate(),'" & usuario & "')"
        Else
            If RbtComentar.Checked = True Then
                sql2 = "insert into afuentes_puntaje (id_fuente_est_nest, puntaje, motivo, fecha_evaluacion, id_usuario) values ('" & LblFuente.Text & "'," & RbtPuntaje.Text & ",'" & HttpUtility.HtmlDecode(TxtCitaComentario.Text) & "',getdate(),'" & usuario & "')"
            End If
        End If
        conexiontmp = Server.CreateObject("ADODB.Connection")
        conexiontmp.Open(Application("CadVJep"))
        conexiontmp.Execute(sql2)
        conexiontmp.close()
        conexiontmp = Nothing
        Return ""
    End Function


    Protected Sub RbtCitar_CheckedChanged(sender As Object, e As EventArgs) Handles RbtCitar.CheckedChanged
        If RbtCitar.Checked = True Then
            RbtComentar.Checked = False
            LblCitadaEn.Text = "Citada en"
            LblObservacionPuntaje.Text = "Observación"
            TxtObservacionPuntaje.Visible = True
            RbtPuntaje.Visible = False
        End If
    End Sub

    Protected Sub RbtComentar_CheckedChanged(sender As Object, e As EventArgs) Handles RbtComentar.CheckedChanged
        If RbtComentar.Checked = True Then
            RbtCitar.Checked = False
            LblCitadaEn.Text = "Comentario"
            LblObservacionPuntaje.Text = "Puntaje"
            TxtObservacionPuntaje.Visible = False
            RbtPuntaje.Visible = True

        End If
    End Sub

    Protected Sub LbtEditar_Click(sender As Object, e As EventArgs) Handles LbtEditar.Click
        Response.Redirect("Wnuevafuente.aspx?guid=" + LblFuente.Text)
    End Sub

    Protected Sub LbtConsultar_Click(sender As Object, e As EventArgs) Handles LbtConsultar.Click
        Response.Redirect("Wnuevafuente.aspx?guid=" + LblFuente.Text & "&lectura=" + "1")
    End Sub
End Class