﻿Imports System.Web.Optimization

Public Class Global_asax
    Inherits HttpApplication

    Sub Application_Start(sender As Object, e As EventArgs)
        ' Fires when the application is started
        RouteConfig.RegisterRoutes(RouteTable.Routes)
        BundleConfig.RegisterBundles(BundleTable.Bundles)
        Application("ExtensionesPermitidas") = ".pdf"


        Application("ExtensionActa") = "pdf"
        Application("FuentesYachay") = "GestionaFuentes"
        Application("EditorBasico") = "EditorBasico"
        Application("EditorJuridico") = "EditorJuridico"
        Application("Consulta") = "Consulta"
        Application("DefaultFileName") = "g:\servidor\yachay\"

        'Application("CadVJep") = "DRIVER={SQL Server}; SERVER=DESKTOP-6M1QUD6; DATABASE=PYachayProd09092019"
        'Application("CadUsr") = "DRIVER={SQL Server}; SERVER=DESKTOP-6M1QUD6; DATABASE=PYachayProd09092019"

        Application("CadVJep") = "DRIVER={SQL Server}; SERVER=SE-SQLSRV01-1; UID=wyachaytest; PWD=12345; DATABASE=PYachayPruebas"
        Application("CadUsr") = "DRIVER={SQL Server}; SERVER=SE-SQLSRV01-1;  UID=wyachaytest; PWD=12345; DATABASE=UsuariosConsulta"

        'Application("CadVJep") = "DRIVER={SQL Server}; SERVER=SE-SQLSRV01-1; UID=wyachay; PWD=Wy4ch4yJ3p2019.; DATABASE=PYachay"
        'Application("CadUsr") = "DRIVER={SQL Server}; SERVER=SE-SQLSRV01-1;  UID=wyachay; PWD=Wy4ch4yJ3p2019.; DATABASE=UsuariosConsulta"

        'Application("DefaultFileName") = "d:\servidor\AspirantesJep\"
        Application("mailportal") = "sin.respuesta@jep.gov.co"
        Application("kmail") = "Jep2.016Car0l"
        Application("smtp") = "smtp.office365.com"
        Application("PortalSSLmail") = True
        Application("PortalPuerto") = 587
        Application("ExtensionActa") = "PDF"
        Application("NombreSistema") = "YACHAY"

    End Sub
End Class