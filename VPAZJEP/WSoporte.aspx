﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" AspCompat="true" MasterPageFile="~/Site.Master" CodeBehind="WSoporte.aspx.vb" Inherits="SICA.WSoporte" %>



<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">


    <asp:ScriptManager ID="ScriptManager1" runat="server"/>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
 

    <h4><span class="colored-text">Soporte por inconvenientes para ingreso al sistema </span> </h4>
    
 
    <table>


            <tr>
                         <td colspan="2">
                             <br />
                             <asp:Label ID="Lblerror" runat="server" CssClass="col-md-12 control-label" ForeColor="Red" BackColor="#FFCCFF" Font-Bold="True" Font-Size="Medium"></asp:Label>
                             <br />
                             <br />
                         </td>

                

                     </tr>


        <tr>
            <td colspan="2">
                <asp:Label ID="Label2" runat="server" CssClass="col-md-12 control-label">Emplee esta funcionalidad cuando se ha olvidado su clave de acceso. El sistema le enviará una clave de confirmación a su correo registrado para permitirle que se registre nuevamente y asigne una nueva clave.</asp:Label>
            </td>
        </tr>
        <tr>
                         <td style="width: 237px">
                <asp:Label ID="Label1" runat="server" CssClass="col-md-5 control-label">Ingrese_su_correo</asp:Label>
                         </td>

                         <td>
                               <asp:TextBox runat="server" ID="Email" CssClass="form-control" TextMode="Email" MaxLength="150" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                    CssClass="text-danger" ErrorMessage="Dato requerido." />


                               <br />
                         </td>

        
                         </tr>

        
          <tr>
                         <td style="width: 237px">
                <asp:Label ID="Label4" runat="server" CssClass="col-md-5 control-label">Número_Documento_de_Identificación</asp:Label>
                              
                         </td>

                         <td>
                               <asp:TextBox runat="server" ID="txtUserName" TextMode="Number" CssClass="form-control" MaxLength="15" Width="264px"  />
                  <asp:RequiredFieldValidator runat="server" ControlToValidate="txtUserName"
                    CssClass="text-danger" ErrorMessage="Dato requerido." />

                               <br />
                         </td>

                     </tr>


<%--          <tr>
                         <td style="width: 237px">
                <asp:Label ID="Label3" runat="server" CssClass="col-md-5 control-label">Nombre_persona</asp:Label>
                         </td>

                         <td>
                               <asp:TextBox runat="server" ID="Txtnombreusua" CssClass="form-control" pattern="^[a-zA-ZñÑ, {|}~-]{0,100}"  title="entre 0 a 100 caracteres"  TextMode="Phone" MaxLength="150"   />
                
                              <asp:RequiredFieldValidator runat="server" ControlToValidate="Txtnombreusua"
                    CssClass="text-danger" ErrorMessage="Dato requerido." />
                               <br />
                         </td>

                     </tr>--%>

      

        <script type="text/javascript">
            function deshabilitar(boton) {
                
                document.getElementById(boton).style.visibility = 'hidden';  
                
                               
            }
    </script>
    <tr>
                         <td colspan="2" style="height: 46px">

                                      
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
<ProgressTemplate>
<div id="Background"></div>
<div id="Progress">
<img src="Imagenes/loading.gif" style="vertical-align:middle; height: 94px; width: 121px;"/>
    Espere mientras se envia a su correo la clave de autorización del cambio...
</div>
</ProgressTemplate>
</asp:UpdateProgress>
     <asp:UpdatePanel runat="server" id="Panel">
            <ContentTemplate>

                              <asp:Button ID="CmdEnviarClave" runat="server" class="btn btn-primary btn-lg" Height="47px" Text="Enviar Clave a su email" Width="332px" BackColor="#133e65"  OnClientClick="deshabilitar(this.id);" />
                   </ContentTemplate>
        </asp:UpdatePanel>
 
    

                                         <br />
                              <br />

                                         <br />
                                         <br />
                                         </td>

                     </tr>


        <tr>
                         <td style="width: 237px">
                <asp:Label ID="LblLugarS3" runat="server" CssClass="col-md-5 control-label" Visible="False">Código_de_Verificación</asp:Label>
                         </td>

                         <td>
                               <asp:TextBox ID="txtsegundaclave" runat="server" AutoPostBack="True" CssClass="form-control" MaxLength="20" TextMode="Number" Width="173px" Visible="False" />
                               <br />
                         </td>

                     </tr>

        <tr>
            <td colspan="2">

                <asp:Label ID="LblMensaje" runat="server" CssClass="col-md-12 control-label" ForeColor="#990000" Visible="False" Font-Bold="True">Puede volver a registrar su usuario para asignar una clave.</asp:Label>
                <br />

            </td>
        </tr>


        <tr>
                         <td colspan="2">
                                                            
                                                            <asp:Button ID="cmdSegundaclave" runat="server" class="btn btn-primary btn-lg" Height="47px" Text="Confirmar" Width="207px" Visible="False" BackColor="#133e65" />
                                                            <br />
                         </td>
                     </tr>

        <tr>
            <td colspan="2">
                
            </td>
        </tr>

                 </table>
    


     
                 </ContentTemplate>
</asp:UpdatePanel>
         

</asp:Content>
